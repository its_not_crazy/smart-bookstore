/*
 Navicat Premium Data Transfer

 Source Server         : aliyun
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : 123.57.145.162:3306
 Source Schema         : wisdombookstore

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 22/10/2020 08:33:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint(0) NULL DEFAULT NULL
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);
INSERT INTO `hibernate_sequence` VALUES (1);

-- ----------------------------
-- Table structure for rl_book_type_info
-- ----------------------------
DROP TABLE IF EXISTS `rl_book_type_info`;
CREATE TABLE `rl_book_type_info`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tb_book_info_code` bigint(0) NOT NULL,
  `tb_book_types_code` bigint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of rl_book_type_info
-- ----------------------------

-- ----------------------------
-- Table structure for tb_book
-- ----------------------------
DROP TABLE IF EXISTS `tb_book`;
CREATE TABLE `tb_book`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `input_date` datetime(0) NULL DEFAULT NULL,
  `rf_id_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  `tb_book_info_code` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_book
-- ----------------------------

-- ----------------------------
-- Table structure for tb_book_image
-- ----------------------------
DROP TABLE IF EXISTS `tb_book_image`;
CREATE TABLE `tb_book_image`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `book_images_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tb_book_info_code` bigint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_book_image
-- ----------------------------

-- ----------------------------
-- Table structure for tb_book_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_book_info`;
CREATE TABLE `tb_book_info`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `boot_store_id` bigint(0) NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `issuance_time` datetime(0) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `number` int(0) NULL DEFAULT NULL,
  `price` double NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_book_info
-- ----------------------------

-- ----------------------------
-- Table structure for tb_book_store
-- ----------------------------
DROP TABLE IF EXISTS `tb_book_store`;
CREATE TABLE `tb_book_store`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  `store_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_book_store
-- ----------------------------

-- ----------------------------
-- Table structure for tb_book_type
-- ----------------------------
DROP TABLE IF EXISTS `tb_book_type`;
CREATE TABLE `tb_book_type`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_book_type
-- ----------------------------

-- ----------------------------
-- Table structure for tb_brake_machine
-- ----------------------------
DROP TABLE IF EXISTS `tb_brake_machine`;
CREATE TABLE `tb_brake_machine`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_brake_machine
-- ----------------------------

-- ----------------------------
-- Table structure for tb_check_result
-- ----------------------------
DROP TABLE IF EXISTS `tb_check_result`;
CREATE TABLE `tb_check_result`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `lack_number` int(0) NULL DEFAULT NULL,
  `real_number` int(0) NULL DEFAULT NULL,
  `target_number` int(0) NULL DEFAULT NULL,
  `tb_check_task_code` bigint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tb_check_result
-- ----------------------------

-- ----------------------------
-- Table structure for tb_check_task
-- ----------------------------
DROP TABLE IF EXISTS `tb_check_task`;
CREATE TABLE `tb_check_task`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `end_check_date` datetime(0) NULL DEFAULT NULL,
  `start_check_date` datetime(0) NULL DEFAULT NULL,
  `target_number` int(0) NULL DEFAULT NULL,
  `tb_user_id` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tb_check_task
-- ----------------------------

-- ----------------------------
-- Table structure for tb_clerk
-- ----------------------------
DROP TABLE IF EXISTS `tb_clerk`;
CREATE TABLE `tb_clerk`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `age` int(0) NULL DEFAULT NULL,
  `boot_store_id` bigint(0) NULL DEFAULT NULL,
  `code_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `permissions` int(0) NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_clerk
-- ----------------------------

-- ----------------------------
-- Table structure for tb_collect
-- ----------------------------
DROP TABLE IF EXISTS `tb_collect`;
CREATE TABLE `tb_collect`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tb_book_info_code` bigint(0) NOT NULL,
  `tb_user_code` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tb_collect
-- ----------------------------

-- ----------------------------
-- Table structure for tb_escaped_single
-- ----------------------------
DROP TABLE IF EXISTS `tb_escaped_single`;
CREATE TABLE `tb_escaped_single`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tb_user_code` bigint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_escaped_single
-- ----------------------------

-- ----------------------------
-- Table structure for tb_into_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_into_record`;
CREATE TABLE `tb_into_record`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `into_date` datetime(0) NULL DEFAULT NULL,
  `user_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_into_record
-- ----------------------------

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `boot_store_id` bigint(0) NULL DEFAULT NULL,
  `current_min` datetime(0) NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  `tb_user_code` bigint(0) NOT NULL,
  `total_num` int(0) NULL DEFAULT NULL,
  `total_price` int(0) NULL DEFAULT NULL,
  `we_chat_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_order
-- ----------------------------

-- ----------------------------
-- Table structure for tb_orders_detail
-- ----------------------------
DROP TABLE IF EXISTS `tb_orders_detail`;
CREATE TABLE `tb_orders_detail`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `current_price` int(0) NULL DEFAULT NULL,
  `num` int(0) NULL DEFAULT NULL,
  `tb_book_info_code` bigint(0) NOT NULL,
  `tb_book_info_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `total_price` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_orders_detail
-- ----------------------------

-- ----------------------------
-- Table structure for tb_out_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_out_record`;
CREATE TABLE `tb_out_record`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `out_date` datetime(0) NULL DEFAULT NULL,
  `user_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_out_record
-- ----------------------------

-- ----------------------------
-- Table structure for tb_payment_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_payment_record`;
CREATE TABLE `tb_payment_record`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `payment_date` datetime(0) NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  `tb_order_code` bigint(0) NULL DEFAULT NULL,
  `tb_user_id` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tb_payment_record
-- ----------------------------

-- ----------------------------
-- Table structure for tb_plan
-- ----------------------------
DROP TABLE IF EXISTS `tb_plan`;
CREATE TABLE `tb_plan`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `end_minute` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `end_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `start_minute` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `start_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `years` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_plan
-- ----------------------------

-- ----------------------------
-- Table structure for tb_replenishment_result
-- ----------------------------
DROP TABLE IF EXISTS `tb_replenishment_result`;
CREATE TABLE `tb_replenishment_result`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `lack_number` int(0) NULL DEFAULT NULL,
  `real_number` int(0) NULL DEFAULT NULL,
  `target_number` int(0) NULL DEFAULT NULL,
  `tb_replenishment_task_code` bigint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tb_replenishment_result
-- ----------------------------

-- ----------------------------
-- Table structure for tb_replenishment_task
-- ----------------------------
DROP TABLE IF EXISTS `tb_replenishment_task`;
CREATE TABLE `tb_replenishment_task`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `end_check_date` datetime(0) NULL DEFAULT NULL,
  `start_check_date` datetime(0) NULL DEFAULT NULL,
  `target_number` int(0) NULL DEFAULT NULL,
  `tb_book_info_code` bigint(0) NOT NULL,
  `tb_user_id` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tb_replenishment_task
-- ----------------------------

-- ----------------------------
-- Table structure for tb_shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `tb_shopping_cart`;
CREATE TABLE `tb_shopping_cart`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tb_book_info_code` bigint(0) NOT NULL,
  `tb_user_code` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of tb_shopping_cart
-- ----------------------------

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `boot_store_id` bigint(0) NULL DEFAULT NULL,
  `mobile` int(0) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `state` int(0) NULL DEFAULT NULL,
  `we_chat_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `we_chat_image_code` bigint(0) NOT NULL,
  `we_chat_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------

-- ----------------------------
-- Table structure for tb_we_chat_image
-- ----------------------------
DROP TABLE IF EXISTS `tb_we_chat_image`;
CREATE TABLE `tb_we_chat_image`  (
  `id` int(0) NOT NULL,
  `code` bigint(0) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `we_chat_code` bigint(0) NOT NULL,
  `we_chat_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_we_chat_image
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
