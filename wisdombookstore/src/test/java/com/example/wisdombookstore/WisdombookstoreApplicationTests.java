package com.example.wisdombookstore;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;


@Component
@SpringBootTest
public class WisdombookstoreApplicationTests {


    public static int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= 1 << 30) ? 1 << 30 : n + 1;
    }



    @Test
    void contextLoads() {


//        Date date = DateTime.getDate();
//        System.err.println(date);
//
//        LambdaQueryWrapper<TbCheckTask> tbCheckTaskLambdaQueryWrapper = new QueryWrapper<TbCheckTask>()
//                .lambda()
//                .orderByDesc(TbCheckTask::getCreateTime).last("limit 1");
//        TbCheckTask tbCheckTask = this.tbCheckTaskMapper.selectOne(tbCheckTaskLambdaQueryWrapper);
//        Date createTime = tbCheckTask.getCreateTime();
//
//        Date date1 = DateTime.getDate(createTime);
//        System.err.println(date1);
//
//        boolean before = date.before(createTime);
//        System.err.println(before);

    }

}
