package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @author 李慧龙
 * @date 2020/10/21 8:26
 * 计划表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TbPlan extends BaseEntity implements Serializable {

    /**
     * 年
     */
    private String years;

    /**
     * 月
     */
    private String month;

    /**
     * 天
     */
    private String date;

    /**
     * 开始时
     */
    private String startTime;

    /**
     * 开始分
     */
    private String startMinute;

    /**
     * 结束时
     */
    private String endTime;

    /**
     * 结束分
     */
    private String endMinute;
}
