package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 支付记录
 * @Author: 宋合
 * @Date: 2020/10/20 15:53
 * @return: null
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("支付记录")
public class TbPaymentRecord extends BaseEntity implements Serializable {

    /**
     * 用户标识
     */
    private Long tbUserId;
    @Transient
    @TableField(exist = false)
    private TbUser tbUser;

    /**
     * 订单标识
     */
    @ApiModelProperty("订单标识")
    private Long tbOrderCode;
    @Transient
    @TableField(exist = false)
    private TbOrder tbOrder;

    /**
     * 支付时间
     */
    @ApiModelProperty("支付时间")
    private Date paymentDate;

    /**
     * 支付状态 1：成功 2：失败
     */
    @ApiModelProperty("支付状态 1：成功 2：失败")
    private Integer state;


}
