package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author 李慧龙
 * @date 2020/10/21 9:27
 * 订单详情表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("购物车商品表")
public class TbShoppingGoods extends BaseEntity implements Serializable {

    /**
     * 商品标识
     */
    private long tbBookInfoCode;

    @Transient
    @TableField(exist = false)
    private TbBookInfo tbBookInfo;

    /**
     * 单品下单数量
     */
    @ApiModelProperty("单品下单数量")
    private Integer num;

    @Transient
    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private List<TbShoppingBook> tbShoppingBooks;

    /**
     * 购物车标识
     */
    private Long tbShoppingCartCode;

    @Transient
    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private TbShoppingCart tbShoppingCart;

}
