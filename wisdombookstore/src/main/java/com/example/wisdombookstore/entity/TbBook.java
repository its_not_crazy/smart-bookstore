package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 李慧龙
 * @date 2020/10/19 20:59
 * 单个图书 的实体类(具体达到一个)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("图书的实体类")
public class TbBook extends BaseEntity implements Serializable {

    /**
     * 唯一rfid
     */
    @ApiModelProperty("图书的Rfid标识")
    private String rfIdCode;

    /**
     * 图书的名称
     */
    @ApiModelProperty("图书的名称")
    private String title;

    /**
     * 图书价格
     */
    @ApiModelProperty("图书价格")
    private Double price;

    /**
     * 图书的状态  (0未售卖  1以售卖)
     */
    @ApiModelProperty("图书的状态 0未售卖  1以售卖")
    private Integer status;

    @ApiModelProperty("订单详情标识")
    private Long tbOrderDetailCode;
    /**
     * 书籍信息标识
     */
    @ApiModelProperty("书籍信息标识")
    private Long tbBookInfoCode;
    @TableField(exist = false)
    @Transient
    private TbBookInfo tbBookInfo;


}
