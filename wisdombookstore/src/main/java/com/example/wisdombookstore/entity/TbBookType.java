package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 李慧龙
 * @date 2020/10/19 21:12
 * 书籍类别的实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TbBookType extends BaseEntity implements Serializable {

    /**
     * 类别名称 品类1 品类2 品类3 品类4 品类5
     */
    private String typeName;

    /**
     * 一个列别包含多种图书(通过中间表查询)
     */
    @TableField(exist = false)
    @Transient
    private List<TbBookInfo> tbBookInfos;

}
