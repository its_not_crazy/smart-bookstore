package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 盘点图书缺失记录表
 * @date 2020/10/24 17:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
@TableName
public class TbCheckDeficiency extends BaseEntity implements Serializable {

    /**
     * 盘点任务标识
     */
    @ApiModelProperty("盘点任务标识")
    private long tbCheckTaskCode;

    /**
     * 唯一rfid
     */
    @ApiModelProperty("图书的Rfid标识")
    private String rfIdCode;

    /**
     * 图书的名称
     */
    @ApiModelProperty("图书的名称")
    private String title;

    /**
     * 图书价格
     */
    @ApiModelProperty("图书价格")
    private Double price;
}
