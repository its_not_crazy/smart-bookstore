package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author 李慧龙
 * @date 2020/10/21 9:27
 * 订单详情表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("订单详情")
public class TbOrdersDetail extends BaseEntity implements Serializable {

    /**
     * 图书标识(这一类,不是某一本)
     */
    private long tbBookInfoCode;

    @Transient
    @TableField(exist = false)
    private TbBookInfo tbBookInfo;

    /**
     * 下单时单品的名字
     */
    @ApiModelProperty("下单时单品的名字")
    private String tbBookInfoName;

    /**
     * 下订单时的价格
     */
    @ApiModelProperty("下单时单品的价格")
    private double currentPrice;

    /**
     * 订单标识
     */
    private Long tbOrderCode;

    @Transient
    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private TbOrder tbOrder;

    /**
     * 单品下单数量
     */
    @ApiModelProperty("单品下单数量")
    private Integer num;

    @Transient
    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private List<TbBook> tbBooks;

    /**
     * 下单时单品的总价格
     */
    @ApiModelProperty("下单时单品的总价格")
    private double totalPrice;

}
