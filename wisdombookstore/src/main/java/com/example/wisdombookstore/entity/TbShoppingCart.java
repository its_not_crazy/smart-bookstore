package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: 购物车
 * @Author: 宋合
 * @Date: 2020/10/21 11:27
 * @return: null
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@TableName
@Table
@ApiModel("购物车实体类")
public class TbShoppingCart extends BaseEntity implements Serializable {
    /**
     * 用户主键
     **/
    private Long tbUserCode;
    /**
     * 用户详细信息
     */
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(hidden = true)
    private TbUser tbUser;

    /**
     * 商品标识
     **/
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(hidden = true)
    private List<TbShoppingGoods> tbShoppingGoods;

}
