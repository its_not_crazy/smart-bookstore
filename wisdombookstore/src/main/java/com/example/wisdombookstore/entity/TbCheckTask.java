package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Target;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author: 宋合
 * @Date: 2020/10/20 18:06
 * @return: null
 * 盘点任务表
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("盘点任务表")
public class TbCheckTask extends BaseEntity implements Serializable {

    /**
     * 盘点人标识
     */
    @ApiModelProperty("盘点人标识")
    private Long TbClerkCode;

    /**
     * 盘点人详细信息
     */
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(hidden = false)
    private TbClerk tbClerk;

    /**
     * 盘点状态 0:未盘点   1  开始盘点  2   盘点结束   3 未盘点（在规定时间内未进行盘点）
     */
    @ApiModelProperty("盘点状态")
    private Integer status;

    /**
     * 开始时间
     */
    @ApiModelProperty("开始时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date startCheckDate;

    /**
     * 结束时间
     */
    @ApiModelProperty("结束时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date endCheckDate;

    /**
     * 目标数量
     */
    @ApiModelProperty("目标数量")
    private Integer targetNumber;

    /**
     * 盘点类型逗号拼接
     */
    @ApiModelProperty("盘点类型逗号拼接")
    private String typeJoint;

    /**
     * 盘点结果唯一标识
     */
    @ApiModelProperty("盘点结果唯一标识")
    private Long tbCheckResultCode;

    /**
     * 盘点结果
     */
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(value = "盘点结果详细信息",hidden = false)
    private TbCheckResult tbCheckResult;



}
