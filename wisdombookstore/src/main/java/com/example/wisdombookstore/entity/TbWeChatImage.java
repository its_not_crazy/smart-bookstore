package com.example.wisdombookstore.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @author 李慧龙
 * @date 2020/10/20 21:17
 * 微信头像表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("微信头像实体类")
public class TbWeChatImage extends BaseEntity implements Serializable {
    /**
     * 微信头像唯一标识
     */
    @ApiModelProperty("微信头像唯一标识")
    private long weChatCode;

    /**
     * 微信头像图片路径
     */
    @ApiModelProperty("微信头像图片路径")
    private String weChatUrl;
}
