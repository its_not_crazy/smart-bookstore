package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author 李慧龙
 * @date 2020/10/19 21:19
 * 工作人员的实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("工作人员的实体类")
public class TbClerk extends BaseEntity implements Serializable {


    /**
     * 工作人员姓名
     */
    @ApiModelProperty("工作人员姓名")
    private String name;

    /**
     *工作人员密码
     */
    @ApiModelProperty("工作人员密码")
    private String password;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    private String mobile;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String address;

    /**
     * 角色  0店长 1工作人员
     */
    @ApiModelProperty("角色  0店长 1工作人员 ")
    private Integer permissions;

    /**
     * 状态 0:在职  1:离职
     */
    @ApiModelProperty("状态 0:在职  1:离职")
    private Integer status;
    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;

    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    private String idCard;


    /**
     * 书店唯一标识(店名)
     */
    private Long bootStoreCode;
    @Transient
    @TableField(exist = false)
    @ApiModelProperty(value = "书店实体类",hidden = true)
    private TbBookStore bookStore;

}
