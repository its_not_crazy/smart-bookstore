package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author 李慧龙
 * @date 2020/10/19 20:59
 * 单个图书 的实体类(具体达到一个)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("购物车图书")
public class TbShoppingBook extends BaseEntity implements Serializable {

    /**
     * 单品图书标识
     */
    @ApiModelProperty("单品图书标识")
    private Long tbBookCode;

    @ApiModelProperty("购物车商品标识")
    private Long tbShoppingGoodsCode;

    @TableField(exist = false)
    @Transient
    @ApiModelProperty(hidden = true)
    private TbShoppingGoods tbShoppingGoods;

}
