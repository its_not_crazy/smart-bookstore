package com.example.wisdombookstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @program: wisdombookstore
 * @description: 动态定时任务实体类
 * @author: 陈阳
 * @create: 2020-10-28 21:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TbCron extends BaseEntity implements Serializable {

    /**
     * 存放任务周期
     */
    private String cron;
}
