package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

/**
 * @author 李慧龙
 * @date 2020/10/26 20:09
 * @className DataAnalysis
 * 数据分析表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@TableName
public class TbDataAnalysis extends BaseEntity{

    /**
     * 老顾客购买领
     */
    private Integer volume;

    /**
     * 入店人数
     */
    private Integer amount;

    /**
     * 入店转化率
     */
    private Double rate;

    /**
     * 新顾客数
     */
    private Integer numpe;

    /**
     * 订单人数
     */
    private Integer orderNum;

}
