package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author 李慧龙
 * @date 2020/10/21 10:38
 * 图书类别跟图书信息的多对多表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class RlBookTypeInfo extends BaseEntity implements Serializable {

    /**
     * 图书信息的唯一标识(某一类)
     */
    @ApiModelProperty(example = "0")
    private long tbBookInfoCode;

    /**
     * 图书类别的唯一标识
     */
    private long tbBookTypesCode;

    @TableField(exist = false)
    @Transient
    private TbBookType tbBookType;
}
