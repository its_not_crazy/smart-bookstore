package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @author 李慧龙
 * @date 2020/10/19 21:26
 * 闸机的实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("闸机的实体类")
public class TbBrakeMachine extends BaseEntity implements Serializable {

    /**
     * 闸机名称
     */
    @ApiModelProperty("闸机名称")
    private String name;

    /**
     * 闸机状态 0开启 1关闭
     */
    @ApiModelProperty("闸机状态 0开启 1关闭")
    private Integer status;
}
