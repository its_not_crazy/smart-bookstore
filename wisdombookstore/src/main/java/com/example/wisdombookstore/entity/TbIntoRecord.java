package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 李慧龙
 * @date 2020/10/20 15:19
 * 进店记录
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("进店记录")
public class TbIntoRecord extends BaseEntity implements Serializable {

    /**
     * 用户的唯一标识
     */
    @ApiModelProperty("用户的code标识")
    private Long userCode;
    @TableField(exist = false)
    @Transient
    private TbUser tbUser;

    /**
     * 进店时间
     */
    @ApiModelProperty("进店时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date intoDate;
}
