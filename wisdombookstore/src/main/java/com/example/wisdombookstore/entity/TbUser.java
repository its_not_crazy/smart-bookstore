package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * @author 李慧龙
 * @date 2020/10/19 21:16
 * 用户的实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("用户的实体类")
public class TbUser extends BaseEntity implements Serializable {

    /**
     * 用户姓名
     */
    @ApiModelProperty(value = "用户姓名",example = "")
    private String name;
    /**
     * 用户密码
     */
    @ApiModelProperty(value = "用户密码",example = "",required=true,dataType = "String")
    private String pwd;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号",example = "",required=true,dataType = "String")
    private String mobile;

    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号",example ="",required=true,dataType = "String")
    private String weChatCode;

    /**
     * 微信名称
     */
    @ApiModelProperty(value = "微信名称",example ="")
    private String weChatName;

    /**
     * 用户状态 1 正常 2 锁定
     */
    @ApiModelProperty(value = "用户状态 0 正常 1 锁定",example = "0")
    private Integer state;

    /**
     * 微信头像的唯一保标识
     */
    @ApiModelProperty(value = "头像URL",example = "0")
    private String imageUrl;

    /**
     * 购物车集合
     */
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(hidden = true)
    private List<TbShoppingCart> tbShoppingCartList;

    /**
     * 书店唯一标识(店名)
     */
    @ApiModelProperty(value = "书店唯一标识",example = "0")
    private Long bootStoreId;
    @Transient
    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private TbBookStore bookStore;
}
