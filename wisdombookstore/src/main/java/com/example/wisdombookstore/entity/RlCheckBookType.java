package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 盘点任务-书籍类型中间表
 * @date 2020/10/24 16:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
@TableName
public class RlCheckBookType extends BaseEntity implements Serializable {

    /**
     * 盘点任务唯一标识
     */
    private Long TbCheckTaskCode;

    /**
     * 盘点类型唯一标识
     */
    private Long tbBookTypeCode;

    /**
     * 书籍类型标识
     */
    @TableField(exist = false)
    @Transient
    private TbBookType tbBookType;




}
