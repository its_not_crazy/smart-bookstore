package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author 李慧龙
 * @date 2020/10/19 21:29
 * 逃单的实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("逃单的实体类")
public class TbEscapedSingle extends BaseEntity implements Serializable {

    /**
     * 备注(用户逃单后的解决结果)
     */
    @ApiModelProperty("备注")
    private String note;

    /**
     * 用户的唯一标识
     */
    @ApiModelProperty("用户的唯一标识")
    private long tbUserCode;
    @Transient
    @TableField(exist = false)
    private TbUser tbUser;
}
