package com.example.wisdombookstore.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @ClassName TbReplenishmentParticular  补货详情表
 * @Description TODO TbReplenishmentParticular
 * @Author 8aceMak1r
 * @Date 2020/10/26 10:10
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TbReplenishmentParticular extends BaseEntity implements Serializable {

    /**
     * 补货结果标识
     */
    @ApiModelProperty(value = "补货结果标识")
    private Long tbReplenishmentResultCode;

    /**
     * 单品书籍标识
     */
    @ApiModelProperty(value = "单品书籍标识")
    private Long tbBookCode;

    /**
     * 书籍类型标识
     */
    @ApiModelProperty(value = "书籍类型标识")
    private Long tbBookTypeCode;

    /**
     * 唯一rfid
     */
    @ApiModelProperty("图书的Rfid标识")
    private String rfIdCode;
}
