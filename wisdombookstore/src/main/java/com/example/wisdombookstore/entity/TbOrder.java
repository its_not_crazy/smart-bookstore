package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 李慧龙
 * @date 2020/10/20 21:31
 * 订单的实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("订单的实体类")
public class TbOrder extends BaseEntity implements Serializable {

    /**
     * 用户标识
     */
    @ApiModelProperty(value = "用户标识",required=true,dataType = "int")
    private long tbUserCode;
    @TableField(exist = false)
    @Transient
    private TbUser tbUser;

    /**
     * 订单状态  0已完成  1未完成
     */
    @ApiModelProperty(value = "订单状态  0已完成  1未完成",dataType = "int")
    private Integer status;

    /**
     * 总价格(多有单品)
     */
    @ApiModelProperty(value = "总价格",dataType = "int")
    private double totalPrice;

    /**
     * 订单总数量(所有单品)
     */
    @ApiModelProperty(value = "订单总数量",dataType = "int")
    private Integer totalNum;

    /**
     * 微信支付记录唯一标识
     */
    @ApiModelProperty(value = "微信支付记录唯一标识",dataType = "String")
    private String weChatCode;

    /**
     * 下单时间
     */
    @ApiModelProperty(value = "下单时间")
    private Date currentMin;

    /**
     * 订单详情的集合
     */
    @Transient
    @TableField(exist = false)
    private List<TbOrdersDetail> tbOrdersDetails;

    /**
     * 书店的唯一标识
     */
    @ApiModelProperty(value = "书店的唯一标识")
    private Long bootStoreId;
    @Transient
    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private TbBookStore bookStore;
}
