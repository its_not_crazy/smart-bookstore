package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author: 宋合
 * @Date: 2020/10/20 18:06
 * @return: null
 * 补货任务表
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("补货任务表")
public class TbReplenishmentTask extends BaseEntity implements Serializable {

    /**
     * 补货人标识
     */
    @ApiModelProperty("补货人标识")
    private Long tbUserCode;

    @ApiModelProperty("补货人实体")
    @TableField(exist = false)
    @Transient
    private TbClerk tbClerk;

    /**
     * 开始时间
     */
    @ApiModelProperty("开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startCheckDate;

    /**
     * 结束时间
     */
    @ApiModelProperty("结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCheckDate;

    /**
     * 目标数量
     */
    @ApiModelProperty("目标数量")
    private Integer targetNumber;

    /**
     * 书籍类型标识
     */
    @TableField(exist = false)
    @Transient
    private List<TbBookType> tbBookTypes;

    /**
     * 书籍的唯一标识(根据书籍信息进行补货)
     */
    @ApiModelProperty("书籍的唯一标识")
    private long tbBookInfoCode;

    /**
     * 书籍实体
     */
    @ApiModelProperty("书籍实体")
    @TableField(exist = false)
    @Transient
    private TbBookInfo tbBookInfo;



}
