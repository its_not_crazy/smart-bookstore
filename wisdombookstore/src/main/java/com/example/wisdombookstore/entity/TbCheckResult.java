package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 李慧龙
 * @date 2020/10/21 8:40
 * 盘点结果表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("盘点结果表")
public class TbCheckResult extends BaseEntity implements Serializable {

    /**
     * 盘点任务标识
     */
    @ApiModelProperty("盘点任务标识")
    private long tbCheckTaskCode;

    @ApiModelProperty(value = "盘点任务对象",hidden = true)
    @Transient
    @TableField(exist = false)
    private TbCheckTask tbCheckTask;

    /**
     * 目标数量
     */
    @ApiModelProperty(value = "目标数量")
    private Integer targetNumber;

    /**
     * 实际数量
     */
    @ApiModelProperty(value = "实际数量")
    private Integer realNumber;

    /**
     * 缺少数量
     */
    @ApiModelProperty(value = "缺少数量")
    private Integer lackNumber;

    /**
     * 缺少的书籍
     */
    @ApiModelProperty(value = "书籍实体",hidden = true)
    @TableField(exist = false)
    @Transient
    private List<TbBook> tbBookCodes;

}
