package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author 李慧龙
 * @date 2020/10/20 21:34
 * 商品图片表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("书籍照片实体类")
public class TbBookImage extends BaseEntity implements Serializable {

    /**
     * 图书图片路径
     */
    @ApiModelProperty("图书图片路径")
    private String bookImagesUrl;

    /**
     * 图书详情信息的唯一标识
     */
    private long tbBookInfoCode;
    @TableField(exist = false)
    @Transient
    private TbBookInfo tbBookInfo;
}
