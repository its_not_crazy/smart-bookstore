package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * @author 李慧龙
 * @date 2020/10/19 21:22
 * 书店的实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel(value = "书店实体类")
public class TbBookStore extends BaseEntity implements Serializable {

    /**
     * 书店名称
     */
    @ApiModelProperty("书店名称")
    private String storeName;

    /**
     * 门店状态 0.营业中 1.未营业
     */
    @ApiModelProperty("书店状态")
    private Integer status;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细住址")
    private String address;

    /**
     * 包含的员工集合
     */
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(value = "所有员工",hidden = true)
    private List<TbClerk> tbClerks;
    /**
     * 店长的唯一表示
     */
    @ApiModelProperty("店长id")
    private Long tbClerkCode;

    /**
     * 店长实体类
     */
    @ApiModelProperty(value = "店长实体类",hidden = true)
    @Transient
    @TableField(exist = false)
    private TbClerk tbClerk;

    /**
     * 书籍信息集合
     */
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(value = "所有书籍",hidden = true)
    private List<TbBookInfo> tbBookInfos;

    /**
     * 用户的集合
     */
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(value = "所有用户",hidden = true)
    private List<TbUser> tbUsers;

    /**
     * 订单的集合
     */
    @ApiModelProperty(value = "所有订单",hidden = true)
    @TableField(exist = false)
    @Transient
    private List<TbOrder> tbOrders;
}
