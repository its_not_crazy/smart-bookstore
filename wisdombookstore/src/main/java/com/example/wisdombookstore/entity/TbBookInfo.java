package com.example.wisdombookstore.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 书籍信息(具体一类)
 * @date 2020/10/20 11:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ApiModel("书籍信息的实体类")
public class TbBookInfo extends BaseEntity implements Serializable {

    /**
     * 书籍名称
     */
    @ApiModelProperty("书籍名称")
    private String name;

    /**
     * 书籍作者
     */
    @ApiModelProperty("书籍作者")
    private String author;

    /**
     * 发行日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("发行日期")
    private Date issuanceTime;

    /**
     * 书籍介绍
     */
    @ApiModelProperty("书籍介绍")
    private String content;

    /**
     * 书籍价格
     */
    @ApiModelProperty("书籍价格")
    private Double price;

    /**
     * 书籍数量
     */
    @ApiModelProperty("书籍数量")
    private Integer number;

    /**
     * 书籍状态 1：未售完:2已售完
     */
    @ApiModelProperty("书籍状态 1：未售完:2已售完")
    @Column(name="status",columnDefinition="tinyint default 0")
    private Integer status;

    /**
     * 一本书籍对应多个类别(通过中间表查询)
     */
    private Long tbBookTypeCode;
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(hidden = true)
    private TbBookType tbBookType;
    @TableField(exist = false)
    @Transient
    @ApiModelProperty(hidden = true)
    private List<RlBookTypeInfo> rlBookTypeInfos;

    /**
     * 书籍的图片信息唯一标识
     */
    @TableField(exist = false)
    @Transient
    private List<TbBookImage> tbBookImages;

    /**
     * 书籍信息里边包含多本图书
     */
    @TableField(exist = false)
    @Transient
    private List<TbBook> tbBooks;

}
