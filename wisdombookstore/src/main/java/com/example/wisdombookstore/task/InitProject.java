package com.example.wisdombookstore.task;

import com.example.wisdombookstore.entity.TbCron;
import com.example.wisdombookstore.mapper.TbCronMapper;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @program: InitProject
 * @description:
 * @author: 陈阳
 * @create: 2020-10-31 11:28
 */
@Component
@Order(1)
public class InitProject implements CommandLineRunner {

    @Resource
    private TbCronMapper tbCronMapper;

    @Resource
    private IdWorker idWorker;

    public static int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= 1 << 30) ? 1 << 30 : n + 1;
    }

    @Override
    public void run(String... args) throws Exception {
        System.err.println("执行初始化方法");
        System.err.println(this.tableSizeFor(20));
//        TbCron tbCron = new TbCron();
//        tbCron.setId(null);
//        tbCron.setCron(String.valueOf(idWorker.nextId()));
//        tbCron.setIsDel(StateConstant.OBJECT_DEL_NORMAL);
//        tbCron.setCreateTime(new Date());
//        tbCron.setUpdateTime(null);
//        tbCron.setCron("* 00 16 * * *");
//        int insert = tbCronMapper.insert(tbCron);
    }

//    @Override
//    public void run(ApplicationArguments args) throws Exception {
//        System.err.println("执行初始化方法");
//        TbCron tbCron = new TbCron();
//        tbCron.setId(null);
//        tbCron.setCron(String.valueOf(idWorker.nextId()));
//        tbCron.setIsDel(StateConstant.OBJECT_DEL_NORMAL);
//        tbCron.setCreateTime(new Date());
//        tbCron.setUpdateTime(null);
//        tbCron.setCron("* 00 16 * * *");
//        int insert = tbCronMapper.insert(tbCron);
//    }
}
