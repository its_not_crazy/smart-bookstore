package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.*;
import com.example.wisdombookstore.mapper.TbOrderMapper;
import com.example.wisdombookstore.mapper.TbOrdersDetailMapper;
import com.example.wisdombookstore.service.*;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

@Service
public class OrdersDetailServiceImpl extends ServiceImpl<TbOrdersDetailMapper, TbOrdersDetail> implements IOrdersDetailService {


}
