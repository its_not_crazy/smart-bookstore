package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.wisdombookstore.entity.TbReplenishmentResult;
import com.example.wisdombookstore.mapper.TbReplenishmentResultMapper;
import com.example.wisdombookstore.service.IReplenishmentResultService;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;


/**
 * @Author: MaHan
 * @Date: Created in 15:53 2020/10/24
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: ReplenishmentResultServiceImpl
 */
@Service
public class ReplenishmentResultServiceImpl implements IReplenishmentResultService {
    @Autowired
    private TbReplenishmentResultMapper tbReplenishmentResultMapper;

    /**
     * TODO &马晗& 补货结果实现
     * @param currentPage: 当前页数
     * @param pageSize:  每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  查询出所有补货结果
     * @Date 16:09 2020/10/24
     **/
    @Override
    public ResponseResult replenishmentResults(Integer currentPage, Integer pageSize) {
        //提示信息
        ResponseResult result = new ResponseResult();
        //分页插件
        Page<TbReplenishmentResult> page = new Page<>(currentPage,pageSize);
        //mybatis puls 插件
        QueryWrapper<TbReplenishmentResult> queryWrapper = new QueryWrapper<>();
        //查询所有补货结果
        Page<TbReplenishmentResult> tbReplenishmentResultPage = this.tbReplenishmentResultMapper.selectPage(page, queryWrapper);
        //返回结果
        result.setResult(tbReplenishmentResultPage);
        return result;
    }

    @Override
    public ResponseResult submitReplenishmentTask(Long tbReplenishmentResultCode) {
        ResponseResult result=ResponseResult.SUCCESS();
        HashMap<String, Object> resultMap = new HashMap<>();
        try {
            QueryWrapper<TbReplenishmentResult> wrapper=new QueryWrapper<>();
            wrapper.lambda().eq(TbReplenishmentResult::getCode,tbReplenishmentResultCode);
            TbReplenishmentResult replenishmentResult = tbReplenishmentResultMapper.selectOne(wrapper);
            if (replenishmentResult==null){
                result.setFailMessage("无此补货任务");
                return result;
            }
            if (replenishmentResult.getRealNumber().equals(replenishmentResult.getTargetNumber())){
                replenishmentResult.setStatus(StateConstant.TBREPLENISHMENTRECORD_STATUS_ALREADY);
                tbReplenishmentResultMapper.update(replenishmentResult,wrapper);
                resultMap.put("replenishmentResult",replenishmentResult);
                result.setResult(resultMap);
                result.setMessage("提交成功");
                result.setSuccess(true);
                return result;
            }else {
                result.setFailMessage("任务未完成");
                return result;
            }
        }catch (Exception e){
            e.printStackTrace();
            result.setMessage("程序异常" + e.toString());
            result.setSuccess(false);
            return result;
        }
    }


    /**
     * TODO 8aceMak1r getReplenishmentResultCode 根据任务查询补货结果
     *
     * @param tbReplenishmentTaskCode
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description:
     * @author 8aceMak1r
     * @date 2020/10/26 18:54
     */
    @Override
    public ResponseResult getReplenishmentResultCode(Long tbReplenishmentTaskCode) {
        ResponseResult result=ResponseResult.SUCCESS();
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        try {
            QueryWrapper<TbReplenishmentResult> wrapper=new QueryWrapper<>();
            wrapper.lambda().eq(TbReplenishmentResult::getTbReplenishmentTaskCode,tbReplenishmentTaskCode);
            List<TbReplenishmentResult> replenishmentResult = tbReplenishmentResultMapper.selectList(wrapper);
            if(replenishmentResult==null){
                result.setFailMessage("查询失败");
                return result;
            }
            resultMap.put("replenishmentResult",replenishmentResult);
            result.setResult(resultMap);
            result.setSuccess(true);
            result.setMessage("操作成功");
            return result;
        }catch (Exception e) {
            e.printStackTrace();
            result.setMessage("程序异常" + e.toString());
            result.setSuccess(false);
            return result;
        }
    }

}
