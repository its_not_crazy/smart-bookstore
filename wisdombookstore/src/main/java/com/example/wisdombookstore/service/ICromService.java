package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbCron;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @program: wisdombookstore
 * @description: 定时任务规则
 * @author: 陈阳
 * @create: 2020-10-28 22:05
 */
public interface ICromService extends IService<TbCron> {


    /*
    * TODO 陈阳   定时任务规则添加
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/10/28 22:21
     * @param crom:   定时任务规则
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult addCrom(Integer crom);

}
