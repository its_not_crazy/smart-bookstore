package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.TbBookImage;
import com.example.wisdombookstore.entity.TbShoppingCart;
import com.example.wisdombookstore.mapper.TbBookImageMapper;
import com.example.wisdombookstore.mapper.TbShoppingCartMapper;
import com.example.wisdombookstore.service.IBookImageService;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

/**
 * @author 李慧龙
 * @date 2020/10/23 10:12
 * @className BookImageServiceImpl
 */
@Service
public class BookImageServiceImpl extends ServiceImpl<TbBookImageMapper, TbBookImage> implements IBookImageService {
    @Resource
    TbBookImageMapper tbBookImageMapper;

    @Resource
    IdWorker idWorker;
    /**
     * 添加书籍照片信息
     *
     * @param files: 书籍照片信息
     * @return boolean
     * @Author 李慧龙
     * @Description //TODO addTbBookInfoImages
     * @Date 9:47 2020/10/23
     */
    @Override
    public ResponseResult addTbBookInfoImages(MultipartFile[] files) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        if (files.length == 0) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请选择文件");
        }
        //--直接指向项目的跟路径
        String property = System.getProperty("user.dir");
        //---系统根路径
        String filePath = property + "\\src\\main\\resources\\file\\";
        //创建一个存放图片的路径
        File file1 = new File(filePath);
        if (!file1.exists()) {
            file1.mkdir();
        }
        ArrayList<Object> list = new ArrayList<>();
        for (MultipartFile file : files) {
            try {
                //获取文件大的名字
                String fileName = file.getOriginalFilename();
                //创建图片文件
                file.transferTo(new File(filePath + fileName));
                //创建一个图片对象添加
                TbBookImage tbBookImage = new TbBookImage();
                //生成code值
                tbBookImage.setCode(idWorker.nextId());
                tbBookImage.setBookImagesUrl(filePath + fileName);
                tbBookImage.setCreateTime(new Date());
                //添加操作
                tbBookImageMapper.insert(tbBookImage);
                //判断文件是不是添加成功
                if (tbBookImage.getId() == null) {
                    return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "文件上传失败");
                }
                list.add(tbBookImage.getCode());
                LOGGER.info(fileName+"----上传成功");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        resultMap.put("list",list);
        result.setResult(resultMap);
        return result;
    }
}
