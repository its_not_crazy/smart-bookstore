package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbOrder;
import com.example.wisdombookstore.entity.TbUser;
import com.example.wisdombookstore.util.ResponseResult;
/**
 * @Description: 图书订单Service
 * @Author: 宋合
 * @Date: 2020/10/20 12:57
 **/
public interface IOrderService extends IService<TbOrder> {


    /**
     * TODO &宋合& 取消订单功能
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 11:19
     * @param tbOrderCode: 
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult cancelBookOrder(Long tbOrderCode);

    /**
     * TODO &宋合& 生成订单
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 11:20
     * @param tbShoppingCartCode: 购物车标识
     * @return: java.lang.Object
     **/
    public ResponseResult createBookOrder(Long tbShoppingCartCode);

    /**
     * TODO &宋合& 
     * @Description: 查询所有订单
     * @Author: 宋合
     * @Date: 2020/10/27 18:33
     * @param tbUserCode:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult bookOrderList(Long tbUserCode);

    /**
     * TODO &宋合&
     * @Description: 查询单个订单
     * @Author: 宋合
     * @Date: 2020/10/27 19:14
     * @param tbOrderCode: 订单标识
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult bookOrder(Long tbOrderCode);


    /**
     * TODO &宋合& getBookOrderList 查询所有订单
     * @Description:
     * @Author: 宋合
     * @Date: 2020/11/1 15:55
     * @param status:
     * @param pageCurrent:
     * @param pageSize:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    ResponseResult getBookOrderList(Integer status, Integer pageCurrent, Integer pageSize);
}
