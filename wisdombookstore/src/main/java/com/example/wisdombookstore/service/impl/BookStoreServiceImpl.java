package com.example.wisdombookstore.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.wisdombookstore.entity.TbBookStore;
import com.example.wisdombookstore.entity.TbClerk;
import com.example.wisdombookstore.mapper.TbBookStoreMapper;
import com.example.wisdombookstore.mapper.TbClerkMapper;
import com.example.wisdombookstore.service.IBookStoreService;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Author: MaHan
 * @Date: Created in 11:08 2020/10/22
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: IBookStoreServiceImpl
 */
@Service
public class BookStoreServiceImpl implements IBookStoreService {
    /**
     * TODO &马晗& 查询所有的书店数据
     * @param currentPage: 当前页数
     * @param pageSize:  每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult 
     * @Author &马晗&
     * @Description
     * @Date 19:40 2020/10/22
     **/
    @Override
    public ResponseResult findAllBookStore(Integer currentPage, Integer pageSize) {
        //提示信息
        ResponseResult result = new ResponseResult();
        //分页插件
        Page<TbBookStore> page = new Page<>(currentPage,pageSize);
        //mybatis puls 插件
        QueryWrapper<TbBookStore> queryWrapper = new QueryWrapper<>();
        //查询所有书店
        Page<TbBookStore> tbBookStorePage = this.tbBookStoreMapper.selectPage(page, queryWrapper);
        //循环遍历所有书店
        for (TbBookStore tbBookStore : tbBookStorePage.getRecords()){
            //获取书店的唯一标识
            Long tbClerkCode = tbBookStore.getTbClerkCode();
            QueryWrapper<TbClerk> queryWrapper1 = new QueryWrapper<>();
            //查询出所有书店的员工
            queryWrapper1.lambda().eq(TbClerk::getCode,tbClerkCode);
            //查询书店店长
            List<TbClerk> tbClerks = tbClerkMapper.selectList(queryWrapper1);
            for (TbClerk tbClerk : tbClerks){
                //判断是否是店长
                if(tbClerk.getPermissions().equals(StateConstant.TBCLERK_PERMISSIONS_STOREMANAGER)){
                    tbBookStore.setTbClerk(tbClerk);
                }
            }
        }
        result.setResult(tbBookStorePage);
        return result;
    }

    @Override
    public ResponseResult findAllBookStore() {
        //提示信息
        ResponseResult result = new ResponseResult();
        //mybatis puls 插件
        QueryWrapper<TbBookStore> queryWrapper = new QueryWrapper<>();
        //查询所有书店
        List<TbBookStore> tbBookStores = this.tbBookStoreMapper.selectList(queryWrapper);
        //循环遍历所有书店
        for (TbBookStore tbBookStore : tbBookStores){
            //获取书店的唯一标识
            Long tbClerkCode = tbBookStore.getTbClerkCode();
            QueryWrapper<TbClerk> queryWrapper1 = new QueryWrapper<>();
            //查询出所有书店的员工
            queryWrapper1.lambda().eq(TbClerk::getCode,tbClerkCode);
            //查询书店店长
            List<TbClerk> tbClerks = tbClerkMapper.selectList(queryWrapper1);
            for (TbClerk tbClerk : tbClerks){
                //判断是否是店长
                if(tbClerk.getPermissions().equals(StateConstant.TBCLERK_PERMISSIONS_STOREMANAGER)){
                    tbBookStore.setTbClerk(tbClerk);
                }
            }
        }
        result.setResult(tbBookStores);
        return result;
    }

    /**
     * TODO &马晗& 添加门店信息
     * @param tbBookStore:  书店实体类
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 19:46 2020/10/22
     **/
    @Override
    public ResponseResult addBookStore(TbBookStore tbBookStore) {
        System.out.println(tbBookStore.getTbClerkCode()+"---------------");
        //设置返回信息
        ResponseResult result = new ResponseResult();
        //进行地址查询
        QueryWrapper<TbBookStore> qw = new QueryWrapper<>();
        qw.lambda().eq(TbBookStore::getAddress, tbBookStore.getAddress());
        //根据地址查询单个书店
        TbBookStore tbBookStore1 = this.tbBookStoreMapper.selectOne(qw);
        //判断书店存不存在
        if(tbBookStore1 != null){
            result.setFailMessage("书店已存在");
            return result;
        }
        //赋值code的值
        tbBookStore.setCode(idWorker.nextId());
        tbBookStore.setCreateTime(new Date());
        tbBookStore.setUpdateTime(new Date());
        //根据店员的唯一标识查询出店员
        QueryWrapper<TbClerk> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(TbClerk::getCode, tbBookStore.getTbClerkCode());
        //根据店员code查询店员
        TbClerk tbClerk = this.tbClerkMapper.selectOne(queryWrapper);
        //设置新加书店的标识
        tbClerk.setBootStoreCode(tbBookStore.getCode());
        //设置店长
        tbClerk.setPermissions(StateConstant.TBCLERK_PERMISSIONS_STOREMANAGER);
        this.tbClerkMapper.updateById(tbClerk);
        //添加书店信息
        int insert = this.tbBookStoreMapper.insert(tbBookStore);
        //判断书店是否添加成功
        if(insert == 1){
            result.setResult(tbBookStore1);
            return result;
        }else {
            return result.setFailMessage("书店添加失败");
        }
    }
    /**
     * TODO &马晗& 根据书店唯一标识查询书店
     * @param code:  书店唯一标识
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  根据书店唯一标识查询出所有书店 进行回显
     * @Date 9:43 2020/10/23
     **/
    @Override
    public ResponseResult findByIdBookStore(Long code) {
        //返回消息
        ResponseResult result = new ResponseResult();
        //根据书店唯一标识查询书店
        QueryWrapper<TbBookStore> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(TbBookStore::getCode,code);
        //查询唯一书店
        TbBookStore tbBookStore = this.tbBookStoreMapper.selectOne(queryWrapper);
        //判断书店存不存在 如果不存在直接提示消息，如果存在赋值 返回
        if(tbBookStore !=null){
            //如果存在赋值 返回
            result.setResult(tbBookStore);
            return result;
        }else {
            //如果不存在直接提示消息
            return result.setFailMessage("没有该书店");
        }
    }

    /**
     * TODO &马晗& 删除门店(0营业，1未营业）
     * @param code:
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description 删除书店显示未营业
     * @Date 9:10 2020/10/23
     **/
    @Override
    public ResponseResult delBookStore(Long code) {
        //返回值信息
        ResponseResult result = new ResponseResult();
        //根据书店唯一标识查询书店
        QueryWrapper<TbBookStore> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(TbBookStore::getCode,code);
        //查询单个书店
        TbBookStore tbBookStore = this.tbBookStoreMapper.selectOne(queryWrapper);
        //判断书店存不存在
        if(tbBookStore == null){
            result.setFailMessage("没有该门店");
            return result;
        }
        //存在将书店的状态修改为未营业
        tbBookStore.setStatus(StateConstant.TBBOOKSTORE_STATUS_CLOSED);
        this.tbBookStoreMapper.updateById(tbBookStore);
        //返回数据
        result.setResult(tbBookStore);
        return result;
    }

    /**
     * TODO &马晗& 查询所有店员
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  查询所有书店 将书店所有信息返回前端
     * @Date 9:41 2020/10/23
     **/
    @Override
    public ResponseResult findAllClerk() {
        //返回信息
        ResponseResult result = new ResponseResult();
        QueryWrapper<TbClerk> qw = new QueryWrapper<>();
        qw.eq("permissions",1)
                .eq("status",0);
        //查询所有店员集合
        List<TbClerk> tbClerks = this.tbClerkMapper.selectList(qw);
        //判断集合大小是否==0 如果是提示没有店员
        if(tbClerks.size() == StateConstant.HAVEHOT){

            result.setFailMessage("没有店员存在");
        }
        result.setResult(tbClerks);
        return result;

        
    }









    @Autowired
    private TbBookStoreMapper tbBookStoreMapper;
    @Autowired
    private TbClerkMapper tbClerkMapper;
    @Autowired
    private IdWorker idWorker;
}
