package com.example.wisdombookstore.service.impl;

import com.example.wisdombookstore.entity.TbBrakeMachine;
import com.example.wisdombookstore.service.IHardwareModuleService;
import com.example.wisdombookstore.util.ResponseResult;
import org.springframework.stereotype.Service;

/**
 * @Author: MaHan
 * @Date: Created in 11:19 2020/10/22
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: IHardwareModuleServiceImpl
 */
@Service
public class HardwareModuleServiceImpl implements IHardwareModuleService {

    @Override
    public ResponseResult hotPursuit() {
        return null;
    }

    @Override
    public ResponseResult theThermalAnalysis() {
        return null;
    }

    @Override
    public ResponseResult callThePolice() {
        return null;
    }

    @Override
    public ResponseResult rfidScanning() {
        return null;
    }

    @Override
    public ResponseResult photograph() {
        return null;
    }

    @Override
    public ResponseResult brakeMachine(Long code) {
        return null;
    }


}
