package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbUser;
import com.example.wisdombookstore.util.ResponseResult;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * TODO IUserService 用户接口Service
 * @ClassName IUserService
 * @Description
 * @Author 8aceMak1r
 * @Date 2020/10/20 8:46
 * @Version 1.0
 */
public interface IUserService extends IService<TbUser> {


   /**
    * TODO &宋合& 手机发送验证码
    * @Description:
    * @Author: 宋合
    * @Date: 2020/10/22 19:11
    * @param phone: 
    * @return: com.example.wisdombookstore.util.ResponseResult
    **/
    public ResponseResult sendMobileVerificationCode(String phone);

    /**
     * TODO &宋合& 手机注册
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:41
     * @param phone:  手机号
     * @param verificationCode: 验证码
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult telRegister(String phone,String verificationCode);

    /**
     * TODO &宋合& 微信注册
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:44
     * @param phone: 手机号
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult wechatRegister(String phone);

   /**
    * TODO &宋合& 手机号登录
    * @Description:
    * @Author: 宋合
    * @Date: 2020/10/22 10:47
    * @param user: 
    * @return: com.example.wisdombookstore.util.ResponseResult
    **/
    public ResponseResult telLogin(TbUser user);

   /**
    * TODO &宋合&
    * @Description: 微信登录
    * @Author: 宋合
    * @Date: 2020/10/23 9:04
    * @param wechatId: 
    * @return: com.example.wisdombookstore.util.ResponseResult
    **/
    public ResponseResult wechatLogin(String wechatId);

    /**
     * TODO &宋合& 生成二维码
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:48
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult createQrcode(String phone);

    /**
     * TODO &宋合& 人脸识别功能
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:51
     * @param faceUrl: 
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
     public ResponseResult faceRecognition(String faceUrl);

   /**
    * TODO &宋合&  支付
    * @Description:
    * @Author: 宋合
    * @Date: 2020/10/22 10:57
    * @param tbOrderCode: 
    * @param money:
    * @param payCode:
    * @return: com.example.wisdombookstore.util.ResponseResult
    **/
    public ResponseResult payBooks(Long tbOrderCode,double money,String payCode);

    /**
     * TODO 宋合 getUserList 查看所有会员
     * @param pageSize 总页数
     * @param pageCurrent 当前页
     * @Description: 查看所有会员
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 宋合
     * @date 2020/10/22 11:51
     */
    public ResponseResult getUserList(Integer pageSize,Integer pageCurrent);

    /**
     * TODO 宋合 getUserByUserId 查看会员详情
     * @param tbUserCode 会员信息
     * @Description: 查看会员详情信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 宋合
     * @date 2020/10/22 11:20
     */
    public ResponseResult getUserById(Long tbUserCode);

    /**
     * TODO 宋合 setClerkById 修改会员信息
     * @param user 会员详细
     * @Description: 修改店员信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 宋合
     * @date 2020/10/22 11:22
     */
    public ResponseResult setUserById(TbUser user);


    /**
     * TODO &宋合& 完善个人信息
     * @Description: 完善个人信息
     * @Author: 宋合
     * @Date: 2020/10/23 19:24
     * @param user:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult addUserById(TbUser user);
    /**
     * TODO 宋合 delUserById 停用会员
     * @param user 店员
     * @Description: 停用会员
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 宋合
     * @date 2020/10/22 11:28
     */
    public ResponseResult delUserById(Long user);


 /**
  * TODO 8aceMak1r faceAuthentication 人脸认证
  * @Description: 用户将自己的账号进行人脸认证
  * @return com.example.windstorm.util.ResponseResult
  * @param imageBast64 用户人脸信息
  * @param model
  * @param request
  * @param userCode 用户code
  * @throws
  * @author 8aceMak1r
  * @date 2020/10/28 8:54
  */
  public String faceAuthentication(StringBuffer imageBast64, Model model, HttpServletRequest request, Long userCode) throws IOException;

  /**
   * TODO 8aceMak1r searchFace 人脸登录
   * @param imageBast64  照片信息
   * @param model
   * @param request
   * @Description:
   * @return java.lang.String
   * @throws
   * @author 8aceMak1r
   * @date 2020/10/28 20:47
   */
  public String searchFace(StringBuffer imageBast64, Model model, HttpServletRequest request) throws IOException;

 ResponseResult upload(MultipartFile file);

 ResponseResult download(String image, HttpServletResponse response);

 ResponseResult ossUpload(MultipartFile file);
}
