package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbReplenishmentParticular;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @ClassName IReplenishmentParticularService 补货任务详情
 * @Description TODO 8aceMak1r IReplenishmentParticularService
 * @author 8aceMak1r
 * @date 2020/10/22 14:49
 * @Version 1.0
 * */
public interface IReplenishmentParticularService extends IService<TbReplenishmentParticular> {

    /**
     * TODO 8aceMak1r getReplenishmentParticular 查看任务详情
     * @param tbReplenishmentResultCode 结果的code
     * @Description: 查看任务的详情
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/26 19:26
     */
    ResponseResult getReplenishmentParticular(Long tbReplenishmentResultCode);
}
