package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbBook;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @Description: 图书Service
 * @Author: 宋合
 * @Date: 2020/10/20 12:57
 **/

public interface IBookService extends IService<TbBook> {

    /**
     * 添加图书信息
     * @Author 李慧龙
     * @Description //TODO addTbBook
     * @Date 8:41 2020/10/23
     * @param tbBook:
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult addTbBook(TbBook tbBook);

   /**
    * 删除图书的信息
    * @Author 李慧龙
    * @Description //TODO deleteTbBook
    * @Date 10:14 2020/10/22
    * @param tbBook: 图书的code值
    * @return com.example.wisdombookstore.util.ResponseResult
    */
    ResponseResult deleteTbBook(TbBook tbBook);

   /**
    * 修改图书信息
    * @Author 李慧龙
    * @Description //TODO updateTbBook
    * @Date 10:14 2020/10/22
    * @param tbBook: 图书的信息
    * @return com.example.wisdombookstore.util.ResponseResult
    */
    ResponseResult updateTbBook(TbBook tbBook);

    /**
     * 查询所有图书信息
     * @Author 李慧龙
     * @Description //TODO findAllTbBook
     * @Date 10:13 2020/10/22
     * @param currentPage: 当前页
     * @param pageSize: 每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult findAllTbBook(Integer currentPage, Integer pageSize);

    /**
     * 根据图书的code查询图书信息
     * @Author 李慧龙
     * @Description //TODO findOneTbBookByCode
     * @Date 10:13 2020/10/22
     * @param tbBook: 图书的code值
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult findOneTbBookByCode(TbBook tbBook);


    ResponseResult findAllBook();
}
