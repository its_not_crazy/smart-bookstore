package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.TbBookType;
import com.example.wisdombookstore.mapper.TbBookTypeMapper;
import com.example.wisdombookstore.service.IBookTypeService;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author 李慧龙
 * @date 2020/10/22 10:25
 * @className BookTypeServiceImpl
 */
@Service
public class BookTypeServiceImpl extends ServiceImpl<TbBookTypeMapper, TbBookType> implements IBookTypeService {
    @Resource
    TbBookTypeMapper tbBookTypeMapper;

    @Resource
    IdWorker idWorker;

    /**
     * 添加书籍类别
     *
     * @param tbBookType:
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO addTbBookType
     * @Date 20:31 2020/10/22
     */
    @Override
    public ResponseResult addTbBookType(TbBookType tbBookType) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断图书信息是否为空
        if (tbBookType == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写书籍类别信息");
        }
        //根据类别的名称已经状态判断是否存在
        TbBookType tbBookType1 = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getTypeName, tbBookType.getTypeName()).eq(TbBookType::getIsDel, 0));
        if (tbBookType1 != null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍类别信息已经存在");
        }
        //执行添加操作
        tbBookType.setCode(idWorker.nextId());
        tbBookType.setCreateTime(new Date());
        tbBookTypeMapper.insert(tbBookType);
        //根据数据的id来判断是否添加成功
        if (tbBookType.getId() == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "添加失败");
        }
        resultMap.put("tbBookType", tbBookType);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 删除书籍类别
     *
     * @param tbBookType:
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO deleteTbBookType
     * @Date 20:32 2020/10/22
     */
    @Override
    public ResponseResult deleteTbBookType(TbBookType tbBookType) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断图书信息是否为空
        if (tbBookType == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写书籍类别信息");
        }
        //判断类别是否存在
        TbBookType tbBookType1 = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookType.getCode()));
        if (tbBookType1 == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "该类别不存在");
        }
        //判断类是否已经删除
        if (tbBookType1.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "该类别已经删除");
        }
        //修改类别状态
        tbBookType1.setIsDel(StateConstant.OBJECT_DEL_OMIT);
        tbBookType1.setUpdateTime(new Date());
        tbBookTypeMapper.updateById(tbBookType1);
        resultMap.put("tbBookType", tbBookType1);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 修改类别信息
     *
     * @param tbBookType:
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO updateTbBookType
     * @Date 20:38 2020/10/22
     */
    @Override
    public ResponseResult updateTbBookType(TbBookType tbBookType) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断图书信息是否为空
        if (tbBookType == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写书籍类别信息");
        }
        //判断类别是否存在
        TbBookType tbBookType1 = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookType.getCode()));
        if (tbBookType1 == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "类别不存在");
        }
        //判断类是否已经删除
        if (tbBookType1.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "该类别已经删除");
        }
        //修改类别信息
        tbBookType.setId(tbBookType1.getId());
        tbBookType.setUpdateTime(new Date());
        tbBookTypeMapper.updateById(tbBookType);
        resultMap.put("tbBookType", tbBookType);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findAllTbBookType(Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (currentPage.equals(null)) {
            currentPage = 0;
        }
        if (pageSize.equals(null)) {
            currentPage = 2;
        }
        //分页查询
        Page<TbBookType> page = new Page<>(currentPage, pageSize);
        Page<TbBookType> iPage = tbBookTypeMapper.selectPage(page, new QueryWrapper<TbBookType>());
        resultMap.put("iPage", iPage);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findAll() {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        List<TbBookType> tbBookTypes = tbBookTypeMapper.selectList(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getIsDel,0));
        resultMap.put("tbBookTypes", tbBookTypes);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findAllTbBookType() {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        List<TbBookType> tbBookTypes = tbBookTypeMapper.selectList(null);
        resultMap.put("tbBookTypes", tbBookTypes);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findOneTbBookTypeByCode(TbBookType tbBookType) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (tbBookType == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写书籍信息");
        }
        //根据code值查询判断是否存在
        TbBookType tbBookType1 = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookType.getCode()));
        if (tbBookType1 == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "类别信息不存在");
        }
        //判短类别信息是否删除
        if (tbBookType1.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "类别信息已经删除");
        }
        resultMap.put("tbBookType", tbBookType1);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public boolean saveBatch(Collection<TbBookType> entityList) {
        return false;
    }
}
