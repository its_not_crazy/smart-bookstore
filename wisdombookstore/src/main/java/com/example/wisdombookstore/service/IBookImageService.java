package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbBookImage;
import com.example.wisdombookstore.entity.TbUser;
import com.example.wisdombookstore.util.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 李慧龙
 * @date 2020/10/23 10:11
 * @className IBookImageService
 */
public interface IBookImageService extends IService<TbBookImage> {

    /**
     * 添加书籍照片信息
     * @Author 李慧龙
     * @Description //TODO addTbBookInfoImages
     * @Date 9:47 2020/10/23
     * @param file:书籍照片
     * @return boolean
     */
    @PostMapping("/addTbBookInfoImages")
    ResponseResult addTbBookInfoImages(MultipartFile[] file);
}
