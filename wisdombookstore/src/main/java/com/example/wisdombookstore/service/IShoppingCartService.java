package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbShoppingCart;
import com.example.wisdombookstore.entity.TbUser;
import com.example.wisdombookstore.util.ResponseResult;

import java.util.HashMap;

/**
 * @Description: 购物车Service
 * @Author: 陈阳
 * @Date: 2020/10/20 12:57
 **/
public interface IShoppingCartService extends IService<TbShoppingCart> {


   /**
    * TODO &宋合&
    * @Description: 添加订单
    * @Author: 宋合
    * @Date: 2020/10/27 15:26
    * @param rfId: 
    * @param tbUserCode: 
    * @return: com.example.wisdombookstore.util.ResponseResult
    **/
    public ResponseResult addShopping(String rfId,Long tbUserCode);

    /**
     * TODO updateShopping 修改购物车书籍
    * @Description: updateShopping
    * @Param: * @param tbShoppingCart:  （code ：  需要修改此条购物消息的唯一标识
     *                                    tbUserCode： 用户唯一标识；
     *                                    tbbookCode ： 图书唯一标识 ）
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/22
    * @Time: 10:03
    */
    public ResponseResult updateShopping(TbShoppingCart tbShoppingCart);

   /**
    * TODO &宋合&
    * @Description:
    * @Author: 宋合
    * @Date: 2020/10/27 15:34
    * @param tbUserCode: 用户标识
    * @return: com.example.wisdombookstore.util.ResponseResult
    **/
    public ResponseResult pageFindUserAllShopping(Long tbUserCode);




}
