package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbBookType;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @author 李慧龙
 * @date 2020/10/22 10:00
 * @className IBookType
 * 图书类别的service层
 */
public interface IBookTypeService extends IService<TbBookType> {

    /**
     * 添加书记类别信息
     * @Author 李慧龙
     * @Description //TODO addTbBookType
     * @Date 10:12 2020/10/22
     * @param tbBookType: 书籍类别信息
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult addTbBookType(TbBookType tbBookType);

    /**
     * 删除书籍类别信息
     * @Author 李慧龙
     * @Description //TODO deleteTbBookType
     * @Date 10:12 2020/10/22
     * @param tbBookType: 书籍类别信息
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult deleteTbBookType(TbBookType tbBookType);

    /**
     * 修改书籍类别信息
     *
     * @param tbBookType: 书籍类别信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO updateTbBookType
     * @Date 10:08 2020/10/22
     */
    ResponseResult updateTbBookType(TbBookType tbBookType);

    /**
     * 查询所以的书籍类别信息
     *
     * @param currentPage: 当前页
     * @param pageSize:    每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO findAllTbBookType
     * @Date 10:08 2020/10/22
     */
    ResponseResult findAllTbBookType(Integer currentPage, Integer pageSize);

    /*
    * TODO 陈阳   查询所有的书籍类型
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/10/27 19:53

     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    ResponseResult findAllTbBookType();

    /**
     * 根据书籍类别的code值查询书籍类别信息
     *
     * @param tbBookType:书籍类别的code值
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO findOneTbBookTypeByCode
     * @Date 10:07 2020/10/22
     */
    ResponseResult findOneTbBookTypeByCode(TbBookType tbBookType);

    ResponseResult findAll();
}
