package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.TbCron;
import com.example.wisdombookstore.mapper.TbCronMapper;
import com.example.wisdombookstore.service.ICromService;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @program: wisdombookstore
 * @description: 定时任务实现类
 * @author: 陈阳
 * @create: 2020-10-28 22:07
 */
@Service
@Slf4j
public class CromServiceImpl extends ServiceImpl<TbCronMapper, TbCron> implements ICromService {

    @Resource
    private TbCronMapper tbCronMapper;

    @Resource
    private IdWorker idWorker;


    /**
    * TODO 陈阳    添加最新的盘点定时规则
    *
     * 1      每一秒
     * 2      每三秒
     * 3      每分钟
     * 4      每三天执行一次
     * 5      每天的下午四点
     *
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/10/28 22:20
     * @param crom:    定时任务规则
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @Override
    public ResponseResult addCrom(Integer crom) {
        String a = "";
        switch (crom){
            case 1:
                a = "0/1 * * * * ?";
                break;
            case 2:
                a = "0/3 * * * * ?";
                break;
            case 3:
                a = "0 0/1 * * * ?";
                break;
            case 4:
                a = "0 0 0 1/3 0 ? *";
                break;
            case 5:
                a = "* 00 16 * * *";
                break;

        }
        TbCron tbCron = new TbCron();
        tbCron.setId(null);
        tbCron.setCron(String.valueOf(idWorker.nextId()));
        tbCron.setIsDel(StateConstant.OBJECT_DEL_NORMAL);
        tbCron.setCreateTime(new Date());
        tbCron.setUpdateTime(null);
        tbCron.setCron(a);
        int insert = tbCronMapper.insert(tbCron);
        if(insert==1){
            return new ResponseResult(true,1000,"添加成功");
        }
        return new ResponseResult(false,-1,"添加失败");
    }
}
