package com.example.wisdombookstore.service;

import com.example.wisdombookstore.entity.TbBookStore;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @Author: MaHan
 * @Date: Created in 11:08 2020/10/22
 * @Description: 书店service
 * @program: IntelliJ IDEA
 * @ClassName: IBookStoreServiceImpl
 */
public interface IBookStoreService {
    /**
     * TODO &马晗&  查询所有书店信息
     * @param currentPage:  当前页
     * @param pageSize:   每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 10:25 2020/10/22
     **/
    public ResponseResult findAllBookStore(Integer currentPage,Integer pageSize);

    /**
     * TODO &马晗&  查询所有书店信息
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 10:25 2020/10/22
     **/
    public ResponseResult findAllBookStore();
    /**
     * TODO &马晗& 添加书店信息
     * @param tbBookStore: 书店实体
     * @return: com.example.wisdombookstore.util.ResponseResult 
     * @Author &马晗&
     * @Description 
     * @Date 10:07 2020/10/22
     **/
    public ResponseResult addBookStore(TbBookStore tbBookStore);

    /**
     * TODO &马晗& 根据书店标识查询书店信息
     * @param  code: 书店标识
     * @return: com.example.wisdombookstore.util.ResponseResult 
     * @Author &马晗&
     * @Description 
     * @Date 10:11 2020/10/22
     **/
    public ResponseResult findByIdBookStore(Long code);


    /**
     * TODO &马晗& 删除书店信息
     * @param code:书店标识
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  根据书店标识删除书店基本信息（执行软删除）
     * @Date 10:13 2020/10/22
     **/
    public ResponseResult delBookStore(Long code);

    /**
     * TODO &马晗& 查询所有店员
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 19:48 2020/10/22
     **/
    public ResponseResult findAllClerk();
}
