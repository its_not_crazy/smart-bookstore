package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbOrder;
import com.example.wisdombookstore.entity.TbOrdersDetail;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @Description: 图书订单Service
 * @Author: 宋合
 * @Date: 2020/10/20 12:57
 **/
public interface IOrdersDetailService extends IService<TbOrdersDetail> {



}
