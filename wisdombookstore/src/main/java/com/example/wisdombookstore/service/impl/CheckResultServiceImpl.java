package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.wisdombookstore.entity.TbBookStore;
import com.example.wisdombookstore.entity.TbCheckDeficiency;
import com.example.wisdombookstore.entity.TbCheckResult;
import com.example.wisdombookstore.entity.TbClerk;
import com.example.wisdombookstore.mapper.TbCheckDeficiencyMapper;
import com.example.wisdombookstore.mapper.TbCheckResultMapper;
import com.example.wisdombookstore.service.ICheckResultService;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.jws.WebResult;
import java.util.HashMap;
import java.util.List;

/**
 * @Author: MaHan
 * @Date: Created in 15:54 2020/10/24
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: CheckResultServiceImpl
 */
@Service
public class CheckResultServiceImpl implements ICheckResultService {
    @Autowired
    private TbCheckResultMapper tbCheckResultMapper;

    @Resource
    private TbCheckDeficiencyMapper tbCheckDeficiencyMapper;


    /**
     * TODO &马晗&  盘点结果
     * @param currentPage:  当前页
     * @param pageSize: 每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  查询出所有盘点结果
     * @Date 16:10 2020/10/24
     **/
    @Override
    public ResponseResult inventoryResults(Integer currentPage, Integer pageSize) {
        //提示信息
        ResponseResult result = new ResponseResult();
        //分页插件
        Page<TbCheckResult> page = new Page<>(currentPage,pageSize);
        //mybatis puls 插件
        QueryWrapper<TbCheckResult> queryWrapper = new QueryWrapper<>();
        //查询所有盘点结果
        Page<TbCheckResult> tbCheckResultPage = this.tbCheckResultMapper.selectPage(page, queryWrapper);
        System.out.println(tbCheckResultPage);
        //返回结果
        result.setResult(tbCheckResultPage);
        return result;
    }

    @Override
    public ResponseResult inventoryResults(Long code) {
        //提示信息
        ResponseResult result = new ResponseResult();
        //mybatis puls 插件
        LambdaQueryWrapper<TbCheckResult> eq = new QueryWrapper<TbCheckResult>()
                .lambda()
                .eq(TbCheckResult::getTbCheckTaskCode, code)
                .last("limit 1");
        //查询所有盘点结果
        List<TbCheckResult> tbCheckResults = this.tbCheckResultMapper.selectList(eq);
        //返回结果
        result.setResult(tbCheckResults);
        return result;
        //我是你爹，我是你爹，我是你爹，我是你爹,陈阳他爹
    }

    @Override
    public ResponseResult inventoryResultsDis(Long code) {
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
        LambdaQueryWrapper<TbCheckDeficiency> eq = new QueryWrapper<TbCheckDeficiency>()
                .lambda()
                .eq(TbCheckDeficiency::getTbCheckTaskCode, code);
        List<TbCheckDeficiency> tbCheckDeficiencies = this.tbCheckDeficiencyMapper.selectList(eq);

        return new ResponseResult(true,StateConstant.RESPONSERESULT_CODE_TRUE,"查询成功",tbCheckDeficiencies);
    }
}
