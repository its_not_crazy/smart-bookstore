package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.*;
import com.example.wisdombookstore.mapper.RlBookTypeInfomMapper;
import com.example.wisdombookstore.mapper.TbShoppingBookMapper;
import com.example.wisdombookstore.mapper.TbShoppingCartMapper;
import com.example.wisdombookstore.mapper.TbShoppingGoodsMapper;
import com.example.wisdombookstore.service.*;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 购物车service实现类
 * @date 2020/10/22 10:27
 */
@Service
@Slf4j
public class ShoppingCartServiceImpl extends ServiceImpl<TbShoppingCartMapper, TbShoppingCart> implements IShoppingCartService {

    @Autowired
    private TbShoppingBookMapper tbShoppingBookMapper;

    @Autowired
    private TbShoppingGoodsMapper tbShoppingGoodsMapper;

    @Resource
    private IdWorker idWorker;

    @Autowired
    private IShoppingCartService iShoppingCartService;

    @Resource
    private IUserService iUserService;

    @Resource
    private IBookService iBookService;

    @Resource
    private IBookInfoService iBookInfoService;

    @Resource
    private RlBookTypeInfomMapper rlBookTypeInfomMapper;

    @Resource
    private IBookTypeService iBookTypeService;

    @Autowired
    private IBookImageService iBookImageService;

    @Override
    public ResponseResult addShopping(String rfId, Long tbUserCode) {
        ResponseResult responseResult = new ResponseResult();
        QueryWrapper<TbBook> tbBookQueryWrapper = new QueryWrapper<>();
        //根据rfid获取图书信息
        tbBookQueryWrapper.lambda().eq(TbBook::getRfIdCode, rfId);
        TbBook tbBookOne = iBookService.getOne(tbBookQueryWrapper);

        QueryWrapper<TbBookInfo> tbBookInfoQueryWrapper = new QueryWrapper<>();
        //根据该图书的书籍类型查询书籍详情
        tbBookInfoQueryWrapper.lambda().eq(TbBookInfo::getCode, tbBookOne.getTbBookInfoCode());
        TbBookInfo tbBookInfoOne = iBookInfoService.getOne(tbBookInfoQueryWrapper);
        //查询用户的购物车
        QueryWrapper<TbShoppingCart> tbShoppingCartQueryWrapper = new QueryWrapper<>();
        tbShoppingCartQueryWrapper.lambda().eq(TbShoppingCart::getTbUserCode, tbUserCode);
        TbShoppingCart tbShoppingCartOne = iShoppingCartService.getOne(tbShoppingCartQueryWrapper);
        //根据购物车标识和商品标识查询购物车商品
        QueryWrapper<TbShoppingGoods> tbShoppingGoodsQueryWrapperNew = new QueryWrapper<>();
        tbShoppingGoodsQueryWrapperNew.lambda().eq(TbShoppingGoods::getTbShoppingCartCode, tbShoppingCartOne.getCode())
                .eq(TbShoppingGoods::getTbBookInfoCode, tbBookOne.getTbBookInfoCode()).ne(TbShoppingGoods::getIsDel,StateConstant.OBJECT_DEL_OMIT);
        TbShoppingGoods tbShoppingGoods = tbShoppingGoodsMapper.selectOne(tbShoppingGoodsQueryWrapperNew);
        TbShoppingBook tbShoppingBook = new TbShoppingBook();
        //判断之前是否有对应购物车商品
        if (tbShoppingGoods != null) {
            //判断之前是否添加该商品信息
            //防止同一本书在一个购物车
            QueryWrapper<TbShoppingBook> tbShoppingBookQueryWrapper = new QueryWrapper<>();
            tbShoppingBookQueryWrapper.lambda()
                    .eq(TbShoppingBook::getTbShoppingGoodsCode, tbShoppingGoods.getCode())
                    .eq(TbShoppingBook::getTbBookCode, tbBookOne.getCode())
                    .ne(TbShoppingBook::getIsDel,StateConstant.OBJECT_DEL_OMIT);
            TbShoppingBook tbShoppingBookNew = tbShoppingBookMapper.selectOne(tbShoppingBookQueryWrapper);
            if (tbShoppingBookNew != null) {
                responseResult.setFailMessage("不能重复添加一本书");
                return responseResult;
            }

            tbShoppingBook.setCode(idWorker.nextId());
            tbShoppingBook.setTbBookCode(tbBookOne.getCode());
            tbShoppingBook.setTbShoppingGoodsCode(tbShoppingGoods.getCode());
            //给购物车单品添加单品
            tbShoppingBookMapper.insert(tbShoppingBook);
            //购物车商品总单品数量加1
            tbShoppingGoods.setNum(tbShoppingGoods.getNum() + 1);
            //更新购物车商品数据
            tbShoppingGoodsMapper.updateById(tbShoppingGoods);
            responseResult.setResult(tbShoppingCartOne);
            return responseResult;
        }
        //没有添加
        TbShoppingGoods tbShoppingGoodsNew = new TbShoppingGoods();
        tbShoppingGoodsNew.setNum(1);
        tbShoppingGoodsNew.setCode(idWorker.nextId());
        tbShoppingGoodsNew.setTbBookInfoCode(tbBookInfoOne.getCode());
        tbShoppingGoodsNew.setTbShoppingCartCode(tbShoppingCartOne.getCode());
        //添加购物车商品信息
        tbShoppingGoodsMapper.insert(tbShoppingGoodsNew);
        tbShoppingBook.setTbShoppingGoodsCode(tbShoppingGoodsNew.getCode());
        tbShoppingBook.setTbBookCode(tbBookOne.getCode());
        tbShoppingBook.setCode(idWorker.nextId());
        //添加单品信息
        tbShoppingBookMapper.insert(tbShoppingBook);
        responseResult.setResult(tbShoppingCartOne);
        return responseResult;

    }

    @Override
    public ResponseResult updateShopping(TbShoppingCart tbShoppingCart) {
        return null;
    }

    @Override
    public ResponseResult pageFindUserAllShopping(Long tbUserCode) {
        ResponseResult responseResult = new ResponseResult();
        QueryWrapper<TbShoppingCart> tbShoppingCartQueryWrapper = new QueryWrapper<>();
        tbShoppingCartQueryWrapper.lambda().eq(TbShoppingCart::getTbUserCode,tbUserCode);
        TbShoppingCart one = iShoppingCartService.getOne(tbShoppingCartQueryWrapper);
        if(one==null){
            responseResult.setFailMessage("购物车不存在");
            return responseResult;
        }
        QueryWrapper<TbShoppingGoods> tbShoppingGoodsQueryWrapper = new QueryWrapper<>();
        tbShoppingGoodsQueryWrapper.lambda().eq(TbShoppingGoods::getTbShoppingCartCode,one.getCode());
        //查询出该购物车的商品信息
        List<TbShoppingGoods> tbShoppingGoodsNew = tbShoppingGoodsMapper.selectList(tbShoppingGoodsQueryWrapper);
        //信息存放到购物车商品集合中


        //循环存放购物车商品单品信息
        for (TbShoppingGoods tbShoppingGoods: tbShoppingGoodsNew) {
            QueryWrapper<TbBookInfo> tbBookInfoQueryWrapper = new QueryWrapper<>();
            tbBookInfoQueryWrapper.lambda().eq(TbBookInfo::getCode,tbShoppingGoods.getTbBookInfoCode());
            TbBookInfo bookInfoOne = iBookInfoService.getOne(tbBookInfoQueryWrapper);
            QueryWrapper<TbBookImage> tbBookImageQueryWrapper = new QueryWrapper<>();
            tbBookImageQueryWrapper.lambda().eq(TbBookImage::getTbBookInfoCode,bookInfoOne.getCode());
            List<TbBookImage> list = iBookImageService.list(tbBookImageQueryWrapper);
            bookInfoOne.setTbBookImages(list);
            tbShoppingGoods.setTbBookInfo(bookInfoOne);
            QueryWrapper<TbShoppingBook> tbShoppingBookQueryWrapper = new QueryWrapper<>();
            tbShoppingBookQueryWrapper.lambda().eq(TbShoppingBook::getTbShoppingGoodsCode,tbShoppingGoods.getCode());
            //查询每个商品下面的单品信息
            List<TbShoppingBook> tbShoppingBooks = tbShoppingBookMapper.selectList(tbShoppingBookQueryWrapper);
            tbShoppingGoods.setTbShoppingBooks(tbShoppingBooks);
        }
        one.setTbShoppingGoods(tbShoppingGoodsNew);
        responseResult.setResult(one);
        return responseResult;
    }

}
