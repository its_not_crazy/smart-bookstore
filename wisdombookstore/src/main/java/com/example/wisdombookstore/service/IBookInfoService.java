package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbBookImage;
import com.example.wisdombookstore.entity.TbBookInfo;
import com.example.wisdombookstore.util.ResponseResult;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 李慧龙
 * @date 2020/10/22 9:32
 * 书籍的service层
 */
public interface IBookInfoService extends IService<TbBookInfo> {


    /**
     * 添加书籍的信息
     *
     * @param tbBookInfo: 书籍的信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO addTbBootInfo
     * @Date 10:15 2020/10/22
     */
    ResponseResult addTbBootInfo(TbBookInfo tbBookInfo,String tbBookImageCodes);

    /**
     * 删除书籍的信息
     *
     * @param tbBookInfo: 书籍的信息的code值
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO deleteTbBootInfo
     * @Date 10:15 2020/10/22
     */
    ResponseResult deleteTbBootInfo(TbBookInfo tbBookInfo);

    /**
     * 修改书籍的信息
     *
     * @param tbBookInfo: 书籍的信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO updateTbBootInfo
     * @Date 10:15 2020/10/22
     */
    ResponseResult updateTbBootInfo(TbBookInfo tbBookInfo, String imageStr);

    /**
     * 查询所有的书籍信息
     *
     * @param currentPage:当前页
     * @param pageSize:每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO findAllTbBootInfo
     * @Date 10:16 2020/10/22
     */
    ResponseResult findAllTbBootInfo(Integer currentPage, Integer pageSize);

    /**
     * 根据书籍的code值查询书籍信息
     *
     * @param tbBookInfo: 书籍信息的code值
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO findOnetbBookInfo
     * @Date 10:16 2020/10/22
     */
    ResponseResult findOnetbBookInfoByCode(TbBookInfo tbBookInfo);

    ResponseResult findAllInfo();

    ResponseResult findAllAll();
}
