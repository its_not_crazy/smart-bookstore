package com.example.wisdombookstore.service;
import com.example.wisdombookstore.util.ResponseResult;


/**
 * @Author: MaHan
 * @Date: Created in 11:08 2020/10/22
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: IBookStoreServiceImpl
 */
public interface IHardwareModuleService {


    /**
     * TODO &马晗& 热力追踪
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:17 2020/10/22
     **/
    public ResponseResult hotPursuit();

    /**
     * TODO &马晗& 热力分析
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:17 2020/10/22
     **/
    public ResponseResult theThermalAnalysis();

    /**
     * TODO &马晗& 报警功能
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:14 2020/10/22
     **/
    public ResponseResult callThePolice();

    /**
     * TODO &马晗& RFID扫描功能
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:14 2020/10/22
     **/
    public ResponseResult rfidScanning();

    /**
     * TODO &马晗& 预警拍照功能
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:13 2020/10/22
     **/
    public ResponseResult photograph();
    /**
     * TODO &马晗& 闸机开关
     * @param code:  闸机标识
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 2020/10/20 9:24
     * @Param []
     * @return ResponseResult
     */
    ResponseResult brakeMachine(Long code);



}
