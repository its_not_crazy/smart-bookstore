package com.example.wisdombookstore.service;

import com.example.wisdombookstore.util.ResponseResult;

/**
 * @Author: MaHan
 * @Date: Created in 15:49 2020/10/24
 * @Description: 盘点结果
 * @program: IntelliJ IDEA
 * @ClassName: ICheckResultService
 */
public interface ICheckResultService {
    /**
     * TODO &马晗& 盘点结果
     * @param currentPage: 当前页
     * @param pageSize:  每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  根据盘点任务进行盘点形成盘点结果
     * @Date 15:49 2020/10/24
     **/
    public ResponseResult inventoryResults(Integer currentPage, Integer pageSize);

    /**
     * TODO &马晗& 盘点结果
     * @param code:        盘点任务唯一标识
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  根据盘点任务进行盘点形成盘点结果
     * @Date 15:49 2020/10/24
     **/
    public ResponseResult inventoryResults(Long code);

    /**
    * TODO 陈阳   查询却是书籍
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/11/1 15:42
     * @param code:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult inventoryResultsDis(Long code);
}
