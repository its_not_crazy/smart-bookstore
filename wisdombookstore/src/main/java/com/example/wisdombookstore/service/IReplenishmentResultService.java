package com.example.wisdombookstore.service;

import com.example.wisdombookstore.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: MaHan
 * @Date: Created in 15:42 2020/10/24
 * @Description: 补货结果表
 * @program: IntelliJ IDEA
 * @ClassName: IReplenishmentResultService
 */
public interface IReplenishmentResultService {
    
    /**
     * TODO &马晗& 补货结果表
     * @param currentPage: 当前页
     * @param pageSize:  每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult 
     * @Author &马晗&
     * @Description  根据补货任务进行补货，形成补货结果。
     * @Date 15:46 2020/10/24
     **/
    public ResponseResult replenishmentResults(Integer currentPage,Integer pageSize);

    /**
     * TODO 8aceMak1r submitReplenishmentTask 提交补货任务
     *
     * @param tbReplenishmentResultCode
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description:
     * @author 8aceMak1r
     * @date 2020/10/26 18:54
     */
    public ResponseResult submitReplenishmentTask(Long tbReplenishmentResultCode);


    /**
     * TODO 8aceMak1r getReplenishmentResultCode 根据任务查询补货结果
     *
     * @param tbReplenishmentTaskCode
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description:
     * @author 8aceMak1r
     * @date 2020/10/26 18:54
     */
    public ResponseResult getReplenishmentResultCode(Long tbReplenishmentTaskCode);
}
