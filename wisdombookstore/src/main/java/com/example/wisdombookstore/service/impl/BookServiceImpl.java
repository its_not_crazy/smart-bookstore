package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.TbBook;
import com.example.wisdombookstore.entity.TbBookImage;
import com.example.wisdombookstore.entity.TbBookInfo;
import com.example.wisdombookstore.entity.TbBookType;
import com.example.wisdombookstore.mapper.TbBookImageMapper;
import com.example.wisdombookstore.mapper.TbBookInfoMapper;
import com.example.wisdombookstore.mapper.TbBookMapper;
import com.example.wisdombookstore.mapper.TbBookTypeMapper;
import com.example.wisdombookstore.service.IBookService;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author 李慧龙
 * @date 2020/10/22 10:24
 * @className BookServiceImpl
 */
@Service
public class BookServiceImpl extends ServiceImpl<TbBookMapper, TbBook> implements IBookService {

    /**
     * 图书mapper的注入
     */
    @Resource
    TbBookMapper tbBookMapper;
    @Resource
    IdWorker idWorker;
    @Resource
    TbBookInfoMapper tbBookInfoMapper;
    @Resource
    TbBookTypeMapper tbBookTypeMapper;
    @Resource
    TbBookImageMapper tbBookImageMapper;

    @Override
    public ResponseResult addTbBook(TbBook tbBook) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断图书信息是否为空
        if (tbBook == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写图书信息");
        }
        //根据Rfid判断是不是存在
        TbBook tbBook1 = tbBookMapper.selectOne(new QueryWrapper<TbBook>().lambda().eq(TbBook::getRfIdCode, tbBook.getRfIdCode()).eq(TbBook::getIsDel, 0));
        if (tbBook1 != null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "图书已经存在");
        }
        //执行添加图书操作
        TbBookInfo tbBookInfo = tbBookInfoMapper.selectOne(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getCode, tbBook.getTbBookInfoCode()));
        if (tbBookInfo == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍信息错误,没有该书籍");
        }
        //判断图书是否已经删除
        if (tbBookInfo.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "该书籍已经删除,请重新选择");
        }
        //修改书籍的数量信息(每添加一本书,书籍数量加一)
        tbBookInfo.setNumber(tbBookInfo.getNumber() + 1);
        tbBookInfoMapper.updateById(tbBookInfo);
        //添加图书操作
        tbBook.setTitle(tbBookInfo.getName());
        tbBook.setCode(idWorker.nextId());
        tbBook.setCreateTime(new Date());
        //图书默认状态 0未售卖
        tbBook.setStatus(StateConstant.TBBOOK_STSTUS_UNTREATED);
        tbBookMapper.insert(tbBook);
        //通过判断信息的id来判断是否添加成功
        if (tbBook.getId() == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "图书信息添加失败,请重新添加");
        }
        //返回添加成功信息
        resultMap.put("tbBook", tbBook);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult deleteTbBook(TbBook tbBook) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断传入参数是否为空
        if (tbBook == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写图书code信息");
        }
        //通过图书的code信息查询图书信息
        TbBook tbBook1 = tbBookMapper.selectOne(new QueryWrapper<TbBook>().lambda().eq(TbBook::getCode, tbBook.getCode()));
        if (tbBook1 == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "图书信息不存在");
        }
        //判断图书信息的状态  0正常  1删除
        if (tbBook1.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "图书信息已经删除");
        }
        //书籍数量减一
        TbBookInfo tbBookInfo = tbBookInfoMapper.selectOne(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getCode, tbBook1.getTbBookInfoCode()));
        tbBookInfo.setNumber(tbBookInfo.getNumber()-1);
        tbBookInfoMapper.updateById(tbBookInfo);
        //修改图书信息的状态
        tbBook1.setIsDel(StateConstant.OBJECT_DEL_OMIT);
        tbBook1.setUpdateTime(new Date());
        tbBookMapper.updateById(tbBook1);
        resultMap.put("tbBook", tbBook1);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult updateTbBook(TbBook tbBook) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断传入参数是否为空
        if (tbBook == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写图书信息");
        }
        //根据code值判断图书是否存在
        TbBook tbBook1 = tbBookMapper.selectOne(new QueryWrapper<TbBook>().lambda().eq(TbBook::getCode, tbBook.getCode()));
        if (tbBook1 == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "图书信息不存在");
        }
        //判断图书的状态
        if (tbBook1.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "图书信息已经删除");
        }
        TbBookInfo tbBookInfo = tbBookInfoMapper.selectOne(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getCode, tbBook.getTbBookInfoCode()));
        if (tbBookInfo == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍信息不存在");
        }
        //修改图书信息
        tbBook.setUpdateTime(new Date());
        tbBook.setId(tbBook1.getId());
        tbBookMapper.updateById(tbBook);
        //返回结果
        resultMap.put("tbBook", tbBook);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findAllTbBook(Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        HashMap<String, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (currentPage.equals(null)) {
            currentPage = 0;
        }
        if (pageSize.equals(null)) {
            currentPage = 2;
        }
        //分页查询
        Page<TbBook> objectPage = new Page<>(currentPage,pageSize);
        Page<TbBook> tbBookPage = tbBookMapper.selectPage(objectPage, new QueryWrapper<TbBook>());
        //带走书籍信息
        List<TbBook> records = tbBookPage.getRecords();
        for (TbBook book : records) {
            TbBookInfo tbBookInfo = tbBookInfoMapper.selectOne(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getCode, book.getTbBookInfoCode()));
            //根据书籍信息查询类别信息
            TbBookType tbBookType = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookInfo.getTbBookTypeCode()));
            //带走类别信息
            tbBookInfo.setTbBookType(tbBookType);
            //根据书籍信息查询图片信息
            List<TbBookImage> tbBookImages = tbBookImageMapper.selectList(new QueryWrapper<TbBookImage>().lambda().eq(TbBookImage::getTbBookInfoCode, tbBookInfo.getCode()));
            //带走
            tbBookInfo.setTbBookImages(tbBookImages);
            //带走书籍信息
            book.setTbBookInfo(tbBookInfo);
        }

        resultMap.put("iPage", tbBookPage);
        resultMap.put("total", tbBookPage.getTotal());
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findOneTbBookByCode(TbBook tbBook) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (tbBook == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写图书信息");
        }
        //根据code值查询判断是否存在
        TbBook tbBook1 = tbBookMapper.selectOne(new QueryWrapper<TbBook>().lambda().eq(TbBook::getCode, tbBook.getCode()));
        if (tbBook1 == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "图书信息不存在");
        }
        //判短图书信息是否删除
        if (tbBook1.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "图书信息已经删除");
        }
        resultMap.put("tbBook", tbBook1);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findAllBook() {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        List<TbBook> tbBooks = tbBookMapper.selectList(null);
        for (TbBook tbBook : tbBooks) {
            Long tbBookInfoCode = tbBook.getTbBookInfoCode();
            TbBookInfo tbBookInfo = tbBookInfoMapper.selectOne(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getCode, tbBookInfoCode));
            tbBook.setTbBookInfo(tbBookInfo);
            List<TbBookImage> tbBookImages = tbBookImageMapper.selectList(new QueryWrapper<TbBookImage>().lambda().eq(TbBookImage::getTbBookInfoCode, tbBookInfoCode));
            tbBookInfo.setTbBookImages(tbBookImages);
            TbBookType tbBookType = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookInfo.getTbBookTypeCode()));
            tbBookInfo.setTbBookType(tbBookType);
        }
        resultMap.put("tbBooks",tbBooks);
        result.setResult(resultMap);
        return result;
    }
}


