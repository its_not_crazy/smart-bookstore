package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.TbReplenishmentParticular;
import com.example.wisdombookstore.mapper.TbReplenishmentParticularMapper;
import com.example.wisdombookstore.service.IReplenishmentParticularService;
import com.example.wisdombookstore.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @ClassName ReplenishmentParticularServiceImpl
 * @Description TODO 8aceMak1r ReplenishmentParticularServiceImpl
 * @Author 8aceMak1r
 * @Date 2020/10/26 10:28
 * @Version 1.0
 */
@Service
public class ReplenishmentParticularServiceImpl extends ServiceImpl<TbReplenishmentParticularMapper, TbReplenishmentParticular> implements IReplenishmentParticularService {

    @Autowired
    private TbReplenishmentParticularMapper replenishmentParticularMapper;

    @Override
    public ResponseResult getReplenishmentParticular(Long tbReplenishmentResultCode) {
        ResponseResult result=ResponseResult.SUCCESS();
        HashMap<String, Object> resultMap = new HashMap<>();
        try {
            QueryWrapper<TbReplenishmentParticular> wrapper=new QueryWrapper<>();
            wrapper.lambda().eq(TbReplenishmentParticular::getTbReplenishmentResultCode,tbReplenishmentResultCode);
            List<TbReplenishmentParticular> particular = replenishmentParticularMapper.selectList(wrapper);
            if (particular==null){
                result.setFailMessage("没有此任务");
                return result;
            }else {
                resultMap.put("particular",particular);
                result.setMessage("操作成功");
                result.setSuccess(true);
                result.setResult(resultMap);
                result.setCode(1);
                return result;
            }
        }catch (Exception e){
            e.printStackTrace();
            result.setMessage("程序异常" + e.toString());
            result.setSuccess(false);
            return result;
        }
    }
}
