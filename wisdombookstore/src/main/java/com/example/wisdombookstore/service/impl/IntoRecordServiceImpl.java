package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.TbIntoRecord;
import com.example.wisdombookstore.entity.TbUser;
import com.example.wisdombookstore.mapper.TbIntoRecordMapper;
import com.example.wisdombookstore.mapper.TbUserMapper;
import com.example.wisdombookstore.service.IIntoRecordService;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * @author 李慧龙
 * @date 2020/10/24 16:19
 * @className IntoRecordServiceImpl
 */
@Service
@Transactional
public class IntoRecordServiceImpl extends ServiceImpl<TbIntoRecordMapper, TbIntoRecord> implements IIntoRecordService {
    @Resource
    TbIntoRecordMapper tbIntoRecordMapper;
    @Resource
    IdWorker idWorker;
    @Resource
    TbUserMapper tbUserMapper;

    /**
     * 添加用户进店记录
     *
     * @param tbIntoRecord:用户进店记录
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO 李慧龙 addIntoRecord
     * @Date 16:47 2020/10/24
     */
    @Override
    public ResponseResult addIntoRecord(TbIntoRecord tbIntoRecord) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断图书信息是否为空
        if (tbIntoRecord == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写进店记录信息");
        }
        //根据时间跟用户的code判断是不是重复
        TbIntoRecord tbIntoRecord1 = tbIntoRecordMapper.selectOne(new QueryWrapper<TbIntoRecord>().lambda().eq(TbIntoRecord::getIntoDate, tbIntoRecord.getIntoDate()).eq(TbIntoRecord::getUserCode, tbIntoRecord.getUserCode()));
        if (tbIntoRecord1 != null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "进店记录重复");
        }
        //判断传入的用户code值是不是合法的
        TbUser tbUser = tbUserMapper.selectOne(new QueryWrapper<TbUser>().lambda().eq(TbUser::getCode, tbIntoRecord.getUserCode()));
        if (tbUser == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "用户不存在");
        }
        //添加操作
        tbIntoRecord.setCode(idWorker.nextId());
        Date date = new Date();
        tbIntoRecord.setCreateTime(date);
        tbIntoRecord.setIntoDate(date);
        tbIntoRecordMapper.insert(tbIntoRecord);
        resultMap.put("tbIntoRecord", tbIntoRecord);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 查询所有进店记录
     *
     * @param currentPage:当前页
     * @param pageSize:每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findAllIntoRecord
     * @Date 18:36 2020/10/25
     */
    @Override
    public ResponseResult findAllIntoRecord(Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (currentPage == null) {
            currentPage = 1;
        }
        if (pageSize == null) {
            pageSize = 2;
        }
        Page<TbIntoRecord> page = new Page<>(currentPage, pageSize);
        Page<TbIntoRecord> iPage = tbIntoRecordMapper.selectPage(page, null);
        resultMap.put("iPage", iPage);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 根据用户code查询该用户的所有记录
     *
     * @param tbIntoRecord: 用户的code标识
     * @param currentPage:  当前页
     * @param pageSize:     每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findIntoRecordByUserCode
     * @Date 18:37 2020/10/25
     */
    @Override
    public ResponseResult findIntoRecordByUserCode(TbIntoRecord tbIntoRecord, Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (tbIntoRecord == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写用户的code标识");
        }
        if (currentPage == null) {
            currentPage = 1;
        }
        if (pageSize == null) {
            pageSize = 2;
        }
        TbUser tbUser = tbUserMapper.selectOne(new QueryWrapper<TbUser>().lambda().eq(TbUser::getCode, tbIntoRecord.getUserCode()));
        if(tbUser==null){
            return new ResponseResult(false,StateConstant.RESPONSERESULT_CODE_FALSE,"用户不存在");
        }
        Page<TbIntoRecord> page = new Page<>(currentPage, pageSize);
        Page<TbIntoRecord> iPage = tbIntoRecordMapper.selectPage(page, new QueryWrapper<TbIntoRecord>().lambda().eq(TbIntoRecord::getUserCode, tbIntoRecord.getUserCode()));
        resultMap.put("iPage", iPage);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 根据日期查询所有该日期的用户进店记录
     *
     * @param intoDate:进店时间
     * @param currentPage:当前页
     * @param pageSize:每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findIntoRecordByDate
     * @Date 19:10 2020/10/25
     */
    @Override
    public ResponseResult findIntoRecordByDate(String intoDate, Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (intoDate == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写日期");
        }
        if (currentPage == null) {
            currentPage = 1;
        }
        if (pageSize == null) {
            pageSize = 2;
        }
        Page<TbIntoRecord> page = new Page<>(currentPage, pageSize);
        Page<TbIntoRecord> iPage = tbIntoRecordMapper.selectPage(page, new QueryWrapper<TbIntoRecord>().lambda().like(TbIntoRecord::getIntoDate, intoDate));
        resultMap.put("iPage", iPage);
        result.setResult(resultMap);
        return result;

    }
}
