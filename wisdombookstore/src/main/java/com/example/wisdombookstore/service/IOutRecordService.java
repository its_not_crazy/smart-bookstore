package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbOutRecord;
import com.example.wisdombookstore.entity.TbOutRecord;
import com.example.wisdombookstore.util.ResponseResult;

import java.util.Date;

/**
 * @author 李慧龙
 * @date 2020/10/24 16:14
 * @className IOutRecordService
 * 出店记录service层
 */
public interface IOutRecordService extends IService<TbOutRecord> {
    /**
     * 添加出店记录
     * @Author 李慧龙
     * @Description //TODO 李慧龙 addOutRecord
     * @Date 16:11 2020/10/24
     * @param tbOutRecord: 出店记录
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult addOutRecord(TbOutRecord tbOutRecord);

    /**
     * 查询所有出店记录
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findAllOutRecord
     * @Date 16:11 2020/10/24
     * @param currentPage: 当前页
     * @param pageSize: 每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult findAllOutRecord(Integer currentPage,Integer pageSize);

    /**
     * 根据用户code查询所有记录
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findOutRecordByUserCode
     * @Date 16:12 2020/10/24
     * @param tbOutRecord:用户的code值
     * @param currentPage:当前页
     * @param pageSize:每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult findOutRecordByUserCode(TbOutRecord tbOutRecord, Integer currentPage, Integer pageSize);

    /**
     * 根据日期查询所有出店记录
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findOutRecordByDate
     * @Date 16:10 2020/10/24
     * @param intoDate:出店日期
     * @param currentPage:当前页数
     * @param pageSize:每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult findOutRecordByDate(String intoDate, Integer currentPage, Integer pageSize);
}
