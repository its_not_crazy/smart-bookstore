package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.*;
import com.example.wisdombookstore.mapper.RlBookTypeInfomMapper;
import com.example.wisdombookstore.mapper.TbShoppingBookMapper;
import com.example.wisdombookstore.mapper.TbShoppingCartMapper;
import com.example.wisdombookstore.mapper.TbShoppingGoodsMapper;
import com.example.wisdombookstore.service.*;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 宋合
 * @progrom: IntelliJ IDEA
 * @description: 购物车商品实现类
 * @date 2020/10/22 10:27
 */
@Service
@Slf4j
public class ShoppingGoodsServiceImpl extends ServiceImpl<TbShoppingGoodsMapper, TbShoppingGoods> implements IShoppingGoodsService {

}
