package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.SharedString;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.*;
import com.example.wisdombookstore.mapper.*;
import com.example.wisdombookstore.service.IBookInfoService;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author 李慧龙
 * @date 2020/10/22 10:23
 * @className BookInfoServiceImpl
 */
@Service
@Transactional
public class BookInfoServiceImpl extends ServiceImpl<TbBookInfoMapper, TbBookInfo> implements IBookInfoService {

    @Resource
    TbBookMapper tbBookMapper;
    @Resource
    TbBookInfoMapper tbbookInfoMapper;
    @Resource
    TbBookTypeMapper tbBookTypeMapper;
    @Resource
    TbBookImageMapper tbBookImageMapper;
    @Resource
    RlBookTypeInfomMapper rlBookTypeInfomMapper;
    @Resource
    IdWorker idWorker;


    /**
     * 书籍的添加
     *
     * @param tbBookInfo: 书籍信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO addTbBootInfo
     * @Date 16:39 2020/10/22
     */
    @Override
    public ResponseResult addTbBootInfo(TbBookInfo tbBookInfo, String tbBookImageCodes) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断传入参数是否为空
        if (tbBookInfo == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写参数");
        }
        //根据书籍的名称已经状态判断是否存在
        TbBookInfo tbBookInfo1 = tbbookInfoMapper.selectOne(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getName, tbBookInfo.getName()).eq(TbBookInfo::getIsDel, 0));
        if (tbBookInfo1 != null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "已有该书籍,谢谢");
        }
        //判断书籍类别是否存在
        TbBookType tbBookType = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookInfo.getTbBookTypeCode()));
        if (tbBookType == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "该书籍类别不存在");
        }
        //执行添加书籍操
        //书籍的默认数量为0
        tbBookInfo.setNumber(0);
        tbBookInfo.setCode(idWorker.nextId());
        tbBookInfo.setCreateTime(new Date());
        tbbookInfoMapper.insert(tbBookInfo);
        //通过判断信息的id来判断是否添加成功
        if (tbBookInfo.getId() == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍信息添加失败,请重新添加");
        }
       if(tbBookImageCodes!=null){
           //循环遍历头像id并进行头像与书籍的绑定
           String[] imageCode = tbBookImageCodes.split(",");
           for (String a : imageCode) {
               TbBookImage bookImage = tbBookImageMapper.selectOne(new QueryWrapper<TbBookImage>().lambda().eq(TbBookImage::getCode, a));
               if (bookImage == null) {
                   return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "图片信息不正确");
               }
               bookImage.setTbBookInfoCode(tbBookInfo.getCode());
               tbBookImageMapper.updateById(bookImage);
           }
           //书籍信息与书籍类别进行中间表绑定
           RlBookTypeInfo rlBookTypeInfo = new RlBookTypeInfo();
           rlBookTypeInfo.setTbBookInfoCode(tbBookInfo.getCode());
           rlBookTypeInfo.setTbBookTypesCode(tbBookInfo.getTbBookTypeCode());
           rlBookTypeInfo.setCode(idWorker.nextId());
           rlBookTypeInfo.setCreateTime(new Date());
           rlBookTypeInfomMapper.insert(rlBookTypeInfo);
           if (rlBookTypeInfo.getId() == null) {
               return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "中间表添加失败");
           }
       }
        //返回添加成功信息
        resultMap.put("tbBookInfo", tbBookInfo);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 书籍的删除操作
     *
     * @param tbBookInfo: 书籍的code信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO deleteTbBootInfo
     * @Date 16:40 2020/10/22
     */
    @Override
    public ResponseResult deleteTbBootInfo(TbBookInfo tbBookInfo) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断传入参数是否为空
        if (tbBookInfo == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写书籍code信息");
        }
        //通过书籍的code信息查询书籍信息
        TbBookInfo tbBookInfo1 = tbbookInfoMapper.selectOne(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getCode, tbBookInfo.getCode()));
        if (tbBookInfo1 == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍信息不存在");
        }
        //判断书籍信息的状态  0正常  1删除
        if (tbBookInfo1.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍信息已经删除");
        }
        //在下架书籍的时候图书有也会下架
        List<TbBook> tbBooks = tbBookMapper.selectList(new QueryWrapper<TbBook>().lambda().eq(TbBook::getTbBookInfoCode, tbBookInfo.getCode()));
        for (TbBook tbBook : tbBooks) {
            tbBook.setIsDel(StateConstant.OBJECT_DEL_OMIT);
            tbBookMapper.updateById(tbBook);
        }
        //修改书籍信息的状态
        tbBookInfo1.setIsDel(StateConstant.OBJECT_DEL_OMIT);
        tbBookInfo1.setUpdateTime(new Date());
        tbbookInfoMapper.updateById(tbBookInfo1);
        resultMap.put("tbBookInfo", tbBookInfo1);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 修改书籍信息
     *
     * @param tbBookInfo:修改书籍信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO updateTbBootInfo
     * @Date 16:50 2020/10/22
     */
    @Override
    public ResponseResult updateTbBootInfo(TbBookInfo tbBookInfo, String imageStr) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断传入参数是否为空
        if (tbBookInfo == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写书籍信息");
        }
        //根据书籍code值判断书籍是否存在
        TbBookInfo tbBookInfo1 = tbbookInfoMapper.selectOne(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getCode, tbBookInfo.getCode()));
        if (tbBookInfo1 == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍信息不存在");
        }
        //判断书籍的状态
        if (tbBookInfo1.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍信息已经删除");
        }
        //判断书籍类别是否存在
        TbBookType tbBookType = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookInfo.getTbBookTypeCode()));
        if (tbBookType == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "该书籍类别不存在");
        }
        //修改书籍信息
        tbBookInfo.setUpdateTime(new Date());
        tbBookInfo.setId(tbBookInfo1.getId());
        tbbookInfoMapper.updateById(tbBookInfo);
        //返回结果
        resultMap.put("tbBookInfo", tbBookInfo);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findAllTbBootInfo(Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (currentPage.equals(null)) {
            currentPage = 0;
        }
        if (pageSize.equals(null)) {
            currentPage = 2;
        }
        Page<TbBookInfo> page = new Page<>(currentPage, pageSize);
        Page<TbBookInfo> iPage = tbbookInfoMapper.selectPage(page, null);
        List<TbBookInfo> records = iPage.getRecords();
        //遍历集合带走数据
        for(TbBookInfo tbBookInfo:records){
            //根据类别信息查询类别
            TbBookType tbBookType = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookInfo.getTbBookTypeCode()));
            //带走类别信息
            tbBookInfo.setTbBookType(tbBookType);
            //根据书籍信息查询图片信息
            List<TbBookImage> tbBookImages = tbBookImageMapper.selectList(new QueryWrapper<TbBookImage>().lambda().eq(TbBookImage::getTbBookInfoCode, tbBookInfo.getCode()));
            //带走
            tbBookInfo.setTbBookImages(tbBookImages);
        }
        long total = iPage.getTotal();
        resultMap.put("iPage", iPage);
        resultMap.put("total",total);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findAllInfo() {
        ResponseResult result = new ResponseResult();
        HashMap<String, Object> resultMap = new HashMap<>();
        List<TbBookInfo> tbBookInfos = tbbookInfoMapper.selectList(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getIsDel,0));
        for(TbBookInfo tbBookInfo:tbBookInfos){
            //根据类别信息查询类别
            TbBookType tbBookType = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookInfo.getTbBookTypeCode()));
            //带走类别信息
            tbBookInfo.setTbBookType(tbBookType);
            //根据书籍信息查询图片信息
            List<TbBookImage> tbBookImages = tbBookImageMapper.selectList(new QueryWrapper<TbBookImage>().lambda().eq(TbBookImage::getTbBookInfoCode, tbBookInfo.getCode()));
            //带走
            tbBookInfo.setTbBookImages(tbBookImages);
        }
        resultMap.put("tbBookInfos",tbBookInfos);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findOnetbBookInfoByCode(TbBookInfo tbBookInfo) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (tbBookInfo == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写书籍信息");
        }
        //根据code值查询判断是否存在
        TbBookInfo tbBookInfo1 = tbbookInfoMapper.selectOne(new QueryWrapper<TbBookInfo>().lambda().eq(TbBookInfo::getCode, tbBookInfo.getCode()).last("limit 1"));
        if (tbBookInfo1 == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍信息不存在");
        }
        //判短书籍信息是否删除
        if (tbBookInfo1.getIsDel().equals(StateConstant.OBJECT_DEL_OMIT)) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "书籍信息已经删除");
        }
        List<TbBookImage> tbBookImages = tbBookImageMapper.selectList(new QueryWrapper<TbBookImage>().lambda().eq(TbBookImage::getTbBookInfoCode, tbBookInfo1.getCode()));
        tbBookInfo1.setTbBookImages(tbBookImages);
        resultMap.put("tbBookInfo", tbBookInfo1);
        result.setResult(resultMap);
        return result;
    }

    @Override
    public ResponseResult findAllAll() {
        ResponseResult result = new ResponseResult();
        HashMap<String, Object> resultMap = new HashMap<>();
        List<TbBookInfo> tbBookInfos = tbbookInfoMapper.selectList(null);
        for(TbBookInfo tbBookInfo:tbBookInfos){
            //根据类别信息查询类别
            TbBookType tbBookType = tbBookTypeMapper.selectOne(new QueryWrapper<TbBookType>().lambda().eq(TbBookType::getCode, tbBookInfo.getTbBookTypeCode()));
            //带走类别信息
            tbBookInfo.setTbBookType(tbBookType);
            //根据书籍信息查询图片信息
            List<TbBookImage> tbBookImages = tbBookImageMapper.selectList(new QueryWrapper<TbBookImage>().lambda().eq(TbBookImage::getTbBookInfoCode, tbBookInfo.getCode()));
            //带走
            tbBookInfo.setTbBookImages(tbBookImages);
        }
        resultMap.put("tbBookInfos",tbBookInfos);
        result.setResult(resultMap);
        return result;
    }
}
