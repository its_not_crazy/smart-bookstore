package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbShoppingBook;
import com.example.wisdombookstore.entity.TbShoppingCart;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @Description: 购物车单品Service
 * @Author: 宋合
 * @Date: 2020/10/20 12:57
 **/
public interface IShoppingBookService extends IService<TbShoppingBook> {





}
