package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.*;
import com.example.wisdombookstore.mapper.TbCollectMapper;
import com.example.wisdombookstore.service.*;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 收藏实现
 * @date 2020/10/23 14:47
 */
@Service
public class CollectServiceImpl extends ServiceImpl<TbCollectMapper, TbCollect> implements ICollectService {

    @Resource
    private IdWorker idWorker;

    @Resource
    private IUserService iUserService;

    @Resource
    private TbCollectMapper tbCollectMapper;

    @Resource
    private IBookInfoService iBookInfoService;


    @Override
    public ResponseResult addColler(TbCollect tbCollect) {
        //自增量设置null
        tbCollect.setId(null);
        //唯一标识
        tbCollect.setCode(idWorker.nextId());
        //正常状态
        tbCollect.setIsDel(StateConstant.OBJECT_DEL_NORMAL);
        //添加购物车时间
        tbCollect.setCreateTime(new Date());
        //修改购物车信息时间
        tbCollect.setUpdateTime(null);
        boolean save = this.save(tbCollect);
        if(save){
            return new ResponseResult(true,StateConstant.RESPONSERESULT_CODE_TRUE,"收藏成功",tbCollect);
        }
        return new ResponseResult(false,StateConstant.RESPONSERESULT_CODE_FALSE,"收藏失败");
    }

    @Override
    public ResponseResult updateColler(TbCollect tbCollect) {
        //根据code查询所需要修改的购物信息
        QueryWrapper<TbCollect> tbCollectQueryWrapper = new QueryWrapper<TbCollect>();
        //设置查询条件   code  用户code
        tbCollectQueryWrapper.lambda().eq(TbCollect::getCode, tbCollect.getCode())
                .eq(TbCollect::getTbUserCode, tbCollect.getTbUserCode());
        TbCollect collect = this.getOne(tbCollectQueryWrapper);
        //修改购物车对应的信息
        collect.setCode(tbCollect.getCode());
        collect.setTbBookInfoCode(tbCollect.getTbBookInfoCode());
        //设置此条信息的最后修改时间
        collect.setUpdateTime(new Date());

        boolean save = this.updateById(collect);
        if (save) {
            return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "编辑成功", collect);
        }
        return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "编辑失败");
    }

    @Override
    public ResponseResult delColler(TbCollect tbCollect) {
        ///根据收藏的唯一标识查询需要删除的记录
        QueryWrapper<TbCollect> tbCollectQueryWrapper = new QueryWrapper<>();
        //查询条件是收藏记录唯一标识 已经记录为正常的记录
        System.err.println(tbCollect.getCode());
        tbCollectQueryWrapper.lambda().eq(TbCollect::getCode, tbCollect.getCode())
                .eq(TbCollect::getIsDel, StateConstant.OBJECT_DEL_NORMAL);
        TbCollect coller = this.getOne(tbCollectQueryWrapper);
        if (coller == null) {
            return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "收藏已经删除成功");
        }
        //设置软删除值
        coller.setIsDel(StateConstant.OBJECT_DEL_OMIT);
        //执行软删除操作
        boolean b = this.updateById(coller);
        if (b) {
            return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "收藏删除成功", coller);
        }
        return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "收藏删除失败");
    }

    @Override
    public ResponseResult pageFindAllUserColler(TbCollect tbCollect, int countpage, int size) {
        HashMap<String, Object> hashMap = new HashMap<>();
        //创建分页page
        Page<TbCollect> tbShoppingCartPage = new Page<TbCollect>(countpage, size);
        QueryWrapper<TbCollect> tbCollectQueryWrapper = new QueryWrapper<>();
        //根据条件查询 用户的唯一标识
        tbCollectQueryWrapper.lambda().eq(TbCollect::getTbUserCode, tbCollect.getTbUserCode());
        Page<TbCollect> page = this.page(tbShoppingCartPage, tbCollectQueryWrapper);
        for (TbCollect shopping : page.getRecords()) {
            //查询书籍详细信息
            TbBookInfo bookById = this.iBookInfoService.getById(shopping.getTbBookInfoCode());
            //带走书籍详细信息
            shopping.setTbBookInfo(bookById);
        }
        if (page.getTotal() == 0) {
            return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功，请添加收藏", page);
        }
        hashMap.put("page", page);
        //查询出来的总记录数
        hashMap.put("total", page.getTotal());
        return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功", hashMap);
    }

    @Override
    public ResponseResult pageFindAllColler(TbCollect tbCollect, int countpage, int size) {
        HashMap<String, Object> hashMap = new HashMap<>();
        //创建分页page
        Page<TbShoppingCart> tbShoppingCartPage = new Page<>(countpage, size);
        //当用户唯一标识与图书唯一标识都为null时  查询所有
        if (tbCollect.getTbUserCode() == null && tbCollect.getTbBookInfoCode() == null) {
            System.err.println(1);
            ResponseResult a = a(hashMap, tbShoppingCartPage, tbCollect, countpage, size);
            return a;
        }
        //当用户唯一标识与图书唯一标识都不为null时  根据条件查询
        if (tbCollect.getTbUserCode() != null && tbCollect.getTbBookInfoCode() != null) {
            System.err.println(2);
            return b(hashMap,tbShoppingCartPage,tbCollect,countpage, size);
        }
        //当用户唯一标识 为null 时    利用图书唯一标识
        if (tbCollect.getTbUserCode() == null) {
            System.err.println(3);
            return c(hashMap,tbShoppingCartPage,tbCollect,countpage, size);
        }
        //当图书唯一标识都为null时     利用用户唯一标识查询
        if (tbCollect.getTbBookInfoCode() == null) {
            System.err.println(4);
            return d(hashMap,tbShoppingCartPage,tbCollect,countpage, size);
        }
        return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_FALSE, "查询失败");
    }

    public ResponseResult a(HashMap hashMap,Page tbShoppingCartPage,TbCollect tbCollect, int countpage, int size){
        QueryWrapper<TbCollect> tbCollectQueryWrapper = new QueryWrapper<>();
        //根据条件查询 用户的唯一标识
        Page<TbCollect> page = this.page(tbShoppingCartPage);
        for (TbCollect shopping : page.getRecords()) {
            TbUser user = this.iUserService.getById(shopping.getTbUserCode());
            //查询书籍详细信息
            TbBookInfo bookById = this.iBookInfoService.getById(shopping.getTbBookInfoCode());
            //带走书籍详细信息
            shopping.setTbUser(user);
            shopping.setTbBookInfo(bookById);
        }
        if (page.getTotal() == 0) {
            return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功，请添加收藏");
        }
        hashMap.put("page", page);
        //查询出来的总记录数
        hashMap.put("total", page.getTotal());
        return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功", hashMap);
    }

    public ResponseResult b(HashMap hashMap,Page tbShoppingCartPage,TbCollect tbCollect, int countpage, int size){
        QueryWrapper<TbCollect> tbCollectQueryWrapper = new QueryWrapper<>();
        //根据条件查询 用户的唯一标识
        tbCollectQueryWrapper.lambda().eq(TbCollect::getTbUserCode, tbCollect.getTbUserCode());
        Page<TbCollect> page = this.page(tbShoppingCartPage, tbCollectQueryWrapper);
        for (TbCollect shopping : page.getRecords()) {
            TbUser user = this.iUserService.getById(shopping.getTbUserCode());
            //查询书籍详细信息
            TbBookInfo bookById = this.iBookInfoService.getById(shopping.getTbBookInfoCode());
            //带走书籍详细信息
            shopping.setTbUser(user);
            shopping.setTbBookInfo(bookById);
        }
        if (page.getTotal() == 0) {
            return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功，请添加收藏");
        }
        hashMap.put("page", page);
        //查询出来的总记录数
        hashMap.put("total", page.getTotal());
        return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功", hashMap);
    }

    public ResponseResult c(HashMap hashMap,Page tbShoppingCartPage,TbCollect tbCollect, int countpage, int size){
        QueryWrapper<TbCollect> tbCollectQueryWrapper = new QueryWrapper<>();
        //根据条件查询 用户的唯一标识
        tbCollectQueryWrapper.lambda().eq(TbCollect::getTbUserCode, tbCollect.getTbUserCode());
        Page<TbCollect> page = this.page(tbShoppingCartPage, tbCollectQueryWrapper);
        for (TbCollect shopping : page.getRecords()) {
            TbUser user = this.iUserService.getById(shopping.getTbUserCode());
            //查询书籍详细信息
            TbBookInfo bookById = this.iBookInfoService.getById(shopping.getTbBookInfoCode());
            //带走书籍详细信息
            shopping.setTbUser(user);
            shopping.setTbBookInfo(bookById);
        }
        if (page.getTotal() == 0) {
            return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功，请添加收藏");
        }
        hashMap.put("page", page);
        //查询出来的总记录数
        hashMap.put("total", page.getTotal());
        return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功", hashMap);
    }

    public ResponseResult d(HashMap hashMap,Page tbShoppingCartPage,TbCollect tbCollect, int countpage, int size){
        QueryWrapper<TbCollect> tbCollectQueryWrapper = new QueryWrapper<>();
        //根据条件查询 用户的唯一标识
        tbCollectQueryWrapper.lambda().eq(TbCollect::getTbUserCode, tbCollect.getTbUserCode());
        Page<TbCollect> page = this.page(tbShoppingCartPage, tbCollectQueryWrapper);
        for (TbCollect shopping : page.getRecords()) {
            TbUser user = this.iUserService.getById(shopping.getTbUserCode());
            //查询书籍详细信息
            TbBookInfo bookById = this.iBookInfoService.getById(shopping.getTbBookInfoCode());
            //带走书籍详细信息
            shopping.setTbUser(user);
            shopping.setTbBookInfo(bookById);
        }
        if (page.getTotal() == 0) {
            return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功，请添加收藏");
        }
        hashMap.put("page", page);
        //查询出来的总记录数
        hashMap.put("total", page.getTotal());
        return new ResponseResult(true, StateConstant.RESPONSERESULT_CODE_TRUE, "查询成功", hashMap);
    }
}
