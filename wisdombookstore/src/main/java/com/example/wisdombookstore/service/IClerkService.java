package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.*;
import com.example.wisdombookstore.util.ResponseResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @ClassName IClerkService
 * @Description TODO  IClerkService 工作人员接口
 * @Author 8aceMak1r
 * @Date 2020/10/20 8:46
 * @Version 1.0
 */
public interface IClerkService extends IService<TbClerk> {

    /**
     * TODO 8aceMak1r clerkLogin 工作人员登录
     * @param clerk 工作人员信息
     * @param session
     * @Description: 工作人员登录
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/22 11:31
     */
    public ResponseResult clerkLogin(TbClerk clerk, HttpSession session);

    /**
     * TODO 8aceMak1r clerkLogout 退出登录
     * @param request
     * @Description: 退出登录
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/22 11:36
     */
    public ResponseResult clerkLogout(HttpServletRequest request);





    /**
     * TODO 8aceMak1r addClerk 新增店员
     * @param clerk 店员信息
     * @Description: 店长新增店员
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/22 11:05
     */
    public ResponseResult addClerk(TbClerk clerk);

    /**
     * TODO 8aceMak1r getClerkList 查看店员
     * @Description: 查看所有店员
     * @return com.example.windstorm.util.ResponseResult
     * @param pageCurrent 当前页
     * @param pageSize 总页数
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/22 11:16
     */
    public ResponseResult getClerkList(Integer pageSize,Integer pageCurrent);

    /**
     * TODO 8aceMak1r getClerkById 查看店员详情
     * @param tbUserId 店员信息
     * @Description: 查看店员详情信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/22 11:20
     */
    public ResponseResult getClerkById(Long tbUserId);

    /**
     * TODO 8aceMak1r setClerkById 修改店员信息
     * @param clerk 店员信息
     * @Description: 修改店员信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/22 11:22
     */
    public ResponseResult setClerkById(TbClerk clerk);

    /**
     * TODO 8aceMak1r setClerkPassword 修改密码
     * @param tbUserId 工作人员
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @Description: 修改工作人员密码
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/22 11:41
     */
    public ResponseResult setClerkPassword(Long tbUserId,String oldPassword,String newPassword);

    /**
     * TODO 8aceMak1r delClerkById 删除店员
     * @param tbUserId 店员
     * @Description: 删除店员
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/22 11:28
     */
    public ResponseResult delClerkById(Long tbUserId);


    public ResponseResult clerkList(int page,int size);

    /**
    * TODO 陈阳    查询所有员工
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/11/2 10:05

     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult clerkList();
}
