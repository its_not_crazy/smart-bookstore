package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbCollect;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description:   收藏service
 * @date 2020/10/23 14:45
 */
public interface ICollectService extends IService<TbCollect> {


    /**
    * TODO 陈阳   用户添加收藏
    * @Description: addColler
    * @Param: * @param tbCollect:  （tbUserCode     用户唯一标识
     *                               tbBookInfoCode 商品唯一标识
     *                               ）
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/23
    * @Time: 14:53
    */
    public ResponseResult addColler(TbCollect tbCollect);

    /**
    * TODO 陈阳    用户修改收藏
    * @Description: updateColler
    * @Param: * @param tbCollect:（code  修改的这条收藏的唯一标识
     *                             tbUserCode     用户唯一标识
     *                             tbBookInfoCode 商品唯一标识
     *                              ）
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/23
    * @Time: 14:53
    */
    public ResponseResult updateColler(TbCollect tbCollect);

    /**
    * TODO 陈阳   用户删除收藏
    * @Description: delColler
    * @Param: * @param tbCollect: （code  修改的这条收藏的唯一标识)
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/23
    * @Time: 14:53
    */
    public ResponseResult delColler(TbCollect tbCollect);


    /**
    * TODO 陈阳   用户分页查询自己的收藏夹
    * @Description: pageFindAllUserColler
    * @Param: * @param tbCollect:（tbUserCode  用户的唯一标识)
     * @param page:  当前页
     * @param size:  每页几条
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/23
    * @Time: 14:57
    */
    public ResponseResult pageFindAllUserColler(TbCollect tbCollect, int page, int size);


    /**
    * TODO 陈阳    分页查询所有的收藏夹，可根据用户查询，可根据收藏的图书查询
    * @Description: pageFindAllColler
    * @Param: * @param tbCollect:   （tbUserCode     用户唯一标识
     *                               tbBookInfoCode 商品唯一标识
     *                               ）
     * @param page:  当前页
     * @param size:  每页几条
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/23
    * @Time: 14:57
    */
    public ResponseResult pageFindAllColler(TbCollect tbCollect, int page, int size);
}
