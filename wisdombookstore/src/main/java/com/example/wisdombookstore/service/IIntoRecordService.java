package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbIntoRecord;
import com.example.wisdombookstore.util.ResponseResult;

import java.util.Date;

/**
 * @author 李慧龙
 * @date 2020/10/24 15:51
 * @className IIntoRecordService
 * 进店记录service层
 */
public interface IIntoRecordService extends IService<TbIntoRecord> {

    /**
     * 添加进店记录
     * @Author 李慧龙
     * @Description //TODO 李慧龙 addIntoRecord
     * @Date 16:11 2020/10/24
     * @param tbIntoRecord: 进店记录
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult addIntoRecord(TbIntoRecord tbIntoRecord);

    /**
     * 查询所有进店记录
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findAllIntoRecord
     * @Date 16:11 2020/10/24
     * @param currentPage: 当前页
     * @param pageSize: 每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult findAllIntoRecord(Integer currentPage,Integer pageSize);

    /**
     * 根据用户code查询该用户的所有记录
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findIntoRecordByUserCode
     * @Date 16:12 2020/10/24
     * @param tbIntoRecord:用户的code值
     * @param currentPage:当前页
     * @param pageSize:每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    ResponseResult findIntoRecordByUserCode(TbIntoRecord tbIntoRecord,Integer currentPage,Integer pageSize);

   /**
    * 根据日期查询所有该日期的用户进店记录
    * @Author 李慧龙
    * @Description //TODO 李慧龙 findIntoRecordByDate
    * @Date 16:10 2020/10/24
    * @param intoDate:进店日期
    * @param currentPage:当前页数
    * @param pageSize:每页条数
    * @return com.example.wisdombookstore.util.ResponseResult
    */
    ResponseResult findIntoRecordByDate(String intoDate,Integer currentPage,Integer pageSize);
}
