package com.example.wisdombookstore.service;


import com.example.wisdombookstore.entity.TbCheckTask;

import com.baomidou.mybatisplus.extension.service.IService;

import com.example.wisdombookstore.entity.TbReplenishmentTask;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @Author: MaHan
 * @Date: Created in 14:46 2020/10/25
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: IReplenishmentTaskService
 */
public interface IReplenishmentTaskService {

    /**
     * TODO &马晗&  根据补货人查询补货任务
     *
     * @param tbUserCode: 补货人标识
     * @return: com.example.wisdombookstore.entity.TbCheckTask
     * @Author &马晗&
     * @Description 根据负责人查询补货任务
     * @Date 14:43 2020/10/25
     **/
    public ResponseResult replenishmentUserFindCode(Long tbUserCode);

    /**
     * TODO &马晗&  根据补货人查询自己的补货任务
     *
     * @param tbUserCode: 补货人标识
     * @return: com.example.wisdombookstore.entity.TbCheckTask
     * @Author &马晗&
     * @Description 根据负责人查询补货任务
     * @Date 14:43 2020/10/25
     **/
    public ResponseResult replenishmentUserFindCodeList(Long tbUserCode);

    /**
     * TODO &马晗& 查询所有补货任务
     *
     * @param currentPage: 当前页
     * @param pageSize:    每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description 查询所有补货任务
     * @Date 19:46 2020/10/25
     **/
    public ResponseResult replenishmentUserFind(Integer currentPage, Integer pageSize);


    /**
     * TODO 8aceMak1r creteReplenishmentTaskCode 创建补货编号
     *
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description:
     * @author 8aceMak1r
     * @date 2020/10/25 10:43
     */
    public ResponseResult creteReplenishmentTaskCode();

    /**
     * TODO 8aceMak1r createReplenishment 创建补货计划
     *
     * @param replenishmentTask   补货任务
     * @param tbUserId            负责补货的工作人员
     * @param tbReplenishmentCode 任务code
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description 店长创建补货任务
     * @author 8aceMak1r
     * @date 2020/10/22 9:48
     */
    public ResponseResult createReplenishment(TbReplenishmentTask replenishmentTask, Long tbUserId, Long tbReplenishmentCode);

    /**
     * TODO 8aceMak1r executeReplenishmentTask 执行补货任务
     *
     * @param tbUserId                  工作人员
     * @param tbReplenishmentResultCode 补货结果
     * @param rfidCode                  书品的rfid
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description 执行任务将扫描的书本和任务中的书本类型、单品、书籍进行对比是否录入正确
     * @author 8aceMak1r
     * @date 2020/10/20 13:51
     */
    public ResponseResult executeReplenishmentTask(Long tbUserId, Long tbReplenishmentResultCode, String rfidCode);

    /**
     * TODO 8aceMak1r delReplenishmentTask 取消补货任务
     * @param tbReplenishmentResultCode 补货任务code
     * @Description: 取消补货任务
     * @return com.example.wisdombookstore.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/11/1 19:52
     */
    public ResponseResult delReplenishmentTask(Long tbReplenishmentResultCode);

}
