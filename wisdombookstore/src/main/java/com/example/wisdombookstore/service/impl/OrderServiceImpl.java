package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.*;
import com.example.wisdombookstore.mapper.TbOrderMapper;
import com.example.wisdombookstore.mapper.TbOrdersDetailMapper;
import com.example.wisdombookstore.mapper.TbShoppingBookMapper;
import com.example.wisdombookstore.mapper.TbShoppingGoodsMapper;
import com.example.wisdombookstore.service.*;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.scripting.xmltags.ForEachSqlNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class OrderServiceImpl extends ServiceImpl<TbOrderMapper, TbOrder> implements IOrderService {

    @Autowired
    private TbOrderMapper tbOrderMapper;
    @Autowired
    private IBookService iBookService;
    @Autowired
    private IShoppingCartService iShoppingCartService;
    @Autowired
    private TbShoppingGoodsMapper tbShoppingGoodsMapper;
    @Autowired
    private TbShoppingBookMapper tbShoppingBookMapper;
    @Autowired
    private IBookInfoService iBookInfoService;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private IShoppingGoodsService iShoppingGoodsService;
    @Autowired
    private IOrdersDetailService iOrdersDetailService;
    @Autowired
    private IShoppingBookService iShoppingBookService;

    /**
     * TODO &宋合& 取消订单功能
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 11:19
     * @param tbOrderCode:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @Override
    public ResponseResult cancelBookOrder(Long tbOrderCode) {
        ResponseResult responseResult = new ResponseResult();
        try {

            responseResult = this.bookOrder(tbOrderCode);
            TbOrder tbOrder = (TbOrder) responseResult.getResult();

            ArrayList<TbBook> tbBooks = new ArrayList<>();
            if(tbOrder!=null){
                tbOrder.setIsDel(StateConstant.OBJECT_DEL_OMIT);
                for (TbOrdersDetail tbOrdersDetail:tbOrder.getTbOrdersDetails()) {
                    tbOrdersDetail.setIsDel(StateConstant.OBJECT_DEL_OMIT);
                    iOrdersDetailService.saveOrUpdate(tbOrdersDetail);
                    for (TbBook tbBook:tbOrdersDetail.getTbBooks()) {
                            tbBook.setTbOrderDetailCode(0L);
                            tbBooks.add(tbBook);
                    }
                }
                iBookService.saveOrUpdateBatch(tbBooks);
                tbOrder.setStatus(1);
                tbOrderMapper.updateById(tbOrder);
                responseResult.setResult(tbBooks);
                return responseResult;
            }
            responseResult.setFailMessage("查无此订单");
            return responseResult;
        } catch (Exception e) {
            e.printStackTrace();
        }
        responseResult.setFailMessage("取消失败");
        return responseResult;
    }
    /**
     * TODO &宋合& 生成订单
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 11:20
     * @param tbShoppingCartCode: 购物车标识
     * @return: java.lang.Object
     **/
    @Override
    public ResponseResult createBookOrder(Long tbShoppingCartCode) {

        ResponseResult responseResult = new ResponseResult();
        //通过购物车code查询购物车信息
        QueryWrapper<TbShoppingCart> tbShoppingCartQueryWrapper = new QueryWrapper<>();
        tbShoppingCartQueryWrapper.lambda().eq(TbShoppingCart::getCode,tbShoppingCartCode);
        TbShoppingCart one = iShoppingCartService.getOne(tbShoppingCartQueryWrapper);
        if(one==null){
            responseResult.setFailMessage("该购物车不存在");
            return responseResult;
        }
        QueryWrapper<TbShoppingGoods> tbShoppingGoodsQueryWrapper = new QueryWrapper<>();
        tbShoppingGoodsQueryWrapper.lambda().eq(TbShoppingGoods::getTbShoppingCartCode,one.getCode());
        //查询出该购物车的商品信息
        List<TbShoppingGoods> tbShoppingGoodsNew = tbShoppingGoodsMapper.selectList(tbShoppingGoodsQueryWrapper);
        //信息存放到购物车商品集合中
        one.setTbShoppingGoods(tbShoppingGoodsNew);
        //循环存放购物车商品单品信息
        for (TbShoppingGoods tbShoppingGoods: tbShoppingGoodsNew) {
            QueryWrapper<TbShoppingBook> tbShoppingBookQueryWrapper = new QueryWrapper<>();
            tbShoppingBookQueryWrapper.lambda().eq(TbShoppingBook::getTbShoppingGoodsCode,tbShoppingGoods.getCode());
            //查询每个商品下面的单品信息
            List<TbShoppingBook> tbShoppingBooks = tbShoppingBookMapper.selectList(tbShoppingBookQueryWrapper);
            tbShoppingGoods.setTbShoppingBooks(tbShoppingBooks);
        }
        //创建订单对象
        TbOrder tbOrder = new TbOrder();
        tbOrder.setTbUserCode(one.getTbUserCode());
        tbOrder.setStatus(0);
        tbOrder.setCurrentMin(new Date());
        double totalPrice =0.0;
        Integer num=0;
        ArrayList<TbOrdersDetail> tbOrdersDetails = new ArrayList<>();
        ArrayList<Long> longs = new ArrayList<>();
        for (TbShoppingGoods tbShoppingGoods: one.getTbShoppingGoods()) {

            QueryWrapper<TbBookInfo> tbBookInfoQueryWrapper = new QueryWrapper<>();
            //查询购物车商品的详细信息
            tbBookInfoQueryWrapper.lambda().eq(TbBookInfo::getCode,tbShoppingGoods.getTbBookInfoCode());
            TbBookInfo tbBookInfoOne = iBookInfoService.getOne(tbBookInfoQueryWrapper);
            //通过购物车商品的价格和单品数量计算订单总价格
            totalPrice+=tbBookInfoOne.getPrice()*tbShoppingGoods.getTbShoppingBooks().size();
            //通过商品下的所有单品数量计算订单总单品数量
            num+=tbShoppingGoods.getTbShoppingBooks().size();
        }
        tbOrder.setTotalNum(num);
        tbOrder.setCode(idWorker.nextId());
        tbOrder.setTotalPrice(totalPrice);
        //添加订单
        tbOrderMapper.insert(tbOrder);
        //遍历购物车的商品
        for (TbShoppingGoods tbShoppingGoods: one.getTbShoppingGoods()) {

            QueryWrapper<TbBookInfo> tbBookInfoQueryWrapper = new QueryWrapper<>();
            //查询购物车商品的详细信息
            tbBookInfoQueryWrapper.lambda().eq(TbBookInfo::getCode,tbShoppingGoods.getTbBookInfoCode());
            TbBookInfo tbBookInfoOne = iBookInfoService.getOne(tbBookInfoQueryWrapper);
            //创建一个购物车详情对象
            TbOrdersDetail tbOrdersDetail = new TbOrdersDetail();
            tbOrdersDetail.setTbBookInfoCode(tbShoppingGoods.getTbBookInfoCode());
            tbOrdersDetail.setNum(tbShoppingGoods.getTbShoppingBooks().size());
            tbOrdersDetail.setCurrentPrice(tbBookInfoOne.getPrice());
            tbOrdersDetail.setTbBookInfoName(tbBookInfoOne.getName());
            tbOrdersDetail.setCode(idWorker.nextId());
            tbOrdersDetail.setTbOrderCode(tbOrder.getCode());
            tbOrdersDetail.setTotalPrice(tbBookInfoOne.getPrice()*tbShoppingGoods.getTbShoppingBooks().size());
            //循环添加购物车详情
            iOrdersDetailService.save(tbOrdersDetail);
            //循环遍历购物车单品
            for (TbShoppingBook tbShoppingBook: tbShoppingGoods.getTbShoppingBooks()) {

                QueryWrapper<TbBook> tbBookQueryWrapper = new QueryWrapper<>();
                //查询单品信息
                tbBookQueryWrapper.lambda().eq(TbBook::getCode,tbShoppingBook.getTbBookCode());
                TbBook tbBookOne = iBookService.getOne(tbBookQueryWrapper);
                //更改单品状态
                tbBookOne.setStatus(1);
                //关联订单详情
                tbBookOne.setTbOrderDetailCode(tbOrdersDetail.getCode());
                iBookService.saveOrUpdate(tbBookOne);
                ArrayList<TbShoppingBook> tbShoppingBooks = new ArrayList<>();
                for (TbShoppingGoods shoppingGoods: tbShoppingGoodsNew) {
                    shoppingGoods.setIsDel(1);
                    for (TbShoppingBook tbShoppingBookNew:shoppingGoods.getTbShoppingBooks()) {
                        tbShoppingBookNew.setIsDel(1);
                        tbShoppingBooks.add(tbShoppingBookNew);
                    }
                }
                iShoppingGoodsService.saveOrUpdateBatch(tbShoppingGoodsNew);
                iShoppingBookService.saveOrUpdateBatch(tbShoppingBooks);
            }
        }

        responseResult.setResult(tbOrder);
        return responseResult;
    }

    @Override
    public ResponseResult bookOrderList(Long tbBookCode) {
        ResponseResult responseResult = new ResponseResult();
        QueryWrapper<TbOrder> tbBookQueryWrapper = new QueryWrapper<>();
        tbBookQueryWrapper.lambda().eq(TbOrder::getTbUserCode,tbBookCode);
        List<TbOrder> tbOrders = tbOrderMapper.selectList(tbBookQueryWrapper);
        if(tbOrders.size()>0){
            responseResult.setResult(tbOrders);

            return responseResult;
        }
        responseResult.setFailMessage("查无订单");
        return responseResult;
    }

    @Override
    public ResponseResult bookOrder(Long tbOrderCode) {
        ResponseResult responseResult = new ResponseResult();
        QueryWrapper<TbOrder> tbOrderQueryWrapper = new QueryWrapper<>();
        tbOrderQueryWrapper.lambda().eq(TbOrder::getCode,tbOrderCode);
        TbOrder tbOrder = tbOrderMapper.selectOne(tbOrderQueryWrapper);
        QueryWrapper<TbOrdersDetail> tbOrdersDetailQueryWrapper = new QueryWrapper<>();
        tbOrdersDetailQueryWrapper.lambda().eq(TbOrdersDetail::getTbOrderCode,tbOrder.getCode());
        List<TbOrdersDetail> list = iOrdersDetailService.list(tbOrdersDetailQueryWrapper);
        tbOrder.setTbOrdersDetails(list);
        for (TbOrdersDetail tbOrdersDetail: list) {
            QueryWrapper<TbBook> tbBookQueryWrapper = new QueryWrapper<>();
            tbBookQueryWrapper.lambda().eq(TbBook::getTbOrderDetailCode,tbOrdersDetail.getCode());
            List<TbBook> tbBookList = iBookService.list(tbBookQueryWrapper);
            tbOrdersDetail.setTbBooks(tbBookList);
        }
        responseResult.setResult(tbOrder);
        return responseResult;
    }

    @Override
    public ResponseResult getBookOrderList(Integer status, Integer pageCurrent, Integer pageSize) {
        ResponseResult result=ResponseResult.SUCCESS();
        HashMap<String,Object> resultMap=new HashMap<>();
        QueryWrapper<TbOrder> wrapper =new QueryWrapper<>();
        PageHelper.startPage(pageSize,pageCurrent);


        if (status!=null){
            wrapper.lambda().eq(TbOrder::getStatus,status);

        }
        List<TbOrder> orders = tbOrderMapper.selectList(wrapper);
        PageInfo<TbOrder> tbUserPageInfo = new PageInfo<>(orders);
        result.setResult(tbUserPageInfo);
        return result;
    }
}
