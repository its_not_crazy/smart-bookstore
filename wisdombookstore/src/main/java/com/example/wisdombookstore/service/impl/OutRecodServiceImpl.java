package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.TbOutRecord;
import com.example.wisdombookstore.entity.TbOutRecord;
import com.example.wisdombookstore.entity.TbUser;
import com.example.wisdombookstore.mapper.TbOutRecordMapper;
import com.example.wisdombookstore.mapper.TbOutRecordMapper;
import com.example.wisdombookstore.mapper.TbUserMapper;
import com.example.wisdombookstore.service.IOutRecordService;
import com.example.wisdombookstore.util.IdWorker;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.StateConstant;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;

/**
 * @author 李慧龙
 * @date 2020/10/24 16:22
 * @className OutRecodServiceImpl
 * 出店service层
 */
@Service
@Transactional
public class OutRecodServiceImpl extends ServiceImpl<TbOutRecordMapper, TbOutRecord> implements IOutRecordService {
    
    @Resource
    TbOutRecordMapper tbOutRecordMapper;
    @Resource
    IdWorker idWorker;
    @Resource
    TbUserMapper tbUserMapper;

    /**
     * 添加用户出店记录
     *
     * @param tbOutRecord:用户出店记录
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO 李慧龙 addOutRecord
     * @Date 16:47 2020/10/24
     */
    @Override
    public ResponseResult addOutRecord(TbOutRecord tbOutRecord) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断图书信息是否为空
        if (tbOutRecord == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写出店记录信息");
        }
        //根据时间跟用户的code判断是不是重复
        TbOutRecord tbOutRecord1 = tbOutRecordMapper.selectOne(new QueryWrapper<TbOutRecord>().lambda().eq(TbOutRecord::getOutDate, tbOutRecord.getOutDate()).eq(TbOutRecord::getUserCode, tbOutRecord.getUserCode()));
        if (tbOutRecord1 != null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "出店记录重复");
        }
        //判断传入的用户code值是不是合法的
        TbUser tbUser = tbUserMapper.selectOne(new QueryWrapper<TbUser>().lambda().eq(TbUser::getCode, tbOutRecord.getUserCode()));
        if (tbUser == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "用户不存在");
        }
        //添加操作
        tbOutRecord.setCode(idWorker.nextId());
        Date date = new Date();
        tbOutRecord.setCreateTime(date);
        tbOutRecord.setOutDate(date);
        tbOutRecordMapper.insert(tbOutRecord);
        resultMap.put("tbOutRecord", tbOutRecord);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 查询所有出店记录
     *
     * @param currentPage:当前页
     * @param pageSize:每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findAllOutRecord
     * @Date 18:36 2020/10/25
     */
    @Override
    public ResponseResult findAllOutRecord(Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (currentPage == null) {
            currentPage = 1;
        }
        if (pageSize == null) {
            pageSize = 2;
        }
        Page<TbOutRecord> page = new Page<>(currentPage, pageSize);
        Page<TbOutRecord> iPage = tbOutRecordMapper.selectPage(page, null);
        resultMap.put("iPage", iPage);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 根据用户code查询该用户的所有记录
     *
     * @param tbOutRecord: 用户的code标识
     * @param currentPage:  当前页
     * @param pageSize:     每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findOutRecordByUserCode
     * @Date 18:37 2020/10/25
     */
    @Override
    public ResponseResult findOutRecordByUserCode(TbOutRecord tbOutRecord, Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (tbOutRecord == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写用户的code标识");
        }
        if (currentPage == null) {
            currentPage = 1;
        }
        if (pageSize == null) {
            pageSize = 2;
        }
        TbUser tbUser = tbUserMapper.selectOne(new QueryWrapper<TbUser>().lambda().eq(TbUser::getCode, tbOutRecord.getUserCode()));
        if(tbUser==null){
            return new ResponseResult(false,StateConstant.RESPONSERESULT_CODE_FALSE,"用户不存在");
        }
        Page<TbOutRecord> page = new Page<>(currentPage, pageSize);
        Page<TbOutRecord> iPage = tbOutRecordMapper.selectPage(page, new QueryWrapper<TbOutRecord>().lambda().eq(TbOutRecord::getUserCode, tbOutRecord.getUserCode()));
        resultMap.put("iPage", iPage);
        result.setResult(resultMap);
        return result;
    }

    /**
     * 根据日期查询所有该日期的用户出店记录
     *
     * @param intoDate:
     * @param currentPage:
     * @param pageSize:
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO 李慧龙 findOutRecordByDate
     * @Date 19:10 2020/10/25
     */
    @Override
    public ResponseResult findOutRecordByDate(String intoDate, Integer currentPage, Integer pageSize) {
        ResponseResult result = new ResponseResult();
        HashMap<Object, Object> resultMap = new HashMap<>();
        //先判断参数是否为空
        if (intoDate == null) {
            return new ResponseResult(false, StateConstant.RESPONSERESULT_CODE_FALSE, "请填写日期");
        }
        if (currentPage == null) {
            currentPage = 1;
        }
        if (pageSize == null) {
            pageSize = 2;
        }
        Page<TbOutRecord> page = new Page<>(currentPage, pageSize);
        Page<TbOutRecord> iPage = tbOutRecordMapper.selectPage(page, new QueryWrapper<TbOutRecord>().lambda().like(TbOutRecord::getOutDate, intoDate));
        resultMap.put("iPage", iPage);
        result.setResult(resultMap);
        return result;

    }
}
