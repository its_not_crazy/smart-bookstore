package com.example.wisdombookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wisdombookstore.entity.TbCheckResult;
import com.example.wisdombookstore.entity.TbCheckTask;
import com.example.wisdombookstore.util.ResponseResult;

/**
 * @Author: MaHan
 * @Date: Created in 14:40 2020/10/25
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: ICheckTaskService
 */
public interface ICheckTaskService extends IService<TbCheckTask> {

    /**
     * TODO 陈阳  创建盘点任务
     * @Description: addCheck
     * @Param: * @param tbCheckTask: （  TbClerkCode         盘点人标识
     *                                  startCheckDate      开始时间
     *                                  endCheckDate       结束时间
     *                                  typeJoint          盘点类型逗号拼接
     *                                  ）
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/25
     * @Time: 9:57
     */
    public ResponseResult addCheck(TbCheckTask tbCheckTask);

    /**
    * TODO 陈阳  执行盘点任务
    * @Description: findAllCheck
    * @Param: * @param tbCheckTask:（  TbClerkCode         盘点人标识
     *                                 code          盘点任务唯一标识
     *                                 ）
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/24
    * @Time: 17:33
    */
    public ResponseResult findAllCheck(TbCheckTask tbCheckTask);

    /**
     * TODO &马晗&  根据盘点人查询盘点任务
     * @param tbCherkCode : 盘点人标识
     * @return: com.example.wisdombookstore.entity.TbCheckTask
     * @Author &马晗&
     * @Description 根据负责人查询盘点任务
     * @Date 14:43 2020/10/25
     **/
    public ResponseResult inventoryUserFindCode(Long tbCherkCode);

    /**
    * TODO 陈阳
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/10/28 9:29
     * @param tbCheckTask: ( code            盘点任务唯一标识
     *                       TbClerkCode:    盘点人唯一标识
     *                       typeJoint:           盘点类型
     *                        )
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    public ResponseResult showTbook(TbCheckTask tbCheckTask);

    /**
    * TODO 陈阳  进行图书盘点
    * @Description: updateCheckRedis
    * @Param: * @param tbCheckTask: （ TbClerkCode         盘点人标识
     *                                 code          盘点任务唯一标识
     *                                 ）
     * @param rfid:                    图书rfid
     * @param type:                    图书类型唯一标识
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/25
    * @Time: 18:58
    */
    public ResponseResult updateCheckRedis(TbCheckTask tbCheckTask,Long rfid,Long type);

    /**
    * TODO 陈阳 盘点结果提交
    * @Description: checkResult
    * @Param: * @param tbCheckResult:  （ tbCheckTaskCode   盘点任务标识）
     * @param TbClerkCode:                盘点员唯一标识
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/25
    * @Time: 20:34
    */
    public ResponseResult checkResult(TbCheckResult tbCheckResult,Long TbClerkCode);

    /**
     * TODO &马晗& 查询所有盘点任务
     * @param currentPage:  当前页
     * @param pageSize:  每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 18:49 2020/10/25
     **/
    public ResponseResult inventoryUserFind(Integer currentPage,Integer pageSize);
}
