package com.example.wisdombookstore.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wisdombookstore.entity.TbShoppingBook;
import com.example.wisdombookstore.entity.TbShoppingGoods;
import com.example.wisdombookstore.mapper.TbShoppingBookMapper;
import com.example.wisdombookstore.mapper.TbShoppingGoodsMapper;
import com.example.wisdombookstore.service.IShoppingBookService;
import com.example.wisdombookstore.service.IShoppingGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 宋合
 * @progrom: IntelliJ IDEA
 * @description: 购物车单品实现类
 * @date 2020/10/22 10:27
 */
@Service
@Slf4j
public class ShoppingBookServiceImpl extends ServiceImpl<TbShoppingBookMapper, TbShoppingBook> implements IShoppingBookService {

}
