package com.example.wisdombookstore;


import com.example.wisdombookstore.util.IdWorker;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @ClassName WindstormApplication
 * @Description TODO
 * @Author lenovo
 * @Date 2020/10/20 9:05
 * @Version 1.0
 */
@MapperScan("com.example.*.mapper")
@SpringBootApplication
public class WisdombookstoreApplication {
    public static void main(String[] args) {
        SpringApplication.run(WisdombookstoreApplication.class, args);
    }

    @Bean
    public IdWorker idWorker() {
        return new IdWorker(1, 1);
    }
}
