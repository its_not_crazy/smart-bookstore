package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbBookInfo;
import com.example.wisdombookstore.service.impl.BookInfoServiceImpl;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 李慧龙
 * @date 2020/10/22 11:25
 * @className BookInfoController
 */
@RestController
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "接口-书籍的管理模块")
public class BookInfoController {
    /**
     * 书籍service层的注入
     */
    @Resource
    BookInfoServiceImpl bookInfoService;


    /**
     * 添加书籍信息
     *
     * @param :书籍信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO addTbBookInfo
     * @Date 11:17 2020/10/22
     */
    @PostMapping("/addTbBookInfo")
    @ApiOperation(value = "添加书籍信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "书籍名称", required = true,dataType = "String"),
            @ApiImplicitParam(name = "author", value = "书籍作者", required = true,dataType = "String"),
            @ApiImplicitParam(name = "issuanceTime", value = "书籍发行期", required = true,dataType = "Date"),
            @ApiImplicitParam(name = "content", value = "书籍描述", required = true,dataType = "String"),
            @ApiImplicitParam(name = "price", value = "书籍价格", required = true,dataType = "Double"),
            @ApiImplicitParam(name = "tbBookTypeCode", value = "书籍类别", required = true,dataType = "Long"),
            @ApiImplicitParam(name = "tbBookImageCodes", value = "书籍照片信息集合", required = true,dataType = "List")
    })
    public ResponseResult addTbBookInfo(@ApiIgnore @RequestBody TbBookInfo tbBookInfo,
                                        @RequestParam("tbBookImageCodes")String tbBookImageCodes) {
        return this.bookInfoService.addTbBootInfo(tbBookInfo,tbBookImageCodes);
    }

    /**
     * 删除书籍信息
     *
     * @param : 书籍信息的code值
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO deleteTbBootInfo
     * @Date 11:31 2020/10/22
     */
    @PostMapping("/deleteTbBootInfo")
    @ApiOperation(value = "删除书籍信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "书籍的code值", required = true,dataType = "Long")
    })
    public ResponseResult deleteTbBootInfo(@ApiIgnore @RequestBody TbBookInfo tbBookInfo) {
        return bookInfoService.deleteTbBootInfo(tbBookInfo);
    }

    /**
     * 修改书籍信息
     *
     * @param :
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO updateTbBookInfo
     * @Date 11:33 2020/10/22
     */
    @PostMapping("/updateTbBookInfo")
    @ApiOperation(value = "修改书籍信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "书籍code", required = true,dataType = "Long"),
            @ApiImplicitParam(name = "name", value = "书籍名称", required = true,dataType = "String"),
            @ApiImplicitParam(name = "author", value = "书籍作者", required = true,dataType = "String"),
            @ApiImplicitParam(name = "issuanceTime", value = "书籍发行期", required = true,dataType = "Date"),
            @ApiImplicitParam(name = "content", value = "书籍描述", required = true,dataType = "String"),
            @ApiImplicitParam(name = "price", value = "书籍价格", required = true,dataType = "Double"),
            @ApiImplicitParam(name = "tbBookTypeCode", value = "书籍类别", required = true,dataType = "Long"),
            @ApiImplicitParam(name = "imageStr",value = "图片的code值拼接后的字符串",dataType = "String")
    })
    public ResponseResult updateTbBookInfo(@ApiIgnore @RequestBody TbBookInfo tbBookInfo,@RequestParam("imageStr")String imageStr) {
        return bookInfoService.updateTbBootInfo(tbBookInfo,imageStr);
    }

    /**
     * 查询所有书籍信息
     *
     * @param currentPage:
     * @param pageSize:
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO findAllTbBootInfo
     * @Date 11:33 2020/10/22
     */
    @PostMapping("/findAllTbBootInfo")
    @ApiOperation(value = "查询所有书籍信息(带分页效果)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currentPage", value = "当前页数", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true,dataType = "Integer")
    })
    public ResponseResult findAllTbBootInfo(@RequestParam("currentPage") Integer currentPage,
                                            @RequestParam("pageSize") Integer pageSize) {
        return bookInfoService.findAllTbBootInfo(currentPage, pageSize);
    }

    /**
     * 根据书籍的code值查询书籍信息
     *
     * @param :
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO findOnetbBookInfoByCode
     * @Date 11:33 2020/10/22
     */
    @PostMapping("/findOnetbBookInfoByCode")
    @ApiOperation(value = "根据code查询书籍信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "书籍的code值", required = true,dataType = "Long")
    })
    public ResponseResult findOnetbBookInfoByCode(@ApiIgnore @RequestBody TbBookInfo tbBookInfo) {
        return bookInfoService.findOnetbBookInfoByCode(tbBookInfo);
    }

    @PostMapping("/findAllInfo")
    @ApiOperation(value = "查询所有书籍信息(正常的,用于添加图书时追加下拉框)")
    public ResponseResult findAllInfo(){
        return bookInfoService.findAllInfo();
    }

    @PostMapping("/findAllAll")
    @ApiOperation(value = "查询所有书籍信息")
    public ResponseResult findAllAll(){
        return bookInfoService.findAllAll();
    }

}
