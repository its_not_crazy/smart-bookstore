package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.*;
import com.example.wisdombookstore.service.IClerkService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @ClassName ClerkController
 * @Description TODO 8aceMak1r ClerkController 工作人员控制层
 * @Author 8aceMak1r
 * @Date 2020/10/22 14:54
 * @Version 1.0
 */
@Slf4j
@CrossOrigin
@Api(tags = "接口-工作人员模块")
@RestController
public class ClerkController {

    @Autowired
    IClerkService clerkService;

    /**
     * TODO 8aceMak1r clerkLogin 工作人员登录
     *
     * @param clerk 工作人员信息
     * @param session
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 登录功能
     * @author 8aceMak1r
     * @date 2020/10/22 15:07
     */
    @ApiOperation(value = "工作人员登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mobile", value = "员工的手机号", required = true),
            @ApiImplicitParam(name = "password", value = "密码", required = true)
    })
    @PostMapping("/clerkLogin")
    public ResponseResult clerkLogin(@RequestBody TbClerk clerk, HttpSession session) {
        return clerkService.clerkLogin(clerk, session);
    }


    /**
     * TODO 8aceMak1r clerkLogout 退出登录
     *
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 退出登录
     * @param request
     * @author 8aceMak1r
     * @date 2020/10/22 15:23
     */
    @ApiOperation(value = "退出登录")
    @ApiImplicitParams({
    })
    @PostMapping("/clerkLogout")
    public ResponseResult clerkLogout(HttpServletRequest request) {
        return clerkService.clerkLogout(request);
    }

    /**
     * TODO 8aceMak1r addClerk 新增店员
     *
     * @param clerk 店员信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 店长新增店员
     * @author 8aceMak1r
     * @date 2020/10/22 11:05
     */
    @ApiOperation(value = "新增店员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "姓名", required = true),
            @ApiImplicitParam(name = "password", value = "登录密码", required = true),
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true),
            @ApiImplicitParam(name = "permissions", value = "工作岗位", required = true),
            @ApiImplicitParam(name = "status", value = "状态", required = true),
            @ApiImplicitParam(name = "bookStoreCode", value = "书店", required = true),
            @ApiImplicitParam(name = "idCard", value = "身份证号", required = true),
            @ApiImplicitParam(name = "age", value = "年龄", required = true),
            @ApiImplicitParam(name = "address", value = "地址", required = true),
            @ApiImplicitParam(name = "createTime", value = "入职时间", required = true)
    })
    @PostMapping("/addClerk")
    public ResponseResult addClerk(@RequestBody TbClerk clerk) {
        return clerkService.addClerk(clerk);
    }


    /**
     * TODO 8aceMak1r getClerkList 查看店员
     *
     * @param pageCurrent 当前页
     * @param pageSize    总页数
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 查看所有店员
     * @author 8aceMak1r
     * @date 2020/10/22 11:16
     */
    @ApiOperation(value = "查看店员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true),
            @ApiImplicitParam(name = "pageCurrent", value = "当前页数", required = true),
    })
    @PostMapping("/getClerkList")
    public ResponseResult getClerkList(@RequestParam("pageSize") Integer pageSize,
                                       @RequestParam("pageCurrent") Integer pageCurrent) {
        return clerkService.getClerkList(pageSize, pageCurrent);
    }

    /**
     * TODO 8aceMak1r getClerkById 查看店员详情
     * @param tbUserId 店员信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 查看店员详情信息
     * @author 8aceMak1r
     * @date 2020/10/22 11:20
     */
    @ApiOperation(value = "查看店员详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbUserId", value = "店员标识", required = true)
    })
    @PostMapping("/getClerkById")
    public ResponseResult getClerkById(@RequestParam("tbUserId") Long tbUserId) {
        return clerkService.getClerkById(tbUserId);
    }




    /**
     * TODO 8aceMak1r setClerkById 修改店员信息
     *
     * @param clerk 店员信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 修改店员信息
     * @author 8aceMak1r
     * @date 2020/10/22 11:22
     */
    @ApiOperation(value = "编辑店员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "店员标识", required = true),
            @ApiImplicitParam(name = "name", value = "姓名", required = true),
            @ApiImplicitParam(name = "password", value = "登录密码", required = true),
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true),
            @ApiImplicitParam(name = "permissions", value = "工作岗位", required = true),
            @ApiImplicitParam(name = "status", value = "状态", required = true),
            @ApiImplicitParam(name = "updateTime", value = "更新时间", required = true)
    })
    @PostMapping("/setClerkById")
    public ResponseResult setClerkById(@RequestBody TbClerk clerk) {
        return clerkService.setClerkById(clerk);
    }

    /**
     * TODO 8aceMak1r setClerkPassword 修改密码
     *
     * @param tbUserId    工作人员
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 修改工作人员密码
     * @author 8aceMak1r
     * @date 2020/10/22 11:41
     */
    @ApiOperation(value = "修改店员密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbUserId", value = "店员标识", required = true),
            @ApiImplicitParam(name = "oldPassword", value = "旧密码", required = true),
            @ApiImplicitParam(name = "newPassword", value = "新密码", required = true)
    })
    @PostMapping("/setClerkPassword")
    public ResponseResult setClerkPassword(@RequestParam("tbUserId") Long tbUserId,
                                           @RequestParam("oldPassword") String oldPassword,
                                           @RequestParam("newPassword") String newPassword) {
        return clerkService.setClerkPassword(tbUserId, oldPassword, newPassword);
    }

    /**
     * TODO 8aceMak1r delClerkById 删除店员
     *
     * @param tbUserId 店员
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 删除店员
     * @author 8aceMak1r
     * @date 2020/10/22 11:28
     */
    @ApiOperation(value = "删除店员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbClerkCode", value = "店员标识", required = true,dataType = "Long"),
    })
    @PostMapping("/delClerkById")
    public ResponseResult delClerkById(@RequestParam("tbClerkCode") Long tbUserId) {
        return clerkService.delClerkById(tbUserId);
    }

    /**
     * TODO clerkList 8aceMak1r 查询所有员工信息
     * @Description: 查询所有员工信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/27 19:52
     */
    @ApiOperation(value = "查询所有员工信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页数", required = true),
            @ApiImplicitParam(name = "size", value = "每页条数", required = true),
    })
    @PostMapping("/clerkList")
    public ResponseResult clerkList(@RequestParam("page") Integer page,
                                    @RequestParam("size") Integer size){
        return clerkService.clerkList(page,size);
    }

    /**
     * TODO clerkListshow 8aceMak1r 查询所有员工信息
     * @Description: 查询所有员工信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/27 19:52
     */
    @ApiOperation(value = "查询所有员工信息")
    @PostMapping("/clerkListshow")
    public ResponseResult clerkList(){
        return clerkService.clerkList();
    }
}
