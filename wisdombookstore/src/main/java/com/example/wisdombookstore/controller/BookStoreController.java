package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbBookStore;
import com.example.wisdombookstore.service.IBookStoreService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * @Author: MaHan
 * @Date: Created in 9:59 2020/10/22
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: BookStoreController
 */
@RestController
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/bookStore")
@Api(tags = "接口-书店管理")
public class BookStoreController {

    @Resource
    private IBookStoreService iBookStoreService;

    /**
     * TODO &马晗& 书店所有信息
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description 查询书店所有信息进行分页
     * @Date 10:15 2020/10/22
     **/
    @ApiOperation(value = "书店列表分页", notes = "返回书店所有信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="currentPage", value = "当前页", dataType = "int", required=true, example = "0"),
            @ApiImplicitParam(name="pageSize", value = "每页条数", dataType = "int", required=true, example = "0"),
    })
    @PostMapping("/findAllBookStore")
    public ResponseResult findAllBookStore(Integer currentPage,Integer pageSize){
        System.out.println(pageSize);
        return this.iBookStoreService.findAllBookStore(currentPage,pageSize);
    }

    /**
     * TODO &马晗& 书店所有信息
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description 查询书店所有信息进行分页
     * @Date 10:15 2020/10/22
     **/
    @ApiOperation(value = "书店列表", notes = "返回书店所有信息")
    @PostMapping("/findAllBookStorenotpage")
    public ResponseResult findAllBookStorenotpage(){
        return this.iBookStoreService.findAllBookStore();
    }

    /**
     * TODO &马晗& 添加书店信息
     * @param tbBookStore: 书店实体对象
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 10:49 2020/10/22
     **/
    @ApiOperation(value = "添加书店", notes = "添加书店")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="storeName", value = "书店名称", required=true),
            @ApiImplicitParam(name="address", value = "书店地址", required=true),
            @ApiImplicitParam(name="tbClerkCode", value = "店长标识", required=true),
            @ApiImplicitParam(name="status", value = "书店状态", required=true)
    })
    @PostMapping("/addBookStore")
    public ResponseResult addBookStore(@ApiIgnore @RequestBody TbBookStore tbBookStore){


        return this.iBookStoreService.addBookStore(tbBookStore);
    }

    /**
     * TODO &马晗& 根据书店标识查询书店
     * @param tbBookStore:   书店书体类
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  根据书店唯一标识code查询书店所有信息
     * @Date 10:50 2020/10/22
     **/
    @ApiOperation(value = "根据标识查询书店", notes = "根据标识查询书店")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="code", value = "书店标识", required=true)
    })
    @PostMapping("/findByIdBookStore")
    public ResponseResult findByIdBookStore (@ApiIgnore  TbBookStore tbBookStore){
       return this.iBookStoreService.delBookStore(tbBookStore.getCode());
    }

    /**
     * TODO &马晗& 删除书店
     * @param tbBookStore: 书店实体类
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  根据书店唯一标识code 删除书店（执行软删除 数据库修改书店状态  0.营业中 1.未营业）
     * @Date 10:52 2020/10/22
     **/
    @ApiOperation(value = "删除书店", notes = "根据标识删除书店书店")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="code", value = "书店标识", required=true)
    })
    @PostMapping("/delBookStore")
    public ResponseResult delBookStore (@ApiIgnore  TbBookStore tbBookStore){
        return this.iBookStoreService.delBookStore(tbBookStore.getCode());
    }

    /**
     * TODO &马晗& 查询所有店员
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 19:28 2020/10/27
     **/
    @ApiOperation(value = "查询所有店员", notes = "通过书店标识查询所有店员")
    @PostMapping("/findClerk")
    public ResponseResult findClerk(){
        return iBookStoreService.findAllClerk();
    }


}
