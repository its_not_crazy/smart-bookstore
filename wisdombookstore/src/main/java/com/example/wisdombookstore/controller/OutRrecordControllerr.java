package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbOutRecord;
import com.example.wisdombookstore.service.IOutRecordService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author 李慧龙
 * @date 2020/10/25 15:44
 * @className OutRrecordControllerr
 */
@RestController
@Slf4j
@CrossOrigin
@Api(tags = "接口-用户出店的管理模块")
public class OutRrecordControllerr {
    
    @Resource
    IOutRecordService iOutRecordService;

    @PostMapping("/addOutRecord")
    @ApiOperation(value = "添加用户出店 记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userCode", value = "用户的code标识", required = true,dataType = "Long")
    })
    public ResponseResult addOutRecord(@ApiIgnore TbOutRecord tbOutRecord){
        return iOutRecordService.addOutRecord(tbOutRecord);
    }

    @PostMapping("/findAllOutRecord")
    @ApiOperation(value = "查询所有用户出店 记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currentPage", value = "当前页", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true,dataType = "Integer")
    })
    public ResponseResult findAllOutRecord(Integer currentPage,Integer pageSize){
        return iOutRecordService.findAllOutRecord(currentPage,pageSize);
    }

    @PostMapping("/findOutRecordByUserCode")
    @ApiOperation(value = "根据用户code查询该用户的所有记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userCode", value = "用户的code标识", required = true,dataType = "Long"),
            @ApiImplicitParam(name = "currentPage", value = "当前页", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true,dataType = "Integer")
    })
    public ResponseResult findOutRecordByUserCode(@ApiIgnore TbOutRecord tbOutRecord, Integer currentPage, Integer pageSize){
        return iOutRecordService.findOutRecordByUserCode(tbOutRecord,currentPage,pageSize);
    }

    @PostMapping("/findOutRecordByDate")
    @ApiOperation(value = "根据日期查询所有该日期的用户出店 记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "outDate", value = "出店时间", required = true,dataType = "String"),
            @ApiImplicitParam(name = "currentPage", value = "当前页", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true,dataType = "Integer")
    })
    public ResponseResult findOutRecordByDate(String outDate, Integer currentPage, Integer pageSize){
        return iOutRecordService.findOutRecordByDate(outDate,currentPage,pageSize);
    }
}
