package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbCollect;
import com.example.wisdombookstore.service.ICollectService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 收藏控制层
 * @date 2020/10/23 15:03
 */
@RestController
@CrossOrigin
@RequestMapping("/CollectController")
@Api(tags = "接口-收藏夹")
public class CollectController {

    @Resource
    private ICollectService iCollectService;


    /**
     * TODO 陈阳   用户添加收藏
     * @Description: addColler
     * @Param: * @param tbCollect:  （tbUserCode     用户唯一标识
     *                               tbBookInfoCode 商品唯一标识
     *                               ）
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/23
     * @Time: 14:53
     */
    @ApiOperation("用户添加收藏")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbUserCode",value = "用户唯一标识",dataType = "Long" ),
            @ApiImplicitParam(name = "tbBookInfoCode",value = "商品唯一标识",dataType = "Long" )
    })
    @PostMapping("/addColler")
    public ResponseResult addColler(@ApiIgnore @ModelAttribute TbCollect tbCollect){
        return this.iCollectService.addColler(tbCollect);
    }

    /**
     * TODO 陈阳    用户修改收藏
     * @Description: updateColler
     * @Param: * @param tbCollect:（code  修改的这条收藏的唯一标识
     *                             tbUserCode     用户唯一标识
     *                             tbBookInfoCode 商品唯一标识
     *                              ）
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/23
     * @Time: 14:53
     */
    @ApiOperation("用户修改收藏")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code",value = "收藏唯一标识",dataType = "Long" ),
            @ApiImplicitParam(name = "tbUserCode",value = "用户唯一标识",dataType = "Long" ),
            @ApiImplicitParam(name = "tbBookInfoCode",value = "商品唯一标识",dataType = "Long" )
    })
    @PostMapping("/updateColler")
    public ResponseResult updateColler(@ApiIgnore @ModelAttribute TbCollect tbCollect){
        return this.iCollectService.updateColler(tbCollect);
    }

    /**
     * TODO 陈阳   用户删除收藏
     * @Description: delColler
     * @Param: * @param tbCollect: （code  修改的这条收藏的唯一标识)
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/23
     * @Time: 14:53
     */
    @ApiOperation("用户删除收藏")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code",value = "收藏唯一标识",dataType = "long" )
    })
    @PostMapping("/delColler")
    public ResponseResult delColler(@ApiIgnore @ModelAttribute TbCollect tbCollect){
        System.err.println(tbCollect);
        return this.iCollectService.delColler(tbCollect);
    }


    /**
     * TODO 陈阳   用户分页查询自己的收藏夹
     * @Description: pageFindAllUserColler
     * @Param: * @param tbCollect:（tbUserCode  用户的唯一标识)
     * @param page:  当前页
     * @param size:  每页几条
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/23
     * @Time: 14:57
     */
    @ApiOperation("用户分页查询自己的收藏夹")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbUserCode",value = "用户唯一标识",dataType = "Long" ),
            @ApiImplicitParam(name = "page",value = "当前页",dataType = "int" ),
            @ApiImplicitParam(name = "size",value = "每页几条",dataType = "int" )
    })
    @PostMapping("/pageFindAllUserColler")
    public ResponseResult pageFindAllUserColler(@ApiIgnore @ModelAttribute TbCollect tbCollect, int page, int size){
        return this.iCollectService.pageFindAllUserColler(tbCollect,page,size);
    }


    /**
     * TODO 陈阳    分页查询所有的收藏夹，可根据用户查询，可根据收藏的图书查询
     * @Description: pageFindAllColler
     * @Param: * @param tbCollect:   （tbUserCode     用户唯一标识
     *                               tbBookInfoCode 商品唯一标识
     *                               ）
     * @param page:  当前页
     * @param size:  每页几条
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/23
     * @Time: 14:57
     */
    @ApiOperation("分页查询所有的收藏夹，可根据用户查询，可根据收藏的图书查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbUserCode",value = "用户唯一标识",dataType = "Long" ),
            @ApiImplicitParam(name = "tbBookInfoCode",value = "商品唯一标识",dataType = "Long" ),
            @ApiImplicitParam(name = "page",value = "当前页",dataType = "int" ),
            @ApiImplicitParam(name = "size",value = "每页几条",dataType = "int" )
    })
    @PostMapping("/pageFindAllColler")
    public ResponseResult pageFindAllColler(@ApiIgnore @ModelAttribute TbCollect tbCollect,int page,int size){
        return this.iCollectService.pageFindAllColler(tbCollect,page,size);
    }


}
