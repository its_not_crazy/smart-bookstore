package com.example.wisdombookstore.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * TODO 8aceMak1r faceController 人脸识别控制层
 * @ClassName faceController
 * @Description
 * @Author 8aceMak1r
 * @Date 2020/10/28 11:17
 * @Version 1.0
 */

@Controller
public class FaceController {

    /**
     * TODO 8aceMak1r toIndex 进入主页面
     * @Description:
     * @return java.lang.String
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/28 20:48
     */
    @RequestMapping("/")
    public String toIndex() {
        return "redirect:/index";
    }

    /**
     * TODO 8aceMak1r index 进入主页面
     * @Description:
     * @return java.lang.String
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/28 20:48
     */
    @RequestMapping("/index")
    public String index() {
        return "/index";
    }

    /**
     * TODO 8aceMak1r regionFace 跳转注册页面
     * @Description:
     * @return java.lang.String
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/28 20:49
     */
    @RequestMapping("/jumpRegionFace/{userCode}")
    public ModelAndView regionFace(@PathVariable("userCode")Long userCode){
        System.out.println("wqeqweq==========we"+userCode);
        return new ModelAndView("faceReg","userCode",userCode);
    }

    /**
     * TODO 8aceMak1r getLoginFace 跳转登录页面
     * @Description:
     * @return java.lang.String
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/28 20:49
     */
    @RequestMapping("/jumpLoginFace")
    public String getLoginFace(){
        return "/getface";
    }

    /**
     * TODO 8aceMak1r faceLoginSucceed 跳转登录成功页面
     * @Description:
     * @return java.lang.String
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/28 20:49
     */
    @RequestMapping("/facesucceed")
    public String faceLoginSucceed(){
        System.out.println(1222222);
        return "succeed";
    }

}
