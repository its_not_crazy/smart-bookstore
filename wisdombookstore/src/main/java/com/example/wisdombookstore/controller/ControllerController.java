package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.service.ICromService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: wisdombookstore
 * @description:
 * @author: 陈阳
 * @create: 2020-10-29 19:50
 */
@RestController
@CrossOrigin
@RequestMapping("/ControllerController")
@Api(tags = "接口-定时任务规则添加")
public class ControllerController {

    @Resource
    private ICromService iCromService;

    /*
     * TODO 陈阳   定时任务规则添加
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/10/28 22:21
     * @param crom:   定时任务规则
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation("定时任务规则添加")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "crom",value = "定时任务规则",dataType = "Integer" )
    })
    @PostMapping("/addCrom")
    public ResponseResult addCrom(Integer crom){
        return this.iCromService.addCrom(crom);
    }
}
