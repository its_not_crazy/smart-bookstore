package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.service.ICheckResultService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: MaHan
 * @Date: Created in 16:13 2020/10/24
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: CheckResultController
 */
@RestController
@Slf4j
@RequestMapping("/CheckResult")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "接口-盘点结果")
public class CheckResultController {
    @Autowired
    private ICheckResultService iCheckResultService;
    /**
     * TODO &马晗& 盘点结果(所有)
     * @param currentPage:
     * @param pageSize:
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description 盘点任务完成生成盘点表 查询盘点结果数据
     * @Date 16:18 2020/10/24
     **/
    @ApiOperation(value = "盘点结果(所有)", notes = "返回所有盘点结果信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="currentPage", value = "当前页", dataType = "int", required=true, example = "0"),
            @ApiImplicitParam(name="pageSize", value = "每页条数", dataType = "int", required=true, example = "0"),
    })
    @PostMapping("/inventoryResults")
    public ResponseResult inventoryResults(Integer currentPage, Integer pageSize){
        return this.iCheckResultService.inventoryResults(currentPage, pageSize);
    }

    /**
     * TODO &马晗& 盘点结果
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description 盘点任务完成生成盘点表 查询盘点结果数据
     * @Date 16:18 2020/10/24
     **/
    @ApiOperation(value = "盘点结果", notes = "返回所有盘点结果信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="code", value = "盘点任务唯一标识", dataType = "int", required=true, example = "0")
    })
    @PostMapping("/inventoryResultCode")
    public ResponseResult inventoryResults(Long code){
        return this.iCheckResultService.inventoryResults(code);
    }

    /**
    * TODO 陈阳    盘点结果缺失书籍
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/11/1 15:51
     * @param code:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "盘点结果缺失书籍", notes = "盘点结果缺失书籍")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="code", value = "盘点任务唯一标识", dataType = "int", required=true, example = "0")
    })
    @PostMapping("/inventoryResultsDis")
    public ResponseResult inventoryResultsDis(Long code){
        return this.iCheckResultService.inventoryResultsDis(code);
    }
}
