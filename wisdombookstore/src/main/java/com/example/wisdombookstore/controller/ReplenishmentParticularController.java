package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.service.IReplenishmentParticularService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName ReplenishmentParticularController
 * @Description TODO
 * @Author 8aceMak1r
 * @Date 2020/10/26 19:12
 * @Version 1.0
 */
@RestController
@Slf4j
@RequestMapping("/ReplenishmentParticular")
@Api(tags = "接口-补货任务详情")
public class ReplenishmentParticularController {

    @Autowired
    private IReplenishmentParticularService replenishmentParticularService;

    /**
     * TODO 8aceMak1r getReplenishmentParticular 查看任务详情
     * @param tbReplenishmentResultCode 结果的code
     * @Description: 查看任务的详情
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/26 19:26
     */
    @ApiOperation(value = "查看任务详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbReplenishmentResultCode", value = "任务结果code", required = true)
    })
    @PostMapping("/getReplenishmentParticular/{tbReplenishmentResultCode}")
    public ResponseResult getReplenishmentParticular(@PathVariable("tbReplenishmentResultCode") Long tbReplenishmentResultCode){
        return replenishmentParticularService.getReplenishmentParticular(tbReplenishmentResultCode);
    }
}
