package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbShoppingCart;
import com.example.wisdombookstore.service.IShoppingCartService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 购物车控制层
 * @date 2020/10/22 10:32
 */
@RestController
@CrossOrigin
@RequestMapping("/ShoppingCart")
@Api(tags = "接口-购物车")
public class ShoppingCartController {

    @Resource
    private IShoppingCartService iShoppingCartService;


    /**
     * TODO &宋合&
     * @Description: 购物车添加书籍
     * @Author: 宋合
     * @Date: 2020/10/27 15:39
     * @param rfId:
     * @param tbUserCode:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation("购物车添加书籍")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbUserCode",value = "用户唯一标识",dataType = "Long" ),
            @ApiImplicitParam(name = "rfId",value = "图书唯一标识",dataType = "Long" )
    })
    @PostMapping("/addShopping")
    public ResponseResult addShopping(String rfId,Long tbUserCode){
        return this.iShoppingCartService.addShopping(rfId,tbUserCode);
    }


    /**
    * TODO 陈阳  updateShopping 修改购物车书籍
    * @Description: updateShopping
    * @Param: * @param tbShoppingCart: （code ：  需要修改此条购物消息的唯一标识
     *                                    tbUserCode： 用户唯一标识；
     *                                    tbbookCode ： 图书唯一标识 ）
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/22
    * @Time: 10:53
    */
    @ApiOperation("修改购物车书籍")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code",value = "此条购物消息的唯一标识",dataType = "Long" ),
            @ApiImplicitParam(name = "tbUserCode",value = "用户唯一标识",dataType = "Long" ),
            @ApiImplicitParam(name = "tbBookCode",value = "图书唯一标识",dataType = "Long" )
    })
    //@PostMapping("/updateShopping")
    public ResponseResult updateShopping(@ApiIgnore TbShoppingCart tbShoppingCart){

        return this.iShoppingCartService.updateShopping(tbShoppingCart);
    }



    /**
    * TODO 陈阳  pageFindUserAllShopping 用户分页查询自己的购物车书籍
    * @Description: pageFindUserAllShopping
    * @Param: * @param tbShoppingCart: （tbUserCode： 用户唯一标识)
    * @return: * @return: com.example.wisdombookstore.util.ResponseResult
    * @Author: 陈阳
    * @Date: 2020/10/22
    * @Time: 10:52
    */
    @ApiOperation("用户分页查询自己的购物车书籍")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbUserCode",value = "用户唯一标识",dataType = "Long" ),
    })
    @PostMapping("/pageFindUserAllShopping")
    public ResponseResult pageFindUserAllShopping(@RequestParam("tbUserCode") Long tbUserCode){
        return this.iShoppingCartService.pageFindUserAllShopping(tbUserCode);
    }



}
