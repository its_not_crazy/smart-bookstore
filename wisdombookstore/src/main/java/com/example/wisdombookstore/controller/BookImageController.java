package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbBookImage;
import com.example.wisdombookstore.service.IBookImageService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author 李慧龙
 * @date 2020/10/23 10:15
 * @className BookImageController
 */
@RestController
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "接口-书籍照片的管理模块")
public class BookImageController {
    @Resource
    IBookImageService iBookImageService;
    /**
     * 添加书籍照片信息
     * @Author 李慧龙
     * @Description //TODO addTbBookInfoImages
     * @Date 9:56 2020/10/23
     * @param file:照片文件
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    @PostMapping("/addTbBookInfoImages")
    @ApiOperation(value = "添加图书照片信息")
    @ApiImplicitParam(name = "file", value = "图书照片信息", required = true,dataType = "File")
    public ResponseResult addTbBookInfoImages(@RequestParam("file") MultipartFile[] file){
        return iBookImageService.addTbBookInfoImages(file);
    }
}
