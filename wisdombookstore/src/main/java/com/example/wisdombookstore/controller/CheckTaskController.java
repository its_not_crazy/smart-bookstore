package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbCheckResult;
import com.example.wisdombookstore.entity.TbCheckTask;
import com.example.wisdombookstore.service.ICheckTaskService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 盘点任务控制层
 * @date 2020/10/24 19:19
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Slf4j
@RequestMapping("/CheckTask")
@Api(tags = "接口-盘点任务")
public class CheckTaskController {

    @Resource
    private ICheckTaskService iCheckTaskService;


    /**
     * TODO 陈阳  创建盘点任务
     * @Description: addCheck
     * @Param: * @param tbCheckTask: （  clerkCode         盘点人标识
     *                                  startCheckDate      开始时间
     *                                  endCheckDate       结束时间
     *                                  typeJoint          盘点类型逗号拼接
     *                                  ）
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/25
     * @Time: 9:57
     */
    @ApiOperation("创建盘点任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbClerkCode",value = "盘点人标识",dataType = "Long",required = true),
            @ApiImplicitParam(name = "typeJoint",value = "盘点类型逗号拼接",dataType = "String",required = true),
            @ApiImplicitParam(name = "startCheckDate",value = "开始时间,例如：2020-10-25 09:10:20",dataType = "String",required = true),
            @ApiImplicitParam(name = "endCheckDate",value = "结束时间，例如：2020-10-25 10:10:20",dataType = "String",required = true)
    })
    @PostMapping("/addCheck")
    /*@ApiIgnore @RequestBody TbCheckTask tbCheckTask*/
    public ResponseResult addCheck(@ApiIgnore @RequestBody TbCheckTask tbCheckTask){
        /*tbCheckTask.setTbClerkCode(clerkCode);*/
        return this.iCheckTaskService.addCheck(tbCheckTask);
    }

    /**
     * TODO 陈阳  执行盘点任务
     * @Description: findAllCheck
     * @Param: * @param tbCheckTask:（  TbClerkCode         盘点人标识
     *                                 code          盘点任务唯一标识
     *                                 ）
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/24
     * @Time: 17:33
     */
    @ApiOperation("执行盘点任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code",value = "盘点任务唯一标识",dataType = "long",required = true),
            @ApiImplicitParam(name = "TbClerkCode",value = "盘点人标识",dataType = "long",required = true)
    })
    @PostMapping("/findAllCheck")
    public ResponseResult findAllCheck(@ApiIgnore @RequestBody TbCheckTask tbCheckTask){
        System.err.println("执行盘点任务");
        return this.iCheckTaskService.findAllCheck(tbCheckTask);
    }

    /**
     * TODO 陈阳  进行图书盘点
     * @Description: updateCheckRedis
     * @Param: * @param tbCheckTask: （ TbClerkCode         盘点人标识
     *                                 code          盘点任务唯一标识
     *                                 ）
     * @param rfid:                    图书rfid
     * @param type:                    图书类型唯一标识
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/25
     * @Time: 18:58
     */
    @ApiOperation("进行图书盘点")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code",value = "盘点任务唯一标识",dataType = "long",required = true),
            @ApiImplicitParam(name = "TbClerkCode",value = "盘点人标识",dataType = "long",required = true),
            @ApiImplicitParam(name = "rfid",value = "图书rfid",dataType = "long",required = true),
            @ApiImplicitParam(name = "type",value = "图书类型唯一标识",dataType = "long",required = true)
    })
    @PostMapping("/updateCheckRedis")
    public ResponseResult updateCheckRedis(@ApiIgnore @RequestBody TbCheckTask tbCheckTask,Long rfid,Long type){
        return this.iCheckTaskService.updateCheckRedis(tbCheckTask, rfid, type);
    }

    /**
     * TODO 陈阳 盘点结果提交
     * @Description: checkResult
     * @Param: * @param tbCheckResult:  （ tbCheckTaskCode   盘点任务标识）
     * @param TbClerkCode:                盘点员唯一标识
     * @return: * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author: 陈阳
     * @Date: 2020/10/25
     * @Time: 20:34
     */
    @ApiOperation("盘点结果提交")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbCheckTaskCode",value = "盘点任务标识",dataType = "long",required = true),
            @ApiImplicitParam(name = "TbClerkCode",value = "盘点人标识",dataType = "long",required = true)
    })
    @PostMapping("/checkResult")
    public ResponseResult checkResult(@ApiIgnore @RequestBody TbCheckResult tbCheckResult, Long TbClerkCode){
        return this.iCheckTaskService.checkResult(tbCheckResult, TbClerkCode);
    }




    /**
     * TODO &马晗& 负责人查询盘点任务
     * @param tbCherkCode:  负责人标识
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 18:39 2020/10/25
     **/
    @ApiOperation(value = "负责人查询盘点任务", notes = "返回该负责人的任务信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="tbCherkCode", value = "负责人标识",required=true)
    })
    @PostMapping("/inventoryUserFindCode")
    public ResponseResult inventoryUserFindCode(Long tbCherkCode){
        return this.iCheckTaskService.inventoryUserFindCode(tbCherkCode);
    }

    /**
     * TODO &马晗& 查看所有盘点任务
     * @param currentPage:  当前页
     * @param pageSize:  每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 19:59 2020/10/25
     **/
    @ApiOperation(value = "所有盘点任务", notes = "返回盘点任务所有信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="currentPage", value = "当前页", dataType = "int", required=true, example = "0"),
            @ApiImplicitParam(name="pageSize", value = "每页条数", dataType = "int", required=true, example = "0"),
    })
    @PostMapping("/inventoryUserFind")
    public ResponseResult inventoryUserFind(Integer currentPage, Integer pageSize) {
        return this.iCheckTaskService.inventoryUserFind(currentPage, pageSize);
    }

    /**
     * TODO 陈阳
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/10/28 9:29
     * @param tbCheckTask: ( code            盘点任务唯一标识
     *                       TbClerkCode:    盘点人唯一标识
     *                       typeJoint:           盘点类型
     *                        )
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "盘点任务书籍查询", notes = "盘点任务书籍详细信息查询")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="code", value = "盘点任务唯一标识", dataType = "long", required=true, example = "0"),
            @ApiImplicitParam(name="TbClerkCode", value = "盘点人唯一标识", dataType = "long", required=true, example = "0"),
            @ApiImplicitParam(name="typeJoint", value = "盘点类型", dataType = "long", required=true, example = "0")
    })
    @PostMapping("/showTbBook")
    public ResponseResult showTbBook(@ApiIgnore @RequestBody TbCheckTask tbCheckTask){
        return this.iCheckTaskService.showTbook(tbCheckTask);
    }
}
