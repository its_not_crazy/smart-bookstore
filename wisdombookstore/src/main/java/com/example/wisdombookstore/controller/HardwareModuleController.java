package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbBrakeMachine;
import com.example.wisdombookstore.service.IHardwareModuleService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: MaHan
 * @Date: Created in 11:20 2020/10/22
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: HardwareModuleController
 */
@RestController
@Slf4j
@CrossOrigin
@RequestMapping("/hardwareModule")
@Api(tags = "接口-硬件管理")
public class HardwareModuleController {

    @Resource
    private IHardwareModuleService iHardwareModuleService;

    /**
     * TODO &马晗& 热力追踪
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:23 2020/10/22
     **/
    @ApiOperation(value = "热力追踪", notes = "根据热力追踪用户")
    @PostMapping("/hotPursuit")
    public ResponseResult hotPursuit(){
        return this.iHardwareModuleService.hotPursuit();
    }

    /**
     * TODO &马晗& 热力分析
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:34 2020/10/22
     **/
    @ApiOperation(value = "热力分析")
    @PostMapping("/theThermalAnalysis")
    public ResponseResult theThermalAnalysis(){
        return this.iHardwareModuleService.theThermalAnalysis();
    }
    /**
     * TODO &马晗& 报警功能
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:35 2020/10/22
     **/
    @ApiOperation(value = "报警功能")
    @PostMapping("/callThePolice")
    public ResponseResult callThePolice(){
        return  this.iHardwareModuleService.callThePolice();
    }
    /**
     * TODO &马晗& RFID扫描功能
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:35 2020/10/22
     **/
    @ApiOperation(value = "RFID扫描功能")
    @PostMapping("/rfidScanning")
    public ResponseResult rfidScanning(){
        return this.iHardwareModuleService.rfidScanning();
    }

    /**
     * TODO &马晗& 预警拍照功能
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:35 2020/10/22
     **/
    @ApiOperation(value = "预警拍照功能")
    @PostMapping("/photograph")
    public ResponseResult photograph(){
        return this.iHardwareModuleService.photograph();
    }

    /**
     * TODO &马晗& 闸机开关
     * @param tbBrakeMachine:
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 11:36 2020/10/22
     **/
    @ApiOperation(value = "闸机开关")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="code", value = "闸机标识", required=true)
    })
    @PostMapping("/brakeMachine")
    public ResponseResult brakeMachine(TbBrakeMachine tbBrakeMachine){
        return this.iHardwareModuleService.brakeMachine(tbBrakeMachine.getCode());
    }
}
