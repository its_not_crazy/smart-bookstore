package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.service.IReplenishmentResultService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: MaHan
 * @Date: Created in 16:13 2020/10/24
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: CheckResultController
 */
@RestController
@Slf4j
@RequestMapping("/ReplenishmentResult")
@Api(tags = "接口-补货结果")
@CrossOrigin
public class ReplenishmentResultController {
    @Autowired
    private IReplenishmentResultService iReplenishmentResultService;
    /**
     * TODO &马晗& 补货结果
     * @param currentPage:
     上* @param pageSize:
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description  补货任务达成，形成补货结果，查询所有补货结果
     * @Date 16:22 2020/10/24
     **/
    @ApiOperation(value = "补货结果", notes = "返回所有补货结果信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="currentPage", value = "当前页", dataType = "int", required=true, example = "0"),
            @ApiImplicitParam(name="pageSize", value = "每页条数", dataType = "int", required=true, example = "0"),
    })
    @PostMapping("/replenishmentResults")
    public ResponseResult replenishmentResults(Integer currentPage, Integer pageSize){
        return this.iReplenishmentResultService.replenishmentResults(currentPage, pageSize);
    }

    /**
     * TODO 8aceMak1r submitReplenishmentTask 提交补货任务
     * @param tbReplenishmentResultCode
     * @Description:
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/26 18:54
     */
    @ApiOperation(value = "提交补货任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbReplenishmentResultCode", value = "任务结果code", required = true)
    })
    @PostMapping("/submitReplenishmentTask")
    public ResponseResult submitReplenishmentTask(@RequestParam("tbReplenishmentResultCode") Long tbReplenishmentResultCode){
        return iReplenishmentResultService.submitReplenishmentTask(tbReplenishmentResultCode);
    }

    /**
     * TODO 8aceMak1r getReplenishmentResultCode 根据任务查询补货结果
     *
     * @param tbReplenishmentTaskCode
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description:
     * @author 8aceMak1r
     * @date 2020/10/26 18:54
     */
    @ApiOperation(value = "根据任务code结果")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbReplenishmentTaskCode", value = "任务code", required = true)
    })
    @PostMapping("getReplenishmentResultCode")
    public ResponseResult getReplenishmentResultCode(@RequestParam("tbReplenishmentTaskCode") Long tbReplenishmentTaskCode){
        return iReplenishmentResultService.getReplenishmentResultCode(tbReplenishmentTaskCode);
    }
}
