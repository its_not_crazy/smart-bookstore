package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbIntoRecord;
import com.example.wisdombookstore.service.IIntoRecordService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author 李慧龙
 * @date 2020/10/24 16:24
 * @className IntoRecordController
 * 用户进店的管理模块
 */
@RestController
@Slf4j
@CrossOrigin
@Api(tags = "接口-用户进店的管理模块")
public class IntoRecordController {
    @Resource
    IIntoRecordService iIntoRecordService;

    @PostMapping("/addIntoRecord")
    @ApiOperation(value = "添加用户进店记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userCode", value = "用户的code标识", required = true,dataType = "Long")
    })
    public ResponseResult addIntoRecord(@ApiIgnore TbIntoRecord tbIntoRecord){
        return iIntoRecordService.addIntoRecord(tbIntoRecord);
    }

    @PostMapping("/findAllIntoRecord")
    @ApiOperation(value = "查询所有用户进店记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currentPage", value = "当前页", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true,dataType = "Integer")
    })
    public ResponseResult findAllIntoRecord(Integer currentPage,Integer pageSize){
        return iIntoRecordService.findAllIntoRecord(currentPage,pageSize);
    }

    @PostMapping("/findIntoRecordByUserCode")
    @ApiOperation(value = "根据用户code查询该用户的所有记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userCode", value = "用户的code标识", required = true,dataType = "Long"),
            @ApiImplicitParam(name = "currentPage", value = "当前页", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true,dataType = "Integer")
    })
    public ResponseResult findIntoRecordByUserCode(@ApiIgnore TbIntoRecord tbIntoRecord,Integer currentPage,Integer pageSize){
        return iIntoRecordService.findIntoRecordByUserCode(tbIntoRecord,currentPage,pageSize);
    }

    @PostMapping("/findIntoRecordByDate")
    @ApiOperation(value = "根据日期查询所有该日期的用户进店记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "intoDate", value = "进店时间", required = true,dataType = "String"),
            @ApiImplicitParam(name = "currentPage", value = "当前页", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true,dataType = "Integer")
    })
    public ResponseResult findIntoRecordByDate(String intoDate, Integer currentPage, Integer pageSize){
        return iIntoRecordService.findIntoRecordByDate(intoDate,currentPage,pageSize);
    }
}
