package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbBookType;
import com.example.wisdombookstore.service.impl.BookTypeServiceImpl;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * @author 李慧龙
 * @date 2020/10/22 11:35
 * @className BookTypeController
 */
@RestController
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "接口-书籍类别管理模块")
public class BookTypeController {
    /**
     * 书籍类别的service层注入
     */
    @Resource
    BookTypeServiceImpl bookTypeService;

    /**
     * 添加图书类别信息
     *
     * @param :图书的信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO addTbBookType
     * @Date 11:17 2020/10/22
     */
    @PostMapping("/addTbBookType")
    @ApiOperation(value = "添加图书类别信息")
    @ApiImplicitParam(name = "typeName", value = "图书类别信息", required = true, dataType = "String")
    public ResponseResult addTbBookType(@ApiIgnore @RequestBody TbBookType tbBookType) {
        return this.bookTypeService.addTbBookType(tbBookType);
    }

    /**
     * 删除图书类别信息
     *
     * @param tbBookType: 图书的code值
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO deleteTbBookType
     * @Date 11:33 2020/10/22
     */
    @PostMapping("/deleteTbBookType")
    @ApiOperation(value = "删除图书类别信息")
    @ApiImplicitParam(name = "code", value = "图书类别的code值", required = true,dataType = "Long")
    public ResponseResult deleteTbBookType(@ApiIgnore @RequestBody TbBookType tbBookType) {
        return bookTypeService.deleteTbBookType(tbBookType);
    }

    /**
     * 修改图书类别信息
     *
     * @param tbBookType: 图书的信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO updateTbBookType
     * @Date 11:34 2020/10/22
     */
    @PostMapping("/updateTbBookType")
    @ApiOperation(value = "修改图书类别信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "图书类别code", required = true,dataType = "Long"),
            @ApiImplicitParam(name = "typeName", value = "图书类别信息", required = true,dataType = "String")
    })

    public ResponseResult updateTbBookType(@ApiIgnore @RequestBody TbBookType tbBookType) {
        return bookTypeService.updateTbBookType(tbBookType);
    }

    /**
     * 分页查询所有图书类别信息
     *
     * @param currentPage: 当前页
     * @param pageSize:    每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO findAllTbBookType
     * @Date 11:34 2020/10/22
     */
    @PostMapping("/findAllTbBookTypePage")
    @ApiOperation(value = "分页查询所有图书类别信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currentPage", value = "当前页数", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true,dataType = "Integer")
    })
    public ResponseResult findAllTbBookTypePage(@RequestParam("currentPage") Integer currentPage,
                                           @RequestParam("pageSize") Integer pageSize) {
        return bookTypeService.findAllTbBookType(currentPage, pageSize);
    }

    /*
    * TODO 陈阳  查询所有图书类别信息
     * @Description:
     * @Author: 陈阳
     * @Date: 2020/10/27 20:09
     * @param currentPage:
     * @param pageSize:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @PostMapping("/findAllTbBookType")
    @ApiOperation(value = "查询所有图书类别信息(所有的包括删除的)")
    public ResponseResult findAllTbBookType() {
        return bookTypeService.findAllTbBookType();
    }

    /**
     * 根据图书code值查询图书类别信息
     *
     * @param tbBookType:
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO findOneTbBookTypeByCode
     * @Date 11:35 2020/10/22
     */
    @PostMapping("/findOneTbBookTypeByCode")
    @ApiOperation(value = "根据code查询图书类别信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "图书的code值", required = true,dataType = "Long")
    })
    public ResponseResult findOneTbBookTypeByCode(@ApiIgnore @RequestBody TbBookType tbBookType) {
        return bookTypeService.findOneTbBookTypeByCode(tbBookType);
    }


    @PostMapping("/findAllTypes")
    @ApiOperation(value = "查询所有类别(添加或修改下拉框)")
    public ResponseResult findAll(){
        return bookTypeService.findAll();
    }
}
