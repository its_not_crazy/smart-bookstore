package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbReplenishmentTask;
import com.example.wisdombookstore.service.IReplenishmentTaskService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;

import io.swagger.annotations.ApiImplicitParam;

import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;

/**
 * @Author: MaHan
 * @Date: Created in 18:42 2020/10/25
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: ReplenishmentTaskController
 */
@RestController
@RequestMapping("/ReplenishmentTask")
@Slf4j
@CrossOrigin
@Api(tags = "接口-查询补货任务")
public class ReplenishmentTaskController {

    @Autowired
    private IReplenishmentTaskService replenishmentTaskService;


    /**
     * TODO &马晗& 负责人查询补货任务
     *
     * @param tbUserCode: 负责人标识
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 18:46 2020/10/25
     **/
    @ApiOperation(value = "负责人查询补货任务", notes = "返回该负责人的任务信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "tbUserCode", value = "负责人标识", required = true)
    })
    @PostMapping("/replenishmentUserFindCode")
    public ResponseResult replenishmentUserFindCode(Long tbUserCode) {
        return this.replenishmentTaskService.replenishmentUserFindCode(tbUserCode);
    }

    /**
     * TODO &马晗& 负责人查询自己的补货任务
     *
     * @param tbUserCode: 负责人标识
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description
     * @Date 18:46 2020/10/25
     **/
    @ApiOperation(value = "负责人查询补货任务", notes = "返回该负责人的任务信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "tbUserCode", value = "负责人标识", required = true)
    })
    @PostMapping("/replenishmentUserFindCodeList")
    public ResponseResult replenishmentUserFindCodeList(Long tbUserCode) {
        return this.replenishmentTaskService.replenishmentUserFindCodeList(tbUserCode);
    }

    /**
     * TODO &马晗&  查询所有补货任务
     *
     * @param currentPage: 当前页
     * @param pageSize:    每页条数
     * @return: com.example.wisdombookstore.util.ResponseResult
     * @Author &马晗&
     * @Description 查询所有补货任务
     * @Date 20:01 2020/10/25
     **/
    @ApiOperation(value = "所有补货任务", notes = "返回补货任务所有信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "currentPage", value = "当前页", dataType = "int", required = true, example = "0"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", dataType = "int", required = true, example = "0"),
    })
    @PostMapping("/replenishmentUserFind")
    public ResponseResult replenishmentUserFind(Integer currentPage, Integer pageSize) {
        return this.replenishmentTaskService.replenishmentUserFind(currentPage, pageSize);
    }

    /**
     * TODO 8aceMak1r creteReplenishmentTaskCode 创建补货编号
     * @Description:
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/25 10:43
     */
    @ApiOperation(value = "创建补货编号")
    @ApiImplicitParams({
    })
    @PostMapping("creteReplenishmentTaskCode")
    public ResponseResult creteReplenishmentTaskCode(){
        return replenishmentTaskService.creteReplenishmentTaskCode();
    }



    /**
     * TODO 8aceMak1r createReplenishment 创建补货任务
     *
     * @param replenishmentTask 补货任务
     * @param tbUserId          负责补货的工作人员
     * @param tbReplenishmentCode 补货code
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 店长创建补货任务
     * @author 8aceMak1r
     * @date 2020/10/22 15:57
     */
    @ApiOperation(value = "创建补货任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbUserId", value = "负责人", required = true),
            @ApiImplicitParam(name = "startCheckDate", value = "开始时间", required = true),
            @ApiImplicitParam(name = "endCheckDate", value = "结束时间", required = true),
            @ApiImplicitParam(name = "targetNumber", value = "目标数量", required = true),
            @ApiImplicitParam(name = "tbBookInfoCode", value = "书籍标识", required = true),
            @ApiImplicitParam(name = "tbBookTypeCode", value = "书籍类型", required = true)
    })
    @PostMapping("/createReplenishment/{code}")
    public ResponseResult createReplenishment(@ApiIgnore @RequestBody TbReplenishmentTask replenishmentTask,
                                              @RequestParam("tbUserId") Long tbUserId,
                                              @PathVariable("code") Long tbReplenishmentCode) {
        return replenishmentTaskService.createReplenishment(replenishmentTask, tbUserId,tbReplenishmentCode);
    }


    /**
     * TODO 8aceMak1r executeReplenishmentTask 执行补货任务
     *
     * @param tbUserId          工作人员
     * @param tbReplenishmentResultCode 补货结果
     * @param rfidCode 书品的rfid
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @Description: 工作人员执行任务
     * @author 8aceMak1r
     * @date 2020/10/22 16:20
     */
    @ApiOperation(value = "执行补货任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbUserId", value = "负责人", required = true),
            @ApiImplicitParam(name = "tbReplenishmentResultCode", value = "任务结果code", required = true),
            @ApiImplicitParam(name = "rfidCode", value = "书的rfid", required = true)
    })
    @PostMapping("/executeReplenishmentTask")
    public ResponseResult executeReplenishmentTask(@RequestParam("tbUserId") Long tbUserId,
                                                   @RequestParam("tbReplenishmentResultCode") Long tbReplenishmentResultCode,
                                                   @RequestParam("rfidCode") String rfidCode) {
        return replenishmentTaskService.executeReplenishmentTask(tbUserId, tbReplenishmentResultCode, rfidCode);
    }


    /**
     * TODO 8aceMak1r delReplenishmentTask 取消补货任务
     * @param tbReplenishmentResultCode 补货任务code
     * @Description: 取消补货任务
     * @return com.example.wisdombookstore.util.ResponseResult
     * @throws
     * @author 8aceMak1r
     * @date 2020/11/1 19:52
     */
    @ApiOperation(value = "取消补货任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tbReplenishmentResultCode", value = "任务code", required = true),
    })
    @PostMapping("/delReplenishmentTask")
    public ResponseResult delReplenishmentTask(@RequestParam("tbReplenishmentResultCode") Long tbReplenishmentResultCode){
        return replenishmentTaskService.delReplenishmentTask(tbReplenishmentResultCode);
    }


}
