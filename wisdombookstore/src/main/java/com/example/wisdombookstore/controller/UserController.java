package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbUser;
import com.example.wisdombookstore.service.IUserService;
import com.example.wisdombookstore.util.ResponseResult;
import com.example.wisdombookstore.util.face.GsonUtils;
import com.example.wisdombookstore.util.face.Service.LoginService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

/**
 * @program: wisdombookstore
 *
 * @description: 用户控制类
 *
 * @author: 宋合
 *
 * @create: 2020-10-22 10:59
 **/
@RestController("user")
@CrossOrigin
@SessionAttributes(value = "useinf")
@Api(tags = "接口-用户管理")
public class UserController {

    @Autowired
    LoginService loginService=null;

    @Autowired
    private IUserService iUserService;


    /**
     * TODO &宋合& 手机注册
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:41
     * @param phone:  手机号
     * @param verificationCode: 验证码
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "手机号注册",notes = "根据手机号注册")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="phone", value = "手机号", dataType = "int", required=true, example = "0"),
            @ApiImplicitParam(name="verificationCode", value = "验证码", dataType = "int", required=true, example = "0")
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("telRegister")
    public ResponseResult telRegister(String phone,String verificationCode){

        ResponseResult responseResult = iUserService.telRegister(phone, verificationCode);
        return responseResult;
    }
    /**
     * TODO &宋合& 发送验证码
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:41
     * @param phone:  手机号
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "发送手机验证码",notes = "发送手机验证码")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="phone", value = "手机号", dataType = "String", required=true, example = "0"),
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("getVerificationCode")
    public ResponseResult getVerificationCode(@RequestParam("phone") String phone){
        System.out.println("发送验证码");
        ResponseResult responseResult = iUserService.sendMobileVerificationCode(phone);
        return responseResult;
    }
    /**
     * TODO &宋合& 微信注册
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:44
     * @param wechatId: 手机号
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "微信注册",notes = "根据微信号注册")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="wechatId", value = "微信号", dataType = "String", required=true, example = "0"),
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("wechatRegister")
    public ResponseResult wechatRegister(String wechatId){

        ResponseResult responseResult = iUserService.wechatRegister(wechatId);
        return responseResult;
    }

    /**
     * TODO &宋合& 手机号登录
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:47
     * @param user:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "手机号登录",notes = "根据手机号和密码登录")
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("telLogin")
    public ResponseResult telLogin(@RequestBody TbUser user){

        ResponseResult responseResult = iUserService.telLogin(user);
        return responseResult;
    }

    /**
     * TODO &宋合& 微信登录
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:47
     * @param wechatId:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "微信登录",notes = "微信登录")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="wechatId", value = "微信标识", dataType = "String", required=true, example = "0"),
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("wechatLogin")
    public ResponseResult wechatLogin(String wechatId){

        ResponseResult responseResult = iUserService.wechatLogin(wechatId);
        return responseResult;
    }

    /**
     * TODO &宋合& 生成二维码
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:48
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="phone", value = "手机号", dataType = "String", required=true, example = "0"),
    })
    @ApiOperation(value = "生成二维码",notes = "动态生成二维码")
    @PostMapping("createQrcode")
    public ResponseResult createQrcode(String phone){
        System.out.println("生成二维码-------------------------------");
        ResponseResult qrcode = iUserService.createQrcode(phone);
        return qrcode;
    }

    /**
     * TODO &宋合& 人脸识别功能
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:51
     * @param faceUrl:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "人脸识别功能",notes = "根据人脸识别")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="faceUrl", value = "人脸路径", dataType = "String", required=true, example = "0"),
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    //@PostMapping("faceRecognition")
    public ResponseResult faceRecognition(String faceUrl){

        ResponseResult responseResult = iUserService.faceRecognition(faceUrl);

        return responseResult;
    }
    @ApiOperation(value = "回显头像",notes = "回显头像")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="image", value = "图片路径", dataType = "String", required=true, example = "0"),
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @GetMapping("download")
    public ResponseResult download(@RequestParam("image") String image, HttpServletResponse response){

        ResponseResult responseResult = iUserService.download(image,response);

        return responseResult;
    }
    /**
     * TODO &宋合&  支付
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 10:57
     * @param tbOrderCode:
     * @param money:
     * @param payCode:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "支付",notes = "支付订单金额")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="tbOrderCode", value = "订单标识", dataType = "Long", required=true, example = "0"),
            @ApiImplicitParam(name="money", value = "支付金额", dataType = "double", required=true, example = "0"),
            @ApiImplicitParam(name="payCode", value = "支付标识", dataType = "String", required=true, example = "0")
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    //@PostMapping("payBooks")
    public ResponseResult payBooks(Long tbOrderCode,double money,String payCode){
        ResponseResult responseResult = new ResponseResult();

        return responseResult;
    }

    /**
     * TODO 宋合 getUserList 查看所有会员
     * @param pageSize 总页数
     * @param pageCurrent 当前页
     * @Description: 查看所有会员
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 宋合
     * @date 2020/10/22 11:51
     */
    @ApiOperation(value = "查看所有会员",notes = "查看所有会员")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="pageCurrent", value = "页数", dataType = "int", required=true, example = "0"),
            @ApiImplicitParam(name="pageSize", value = "一页多少条", dataType = "int", required=true, example = "0"),

    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("getUserList")
    public ResponseResult getUserList(Integer pageSize,Integer pageCurrent){

        ResponseResult responseResult = iUserService.getUserList(pageSize, pageCurrent);
        return responseResult;
    }
    /**
     * TODO 宋合 getUserByUserId 完善会员信息
     * @param user 会员信息
     * @Description: 完善会员信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 宋合
     * @date 2020/10/22 11:20
     */
    @ApiOperation(value = "完善会员信息",notes = "完善会员信息")
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("addUserById")
    public ResponseResult addUserById(@RequestBody TbUser user){

        ResponseResult responseResult = iUserService.addUserById(user);
        return responseResult;
    }
    /**
     * TODO &宋合& 上传到本地
     * @Description:
     * @Author: 宋合
     * @Date: 2020/11/2 11:15
     * @param file: 
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "上传图片",notes = "上传图片")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="file", value = "图片文件", dataType = "String", required=true, example = "0"),
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("upload")
    public ResponseResult upload(MultipartFile file){

        ResponseResult responseResult =iUserService.upload(file);
        return responseResult;
    }
    @ApiOperation(value = "oos上传图片",notes = "上传图片")
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("ossUpload")
    public ResponseResult ossUpload(@RequestParam("file") MultipartFile file){

        ResponseResult responseResult =iUserService.ossUpload(file);
        return responseResult;
    }
    /**
     * TODO 宋合 getUserByUserId 查看会员详情
     * @param tbUserCode 会员code
     * @Description: 查看会员详情信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 宋合
     * @date 2020/10/22 11:20
     */
    @ApiOperation(value = "查看会员详情",notes = "查看会员详情")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="tbUserCode", value = "会员标识", dataType = "Long", required=true, example = "0")
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("getUserById")
    public ResponseResult getUserById(Long tbUserCode){


        return iUserService.getUserById(tbUserCode);
    }

    /**
     * TODO 宋合 setClerkById 修改会员信息
     * @param user 会员
     * @Description: 修改店员信息
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 宋合
     * @date 2020/10/22 11:22
     */
    @ApiOperation(value = "修改会员信息",notes = "修改会员信息")
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("setUserById")
    public ResponseResult setUserById(@RequestBody TbUser user){

        return iUserService.setUserById(user);
    }

    /**
     * TODO 宋合 delUserById 停用会员
     * @param tbUserCode 店员
     * @Description: 停用会员
     * @return com.example.windstorm.util.ResponseResult
     * @throws
     * @author 宋合
     * @date 2020/10/22 11:28
     */
    @ApiOperation(value = "停用会员",notes = "停用会员")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="tbUserCode", value = "会员标识", dataType = "Long", required=true, example = "0"),
            })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("delUserById")
    public ResponseResult delUserById(Long tbUserCode){


        return iUserService.delUserById(tbUserCode);
    }

    /**
     * TODO 8aceMak1r faceAuthentication 人脸认证
     * @Description: 用户将自己的账号进行人脸认证
     * @return com.example.windstorm.util.ResponseResult
     * @param imageBast64 用户人脸信息
     * @param model
     * @param request
     * @param userCode 用户code
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/28 8:54
     */
    @ApiOperation(value = "人脸认证",notes = "人脸认证")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="userCode", value = "用户code", dataType = "Long", required=true, example = "0"),
            @ApiImplicitParam(name="imagebast64", value = "用户人脸信息", dataType = "String", required=true, example = "0")
    })
    @RequestMapping("faceAuthentication")
    @ResponseBody
    public String faceAuthentication(@RequestBody @RequestParam(name = "imagebast64") StringBuffer imageBast64,
                                             Model model,
                                             HttpServletRequest request,
                                             @RequestParam("userCode") Long userCode)throws IOException {
        return iUserService.faceAuthentication(imageBast64,model,request,userCode);
    }


    /**
     * TODO 8aceMak1r searchFace 人脸登录
     * @param imageBast64  照片信息
     * @param model
     * @param request
     * @Description:
     * @return java.lang.String
     * @throws
     * @author 8aceMak1r
     * @date 2020/10/28 20:47
     */
    @ApiOperation(value = "人脸认证",notes = "人脸认证")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="userCode", value = "用户code", dataType = "Long", required=true, example = "0"),
            @ApiImplicitParam(name="imagebast64", value = "用户人脸信息", dataType = "String", required=true, example = "0")
    })
    @RequestMapping("/searchFace")
    @ResponseBody
    public   String searchFace(@RequestBody @RequestParam(name = "imagebast64") StringBuffer imageBast64,
                               Model model,
                               HttpServletRequest request) throws IOException {
       return iUserService.searchFace(imageBast64,model,request);

    }


}
