package com.example.wisdombookstore.controller;/**
 * @program: wisdombookstore
 * @description: 订单控制类
 * @author: 宋合
 * @create: 2020-10-22 11:23
 **/


import com.example.wisdombookstore.service.IOrderService;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @program: wisdombookstore
 *
 * @description: 订单控制类
 *
 * @author: 宋合
 *
 * @create: 2020-10-22 11:23
 **/
@RestController
@RequestMapping("/order")
@CrossOrigin
@Api(tags = "接口-订单管理")
public class OrderController {

    @Autowired
    private IOrderService iOrderService;


    /**
     * TODO &宋合& 取消订单功能
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 11:19
     * @param tbOrderCode:
     * @return: com.example.wisdombookstore.util.ResponseResult
     **/
    @ApiOperation(value = "取消订单功能",notes = "根据code取消")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="tbOrderCode", value = "订单code", dataType = "Long", required=true, example = "0")
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("/delOrder")
    public ResponseResult cancelBookOrder(Long tbOrderCode){

        ResponseResult responseResult = iOrderService.cancelBookOrder(tbOrderCode);
        return responseResult;
    }

    /**
     * TODO &宋合& 生成订单
     * @Description:
     * @Author: 宋合
     * @Date: 2020/10/22 11:20
     * @param tbShoppingCartCode: 购物车标识
     * @return: java.lang.Object
     **/
    @ApiOperation(value = "生成订单",notes = "根据购物车生成订单")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="tbShoppingCartCode", value = "购物车code", dataType = "Long", required=true, example = "0")
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("/addOrder")
    public ResponseResult createBookOrder(Long tbShoppingCartCode){

        return iOrderService.createBookOrder(tbShoppingCartCode);
    }

    @ApiOperation(value = "订单列表",notes = "查询用户所有订单")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="tbUserCode", value = "用户code", dataType = "Long", required=true, example = "0")
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("/bookOrderList")
    public ResponseResult bookOrderList(Long tbUserCode){

        return iOrderService.bookOrderList(tbUserCode);
    }

    @ApiOperation(value = "查询订单",notes = "查询单个订单")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="tbOrderCode", value = "订单code", dataType = "Long", required=true, example = "0")
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对") })
    @PostMapping("/bookOrder")
    public ResponseResult bookOrder(Long tbOrderCode){

        return iOrderService.bookOrder(tbOrderCode);
    }

/**
 * TODO &宋合& getBookOrderList 查询所有订单
 * @Description:
 * @Author: 宋合
 * @Date: 2020/11/1 15:55
 * @param status:
 * @param pageCurrent:
 * @param pageSize:
 * @return: com.example.wisdombookstore.util.ResponseResult
 **/
    @ApiOperation(value = "查询所有订单",notes = "查询所有订单")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="status", value = "订单状态", dataType = "int", required=false),
            @ApiImplicitParam(name="pageCurrent", value = "当前页数", dataType = "int", required=true),
            @ApiImplicitParam(name="pageSize", value = "全部页数", dataType = "int", required=true)
    })
    @ApiResponses({
            @ApiResponse(code=400,message="请求参数没填好"),
            @ApiResponse(code=404,message="请求路径没有或页面跳                                                                                                                                                                      转路径不对") })
    @PostMapping("/getBookOrderList")
    public ResponseResult getBookOrderList(@RequestParam("status") Integer status,
                                           @RequestParam("pageCurrent")Integer pageCurrent,
                                           @RequestParam("pageSize")Integer pageSize){
        return iOrderService.getBookOrderList(status,pageCurrent,pageSize);
    }
}
