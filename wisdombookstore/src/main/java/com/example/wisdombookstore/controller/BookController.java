package com.example.wisdombookstore.controller;

import com.example.wisdombookstore.entity.TbBook;
import com.example.wisdombookstore.service.impl.BookServiceImpl;
import com.example.wisdombookstore.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * @author 李慧龙
 * @date 2020/10/22 10:26
 * @className BookController
 */
@RestController
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "接口-图书的管理模块")
public class BookController {
    /**
     * 图书service层的注入
     *
     */
    @Resource
    BookServiceImpl bookService;

    /**
     * 添加图书信息
     *
     * @param :图书的信息
     * @return com.example.wisdombookstore.util.ResponseResult
     * @Author 李慧龙
     * @Description //TODO addTbBook
     * @Date 11:17 2020/10/22
     */
    @PostMapping("/addTbBook")
    @ApiOperation(value = "添加图书信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rfIdCode", value = "Rfid标识", required = true,dataType = "String"),
            @ApiImplicitParam(name = "price", value = "图书当前售卖价格", required = true,dataType = "Double"),
            @ApiImplicitParam(name = "tbBookInfoCode", value = "书籍的code", required = true,dataType = "Long")
    })
    public ResponseResult addTbBook(@ApiIgnore @RequestBody TbBook tbBook) {
        return this.bookService.addTbBook(tbBook);
    }

    /**
     * 删除图书信息
     * @Author 李慧龙
     * @Description //TODO deleteTbBook
     * @Date 11:33 2020/10/22
     * @param : 图书的code值
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    @PostMapping("/deleteTbBook")
    @ApiOperation(value = "删除图书信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "图书的code值", required = true,dataType = "Long")
    })
    public ResponseResult deleteTbBook(@ApiIgnore @RequestBody TbBook tbBook) {
        return bookService.deleteTbBook(tbBook);
    }

    /**
     * 修改图书信息
     * @Author 李慧龙
     * @Description //TODO updateTbBook
     * @Date 11:34 2020/10/22
     * @param : 图书的信息
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    @PostMapping("/updateTbBook")
    @ApiOperation(value = "修改图书信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "图书的code", required = true,dataType = "Long"),
            @ApiImplicitParam(name = "rfIdCode", value = "Rfid标识", required = true,dataType = "String"),
            @ApiImplicitParam(name = "title", value = "图书名称", required = true,dataType = "String"),
            @ApiImplicitParam(name = "price", value = "图书价格", required = true,dataType = "Double"),
            @ApiImplicitParam(name = "tbBookInfoCode", value = "书籍的标识", required = true,dataType = "Long")
    })
    public ResponseResult updateTbBook(@ApiIgnore @RequestBody TbBook tbBook) {
        return bookService.updateTbBook(tbBook);
    }

    /**
     * 查询所有图书信息
     * @Author 李慧龙
     * @Description //TODO findAllTbBook
     * @Date 11:34 2020/10/22
     * @param currentPage: 当前页
     * @param pageSize: 每页条数
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    @PostMapping("/findAllTbBook")
    @ApiOperation(value = "查询所有图书信息(带分页效果)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currentPage", value = "当前页数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, dataType = "Integer"),
    })
    public ResponseResult findAllTbBook(@RequestParam("currentPage") Integer currentPage,
                                       @RequestParam("pageSize") Integer pageSize) {
        return bookService.findAllTbBook(currentPage, pageSize);
    }

    /**
     * 根据图书code值查询图书信息
     * @Author 李慧龙
     *
     * @Description //TODO findOneTbBookByCode
     * @Date 11:35 2020/10/22
     * @param :
     * @return com.example.wisdombookstore.util.ResponseResult
     */
    @PostMapping("/findOneTbBookByCode")
    @ApiOperation(value = "根据code查询图书信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "图书的code值", required = true,dataType = "Long")
    })
    public ResponseResult findOneTbBookByCode(@ApiIgnore @RequestBody TbBook tbBook) {
        return bookService.findOneTbBookByCode(tbBook);
    }

    @PostMapping("/findAllBook")
    @ApiOperation(value = "查询所有图书")
    public ResponseResult findAllBook() {
        return bookService.findAllBook();
    }
}
