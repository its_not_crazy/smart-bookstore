package com.example.wisdombookstore.util;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 类型状态常量
 * @date 2020/10/21 9:48
 */
public class StateConstant {

    /**
     * 返回类状态常量-成功
     */
    public static final Integer RESPONSERESULT_CODE_TRUE = 1000;

    /**
     * 返回类状态常量 -失败
     */
    public static final Integer RESPONSERESULT_CODE_FALSE = -1;

    /**
     *  正常
     */
    public static final Integer OBJECT_DEL_NORMAL = 0;
    /**
     * 删除
     */
    public static final Integer OBJECT_DEL_OMIT = 1;

    /**
     * 图书状态爱 0:未售卖
     */
    public static final Integer TBBOOK_STSTUS_UNTREATED=0;

    /**
     * 图书状态 1:以售卖
     */
    public static final Integer TBBOOK_STSTUS_PROCESSED=1;
    /**
     * 书籍状态 0：待上架
     */
    public static final Integer TBBOOKINFORMATION_START_STAY = 0;

    /**
     * 书籍状态 1：已上架
     */
    public static final Integer TBBOOKINFORMATION_START_ALREADY = 1;

    /**
     * 书籍状态2：已售完
     */
    public static final Integer TBBOOKINFORMATION_START_COMPLETE = 2;

    /**
     * 书籍状态 3已下架
     */
    public static final Integer TBBOOKINFORMATION_START_OFFSHELF = 3;

    /**
     * 闸机状态 0开启
     */
    public static final Integer TBBRAKEMACHINE_STATUS_OPEN = 0;

    /**
     * 闸机状态1关闭
     */
    public static final Integer TBBRAKEMACHINE_STATUS_SHUT = 1;

    /**
     * 盘点任务 状态 0 待完成
     */
    public static final Integer TBCHECKTASK_STATUS_STAY = 0;

    /**
     * 盘点任务 状态 1 开始盘点
     */
    public static final Integer TBCHECKTASK_STATUS_START = 1;

    /**
     * 盘点任务 状态 2 盘点完成
     */
    public static final Integer TBCHECKTASK_STATUS_COMPLETE = 2;

    /**
     * 盘点任务 状态 3 未盘点（在规定时间内未进行盘点）
     */
    public static final Integer TBCHECKTASK_STATUS_NOT = 3;


    /**
     * 订单状态 0 待完成
     */
    public static final Integer TBCHILDORDERS_STATUS_STAY = 0;

    /**
     * 订单 状态 1 以完成
     */
    public static final Integer TBCHILDORDERS_STATUS_ALREADY = 1;


    /**
     * 工作人员权限   0  店长
     */
    public static final Integer TBCLERK_PERMISSIONS_STOREMANAGER = 0;

    /**
     * 工作人员权限   1  工作人员
     */
    public static final Integer TBCLERK_PERMISSIONS_STAFFMEMBER = 1;
    /**
     * 工作人员状态  0 在职
     */
    public static final Integer TBCLERK_STATUS_WORK = 0;
    /**
     * 工作人员状态  1 离职
     */
    public static final Integer TBCLERK_STATUS_RESIGN= 1;

    /**
     * 订单支付状态   1  成功
     */
    public static final Integer TBCLERK_PERMISSIONS_SUCCESS = 1;

    /**
     * 订单支付状态   2  失败
     */
    public static final Integer TBCLERK_PERMISSIONS_FAIL = 2;

    /**
     *补货状态   0  待完成
     */
    public static final Integer TBREPLENISHMENTRECORD_STATUS_STAY  = 0;

    /**
     * 补货状态   1  已完成
     */
    public static final Integer TBREPLENISHMENTRECORD_STATUS_ALREADY = 1;

    /**
     *用户状态   1  正常
     */
    public static final Integer TBUSER_STATE_NORMAL  = 0;

    /**
     * 用户状态  2  锁定
     */
    public static final Integer TBUSER_STATE_LOCKING = 1;

    /**
     * 书店状态  0 营业
     */
    public static  final Integer TBBOOKSTORE_STATUS_OPERATE=0;

    /**
     * 书店状态 1 未营业
     */
    public static  final Integer TBBOOKSTORE_STATUS_CLOSED=1;

    /**
     * 代表没有
     */
    public static final Integer HAVEHOT=0;
}
