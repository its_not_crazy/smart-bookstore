package com.example.wisdombookstore.util.face.Service;


import com.example.wisdombookstore.util.face.BaiduAIFace;
import com.example.wisdombookstore.util.face.SetingModel.Setingmodel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

/**
 * 人脸登录
 */
@Service
public class LoginService {

    @Autowired
    BaiduAIFace faceapi;

    @Autowired
    Setingmodel setingModel;

    public Map<String,Object> searchface(StringBuffer imageBase64) throws IOException {
        String substring = imageBase64.substring(imageBase64.indexOf(",")+1, imageBase64.length());
        setingModel.setImgpath(substring);
        setingModel.setGroupID("StRoot");
        System.out.println(substring);
        Map map = faceapi.FaceSearch(setingModel);
        return map;
    }
}
