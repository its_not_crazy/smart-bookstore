package com.example.wisdombookstore.util;

import org.apache.http.HttpResponse;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;
/**
 * @Description:
 * @Author: 宋合
 * @Date: 2020/10/23 14:50
 **/
public class QrCodeTest {

	public static void main(String[] args) throws Exception {

		// 存放在二维码中的内容
		String text = "我是小铭";
		// 嵌入二维码的图片路径
		String imgPath = "D:/fileDB/book.jfif";
		// 生成的二维码的路径及名称
		String destPath = "D:/fileDB/jam.jpg";
		//生成二维码
		QRCodeUtil.encode(text, imgPath, destPath, true);
		// 解析二维码
		String str = QRCodeUtil.decode(destPath);
		// 打印出解析出的内容
		System.out.println(str);

	}

	public void face(String base){
		String host = "https://zfa.market.alicloudapi.com";
		String path = "/efficient/idfaceIdentity";
		String method = "POST";
		String appcode = "2bf8cde064e64b4e90585d8deee4f7f1";
		Map<String, String> headers = new HashMap<String, String>();
		//最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
		headers.put("Authorization", "APPCODE " + appcode);
		//根据API的要求，定义相对应的Content-Type
		headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		Map<String, String> querys = new HashMap<String, String>();
		Map<String, String> bodys = new HashMap<String, String>();
		bodys.put("base64Str", "urlencode("+base+")");
		bodys.put("liveChk", "0");
		bodys.put("name", "宋合");
		bodys.put("number", "130726199810050010");


		try {
			/**
			 * 重要提示如下:
			 * HttpUtils请从
			 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
			 * 下载
			 *
			 * 相应的依赖请参照
			 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
			 */
			HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
			System.out.println(response.toString());
			//获取response的body
			//System.out.println(EntityUtils.toString(response.getEntity()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
 
