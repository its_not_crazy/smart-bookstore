package com.example.wisdombookstore.util;

import org.apache.http.HttpResponse;

import java.util.HashMap;
import java.util.Map;
/**
 * @Description:
 * @Author: 宋合
 * @Date: 2020/10/23 14:51
 **/
public class SmsUtil {


    public static void sendSmsBySanWu(String mobile, String code) {
        // AppKey：203849082     AppSecret：o08VrbeqDLivZJ3YWdBxfqah65dtopWZ 复制
        // AppCode：559b058711ff480ebdbd5bc751d30bf4

        String host = "http://dingxin.market.alicloudapi.com";
        String path = "/dx/sendSms";
        String method = "POST";
        String appcode = "2bf8cde064e64b4e90585d8deee4f7f1";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("mobile", mobile);
        querys.put("param", "code:"+code);
        querys.put("tpl_id", "TP1711063");
        Map<String, String> bodys = new HashMap<String, String>();


        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtilsNote.doPost(host, path, method, headers, querys, bodys);
            System.out.println(response.toString());
            //获取response的body
            //System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
