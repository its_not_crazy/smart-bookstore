package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbOutRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author 李慧龙
 * @date 2020/10/24 16:21
 * @className TbOutRecordMapper
 */
@Mapper
@Component("tbOutRecordMapper")
public interface TbOutRecordMapper extends BaseMapper<TbOutRecord> {
}
