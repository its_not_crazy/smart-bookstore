package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbCheckDeficiency;
import org.mapstruct.Mapper;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 盘点任务中未盘点的书籍
 * @date 2020/10/25 22:41
 */
@Mapper
public interface TbCheckDeficiencyMapper extends BaseMapper<TbCheckDeficiency> {
}
