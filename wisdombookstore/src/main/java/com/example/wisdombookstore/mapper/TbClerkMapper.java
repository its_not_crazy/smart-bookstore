package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.wisdombookstore.entity.TbClerk;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

/**
 * @ClassName ClerkServiceMapper
 * @Description TODO 8aceMak1r ClerkServiceMapper
 * @author 8aceMak1r
 * @date 2020/10/22 14:49
 * @Version 1.0
 */
public interface TbClerkMapper extends BaseMapper<TbClerk> {


}
