package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.RlCheckBookType;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 盘点任务-书籍类型中间表
 * @date 2020/10/24 16:42
 */
@Mapper
public interface RlCheckBookTypeMapper extends BaseMapper<RlCheckBookType> {
}
