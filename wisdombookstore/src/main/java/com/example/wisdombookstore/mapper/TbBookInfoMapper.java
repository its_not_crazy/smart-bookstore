package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbBookInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author 李慧龙
 * @date 2020/10/22 14:23
 * @className TbBookInfoMapper
 */
@Mapper
@Component("tbBookInfoMapper")
public interface TbBookInfoMapper extends BaseMapper<TbBookInfo> {
}
