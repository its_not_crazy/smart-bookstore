package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbCollect;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 收藏Mapper
 * @date 2020/10/23 14:44
 */
@Mapper
public interface TbCollectMapper extends BaseMapper<TbCollect> {
}
