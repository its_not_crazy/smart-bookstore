package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbReplenishmentParticular;

/**
 * @ClassName TbReplenishmentParticularMapper 补货任务详情
 * @Description TODO 8aceMak1r TbReplenishmentParticularMapper
 * @author 8aceMak1r
 * @date 2020/10/22 14:49
 * @Version 1.0
 */
public interface TbReplenishmentParticularMapper extends BaseMapper<TbReplenishmentParticular> {
}
