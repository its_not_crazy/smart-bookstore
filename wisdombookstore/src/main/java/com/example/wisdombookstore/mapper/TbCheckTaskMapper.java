package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbCheckTask;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 盘点任务表maper
 * @date 2020/10/23 16:45
 */
@Mapper
public interface TbCheckTaskMapper extends BaseMapper<TbCheckTask> {
}
