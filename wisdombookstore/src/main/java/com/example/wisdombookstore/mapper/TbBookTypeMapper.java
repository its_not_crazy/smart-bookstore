package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbBookType;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description:   书籍类型
 * @date 2020/10/23 8:44
 */
@Mapper
public interface TbBookTypeMapper extends BaseMapper<TbBookType> {
}
