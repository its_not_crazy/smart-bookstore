package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbBookImage;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李慧龙
 * @date 2020/10/23 9:23
 * @className TbBookImageMapper
 */
@Mapper
public interface TbBookImageMapper extends BaseMapper<TbBookImage> {
}
