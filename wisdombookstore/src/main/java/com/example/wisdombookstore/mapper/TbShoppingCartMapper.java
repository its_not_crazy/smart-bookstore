package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbShoppingCart;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 购物车mapper
 * @date 2020/10/22 10:30
 */
@Mapper
public interface TbShoppingCartMapper extends BaseMapper<TbShoppingCart> {
}
