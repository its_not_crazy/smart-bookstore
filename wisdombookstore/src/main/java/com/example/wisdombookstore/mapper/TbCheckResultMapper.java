package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbCheckResult;

/**
 * @Author: MaHan
 * @Date: Created in 15:52 2020/10/24
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: TbCheckResultMapper
 */
public interface TbCheckResultMapper extends BaseMapper<TbCheckResult> {
}
