package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbOrdersDetail;
import org.springframework.stereotype.Repository;

/**
 * @program: wisdombookstore
 * @description: 订单详情
 * @author: 宋合
 * @create: 2020-10-27 08:21
 **/
@Repository
public interface TbOrdersDetailMapper extends BaseMapper<TbOrdersDetail> {
}
