package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbBookStore;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Author: MaHan
 * @Date: Created in 11:05 2020/10/22
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: TbBookStoreMapper
 */
@Mapper
public interface TbBookStoreMapper extends BaseMapper<TbBookStore> {
}
