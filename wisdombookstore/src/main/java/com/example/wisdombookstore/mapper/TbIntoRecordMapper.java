package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbIntoRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author 李慧龙
 * @date 2020/10/24 16:20
 * @className TbIntoRecordMapper
 * 进店记录mapper
 */
@Mapper
@Component("tbIntoRecordMapper")
public interface TbIntoRecordMapper extends BaseMapper<TbIntoRecord> {
}
