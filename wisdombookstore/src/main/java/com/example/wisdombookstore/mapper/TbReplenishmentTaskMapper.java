package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbReplenishmentTask;
/**
 * @ClassName TbReplenishmentTaskMapper
 * @Description TODO 8aceMak1r TbReplenishmentTaskMapper
 * @author 8aceMak1r
 * @date 2020/10/22 14:49
 * @Version 1.0
 */
public interface TbReplenishmentTaskMapper extends BaseMapper<TbReplenishmentTask> {
}
