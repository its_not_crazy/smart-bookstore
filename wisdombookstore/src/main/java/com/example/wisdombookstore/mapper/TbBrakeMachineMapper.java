package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbBrakeMachine;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @Author: MaHan
 * @Date: Created in 11:42 2020/10/22
 * @Description:
 * @program: IntelliJ IDEA
 * @ClassName: TbBrakeMachineMapper
 */
@Mapper
@Component("tbBrakeMachineMapper")
public interface TbBrakeMachineMapper extends BaseMapper<TbBrakeMachine> {
}
