package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbReplenishmentResult;
/**
 * @ClassName TbReplenishmentResultMapper 补货任务结果
 * @Description TODO 8aceMak1r TbReplenishmentResultMapper
 * @author 8aceMak1r
 * @date 2020/10/22 14:49
 * @Version 1.0
 */
public interface TbReplenishmentResultMapper extends BaseMapper<TbReplenishmentResult> {
}
