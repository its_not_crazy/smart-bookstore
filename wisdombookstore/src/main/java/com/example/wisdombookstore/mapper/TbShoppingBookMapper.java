package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbShoppingBook;
import com.example.wisdombookstore.entity.TbShoppingCart;
import org.springframework.stereotype.Repository;

/**
 * @program: wisdombookstore
 * @description: 购物车图书单品
 * @author: 宋合
 * @create: 2020-10-25 20:50
 **/
@Repository
public interface TbShoppingBookMapper extends BaseMapper<TbShoppingBook> {
}
