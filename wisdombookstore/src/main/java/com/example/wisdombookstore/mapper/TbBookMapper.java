package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbBook;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 图书单品mapper
 * @date 2020/10/23 8:46
 */
@Mapper
public interface TbBookMapper extends BaseMapper<TbBook> {
}
