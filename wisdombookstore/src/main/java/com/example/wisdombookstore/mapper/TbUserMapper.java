package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbUser;
import org.springframework.stereotype.Repository;

/**
 * @author song he
 */
@Repository
public interface TbUserMapper extends BaseMapper<TbUser> {



}
