package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.RlBookTypeInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 书籍与类型中间表mapper
 * @date 2020/10/23 9:26
 */
@Mapper
public interface RlBookTypeInfomMapper extends BaseMapper<RlBookTypeInfo> {

}
