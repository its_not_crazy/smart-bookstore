package com.example.wisdombookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wisdombookstore.entity.TbCron;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: wisdombookstore
 * @description: 周期任务Mapper
 * @author: 陈阳
 * @create: 2020-10-28 21:44
 */
@Mapper
public interface TbCronMapper extends BaseMapper<TbCron> {



}
